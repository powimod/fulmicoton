#include <glib.h>
#include "message.h"
#include "message-manager.h"
#include "util.h"
#include "data-manager.h"
#include "constant-component.h"

#define CAPTURE G_TEST_SUBPROCESS_INHERIT_STDOUT | G_TEST_SUBPROCESS_INHERIT_STDERR

typedef struct {
	FulmStorage* storage;
} TestContext;


static void
context_test_root(TestContext* context, gconstpointer user_data) 
{
	GError* error = NULL;
	FulmComponent* child_component;
	FulmDataManager* data_manager;
	FulmRootComponent* root_component;

	data_manager = fulm_data_manager_new(context->storage);

	root_component = fulm_data_manager_get_root_component(data_manager, &error);
	g_assert_null(error);
	g_assert_nonnull(root_component);
	g_assert_cmpstr(fulm_component_get_key(FULM_COMPONENT(root_component)), ==, "#root");
	g_assert_null(fulm_component_get_parent_key(FULM_COMPONENT(root_component)));

	g_print("\nChecking adding a composite component...\n");
	child_component = fulm_data_manager_create_component(data_manager, 
		fulm_composite_component_type_name(), 
		FULM_COMPOSITE_COMPONENT(root_component),
		&error);
	g_assert_no_error(error);
	g_assert_nonnull(child_component);
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(child_component));
	gchar* child_key = g_strdup(fulm_component_get_key(child_component));
	g_print("Child key : [%s]\n", child_key);
	g_assert_nonnull(child_key);
	g_assert_cmpstr(fulm_component_get_parent_key(child_component), ==, "#root");

	gchar** child_key_array = fulm_composite_component_get_child_component_keys(FULM_COMPOSITE_COMPONENT(root_component));
	g_assert_nonnull(child_key_array);
	g_assert_nonnull(child_key_array[0]);
	g_print("=> Child key : [%s]\n", child_key);
	g_assert_cmpstr(child_key_array[0], ==, child_key);
	g_assert_null(child_key_array[1]);
	g_free(child_key_array);

	g_object_unref(child_component);
	

	// check only composite component can be add to root component
	g_print("\nChecking adding a non-composite component is forbidden...\n");
	g_clear_error(&error);
	child_component = fulm_data_manager_create_component(data_manager, 
		fulm_constant_component_type_name(), 
		FULM_COMPOSITE_COMPONENT(root_component),
		&error);
	g_assert_error(error, FULM_ROOT_COMPONENT_ERROR, FULM_ROOT_COMPONENT_ERROR_ONLY_COMPOSITE);

	g_assert_null(child_component);

	g_object_unref(data_manager);


	g_print("\nReloading data-manager...\n");
	data_manager = fulm_data_manager_new(context->storage);
	g_clear_error(&error);
	root_component = fulm_data_manager_get_root_component(data_manager, &error);

	child_key_array = fulm_composite_component_get_child_component_keys(FULM_COMPOSITE_COMPONENT(root_component));
	g_assert_nonnull(child_key_array);
	g_assert_nonnull(child_key_array[0]);
	g_print("=> Child key : [%s]\n", child_key);
	g_assert_cmpstr(child_key_array[0], ==, child_key);
	g_assert_null(child_key_array[1]);
	g_free(child_key_array);

	g_clear_error(&error);
	child_component = fulm_data_manager_load_component(data_manager,child_key, &error);
	g_assert_no_error(error);
	g_assert_nonnull(child_component);
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(child_component));
	const gchar* real_child_key = fulm_component_get_key(child_component);
	g_assert_nonnull(real_child_key);
	g_assert_cmpstr(real_child_key, ==, child_key);
	g_print("Child key : [%s]\n", real_child_key);
	g_assert_nonnull(child_key);
	g_assert_cmpstr(fulm_component_get_parent_key(child_component), ==, "#root");

	g_free(child_key);
	g_object_unref(child_component);

}


static void
context_setup (TestContext* context, gconstpointer user_data)
{
	context->storage = fulm_testsuite_util_build_storage(/*delete_previous_file*/ TRUE);
	g_assert_nonnull(context->storage);
}


static void
context_teardown(TestContext* context, gconstpointer user_data)
{
	fulm_testsuite_util_terminate_storage(context->storage, /*delete_file*/ TRUE);
	g_object_unref(context->storage);
}


int
main(int argc, char** argv) 
{
	g_test_init(&argc, &argv, NULL);
	g_test_add("/data-manager-test/root", TestContext, NULL, context_setup, context_test_root, context_teardown);
	return g_test_run();
}
