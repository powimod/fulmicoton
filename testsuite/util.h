#ifndef __FULM_TEST_SUITE_UTIL__
#define __FULM_TEST_SUITE_UTIL__

#include "storage.h"

FulmStorage* fulm_testsuite_util_build_storage(gboolean delete_previous_file);
void fulm_testsuite_util_terminate_storage(FulmStorage* storage, gboolean delete_file);

#endif /*__FULM_TEST_SUITE_UTIL__*/
