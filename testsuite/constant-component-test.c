#include <glib.h>
#include "constant-component.h"

#ifdef G_OS_WIN32
#include <fileapi.h> // Windows file attribute SetFileAttributes
#endif

#include "util.h"

typedef struct {
	FulmComponentFactory* factory;
} TestContext;


static void
context_test_json(TestContext* context, gconstpointer user_data) 
{
	g_print("\nSaving constant component to json :\n");

	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_constant_component_type_name()));
	g_assert_nonnull(my_component);
	g_assert_true(FULM_IS_CONSTANT_COMPONENT(my_component));
	fulm_component_set_name(FULM_COMPONENT(my_component), "my_composant");
	fulm_component_set_key(FULM_COMPONENT(my_component), "#620");
	fulm_component_set_parent_key(FULM_COMPONENT(my_component), "#610");
	fulm_constant_component_set_value(FULM_CONSTANT_COMPONENT(my_component), "a_value");

	g_print("- name=%s\n", fulm_component_get_name(FULM_COMPONENT(my_component)));
	g_print("- key=%s\n", fulm_component_get_key(FULM_COMPONENT(my_component)));
	g_print("- parent key=%s\n", fulm_component_get_parent_key(FULM_COMPONENT(my_component)));
	g_print("- value=%s\n", fulm_constant_component_get_value(FULM_CONSTANT_COMPONENT(my_component)));

	JsonBuilder *json_builder = json_builder_new();
	fulm_component_save_to_json(FULM_COMPONENT(my_component), json_builder);

	JsonNode* json_node = json_builder_get_root(json_builder);
	JsonGenerator *json_generator = json_generator_new();
	json_generator_set_root(json_generator, json_node);
	gchar *json_str = json_generator_to_data (json_generator, NULL);
	g_print("json=%s\n\n", json_str);

	g_object_unref(json_generator);
	g_object_unref(json_builder);
	g_object_unref(my_component);




	g_print("\nLoading constant component from json :\n");

	my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_constant_component_type_name()));
	g_assert_nonnull(my_component);
	g_assert_true(FULM_IS_CONSTANT_COMPONENT(my_component));

	JsonParser* json_parser = json_parser_new();
	g_assert(json_parser_load_from_data(json_parser, json_str, -1, NULL));
	json_node = json_parser_get_root (json_parser);
	JsonReader* json_reader = json_reader_new(json_node);
	fulm_component_load_from_json(my_component, json_reader);

	const gchar* str = fulm_component_get_name(my_component);
	g_assert(str != NULL);
	g_print("- name=%s\n", str);
	g_assert_cmpstr(str, ==, "my_composant"); 

	str = fulm_component_get_key(my_component);
	g_assert(str != NULL);
	g_print("- key = %s\n", str);
	g_assert_cmpstr(str, ==, "#620"); 

	str = fulm_component_get_parent_key(my_component);
	g_assert(str != NULL);
	g_print("- parent key = %s\n", str);
	g_assert_cmpstr(str, ==, "#610"); 


	str = fulm_constant_component_get_value(FULM_CONSTANT_COMPONENT(my_component));
	g_assert(str != NULL);
	g_print("- value = %s\n", fulm_constant_component_get_value(FULM_CONSTANT_COMPONENT(my_component)));
	g_assert_cmpstr(str, ==, "a_value"); 

	g_object_unref(json_reader);
	g_object_unref(json_parser);

	g_object_unref(my_component);
	g_free(json_str);
}

static void
context_test_pins(TestContext* context, gconstpointer user_data) 
{
	const gchar** str_array;
	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_constant_component_type_name()));
	g_assert_nonnull(my_component);
	g_assert_true(FULM_IS_CONSTANT_COMPONENT(my_component));


	//===== INPUTS 

	g_print("\nTesting empty input pins...\n");
	g_assert_cmpint(fulm_component_get_pin_count(my_component, FULM_COMPONENT_PIN_TYPE_INPUT), ==, 0);
	str_array = fulm_component_get_pin_names(my_component, FULM_COMPONENT_PIN_TYPE_INPUT);
	g_assert_nonnull(str_array);
	g_assert_null(str_array[0]);
	g_test_trap_subprocess( "/constant-component-test/pins/subprocess/input-out-of-range", 0, 0);
	g_test_trap_assert_failed();
	

	//===== OUTPUTS 

	g_print("\nTesting empty output pins...\n");
	g_assert_cmpint(fulm_component_get_pin_count(my_component, FULM_COMPONENT_PIN_TYPE_OUTPUT), ==, 1);
	str_array = fulm_component_get_pin_names(my_component, FULM_COMPONENT_PIN_TYPE_OUTPUT);
	g_assert_nonnull(str_array);
	g_assert_cmpstr(str_array[0], ==, "value");
	g_assert_null(str_array[1]);
	g_test_trap_subprocess( "/constant-component-test/pins/subprocess/output-out-of-range", 0, 0);
	g_test_trap_assert_failed();

	FulmComponentPin* pin = fulm_component_get_pin_by_index(my_component, FULM_COMPONENT_PIN_TYPE_OUTPUT, 0);
	g_assert(pin != NULL);
	g_assert(FULM_IS_COMPONENT_PIN(pin));
	g_assert_cmpstr(fulm_component_pin_get_name(pin), ==, "value");
	g_assert_cmpstr(fulm_component_pin_get_value(pin), ==, "");
	fulm_component_pin_set_value(pin, "first_new_value");
	g_assert_cmpstr(fulm_component_pin_get_value(pin), ==, "first_new_value");
	fulm_component_pin_set_value(pin, "second_new_value");
	g_assert_cmpstr(fulm_component_pin_get_value(pin), ==, "second_new_value");

	//===== HIDDEN
	
	g_print("Testing hidden pins...\n");
	g_assert_cmpint(fulm_component_get_pin_count(my_component, FULM_COMPONENT_PIN_TYPE_HIDDEN), ==, 0);
	str_array = fulm_component_get_pin_names(my_component, FULM_COMPONENT_PIN_TYPE_HIDDEN);
	g_assert_nonnull(str_array);
	g_assert_null(str_array[0]);
	g_test_trap_subprocess( "/constant-component-test/pins/subprocess/hidden-out-of-range", 0, 0);
	g_test_trap_assert_failed();

}

static void context_test_pins_input_out_of_range(TestContext* context, gconstpointer user_data) 
{
	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_constant_component_type_name()));
	fulm_component_get_pin_by_index(my_component, FULM_COMPONENT_PIN_TYPE_INPUT, 0); // input index out of range 
}

static void context_test_pins_output_out_of_range(TestContext* context, gconstpointer user_data) 
{
	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_constant_component_type_name()));
	fulm_component_get_pin_by_index(my_component, FULM_COMPONENT_PIN_TYPE_INPUT, 1); // output index out of range 
}

static void context_test_pins_hidden_out_of_range(TestContext* context, gconstpointer user_data) 
{
	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_constant_component_type_name()));
	fulm_component_get_pin_by_index(my_component, FULM_COMPONENT_PIN_TYPE_HIDDEN, 0); // hidden index out of range 
}

static void
context_test_storage(TestContext* context, gconstpointer user_data) 
{
	GError* error = NULL;
	FulmStorage* storage;
	gboolean result;

	g_print("\nSaving constant component to storage...\n");
	FulmComponent* my_constant = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_constant_component_type_name()));
	storage = fulm_testsuite_util_build_storage(TRUE);

	fulm_component_set_name(my_constant, "my_composant");
	fulm_component_set_key(my_constant, "#620");
	fulm_component_set_parent_key(my_constant, "#610");
	fulm_constant_component_set_value(FULM_CONSTANT_COMPONENT(my_constant), "a_value");

	g_print("- name=%s\n", fulm_component_get_name(my_constant));
	g_print("- key=%s\n", fulm_component_get_key(my_constant));
	g_print("- parent key=%s\n", fulm_component_get_parent_key(my_constant));
	g_print("- value=%s\n", fulm_constant_component_get_value(FULM_CONSTANT_COMPONENT(my_constant)));

	result = fulm_storable_store(FULM_STORABLE(my_constant), storage, &error);
	g_assert_no_error(error);
	g_assert_true(result);
	g_print("done\n");

	g_print("Checking stored json...\n");
	gchar* json = NULL;
	g_assert_true(fulm_storage_fetch_string(storage, "#620", &json, NULL));
	g_print("Stored JSON=[%s]\n", json);
	gchar* attempted_json = "{\"key\":\"#620\",\"type\":\"constant-component\",\"name\":\"my_composant\",\"pkey\":\"#610\",\"cnx\":[],\"value\":\"a_value\"}";

	g_assert_nonnull(json);
	g_assert_cmpstr(json, ==, attempted_json);

	g_object_unref(my_constant);
	fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ FALSE);


	g_print("\nLoading constant component from storage :\n");
	my_constant = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_constant_component_type_name()));
	storage = fulm_testsuite_util_build_storage(FALSE);
	fulm_component_set_key(my_constant, "#620");
	fulm_component_set_parent_key(my_constant, "#610");

	result = fulm_storable_fetch(FULM_STORABLE(my_constant), storage, &error);
	g_assert_no_error(error);
	g_assert_true(result);

	g_print("- name=%s\n", fulm_component_get_name(my_constant));
	g_print("- key=%s\n", fulm_component_get_key(my_constant));
	g_print("- parent key=%s\n", fulm_component_get_parent_key(my_constant));
	g_print("- value=%s\n", fulm_constant_component_get_value(FULM_CONSTANT_COMPONENT(my_constant)));

	g_assert_cmpstr(fulm_component_get_key(my_constant), ==, "#620");
	g_assert_cmpstr(fulm_component_get_parent_key(my_constant), ==, "#610");
	g_assert_cmpstr(fulm_component_get_name(my_constant), ==, "my_composant");
	g_assert_cmpstr(fulm_constant_component_get_value(FULM_CONSTANT_COMPONENT(my_constant)), ==, "a_value");

	g_object_unref(my_constant);
	fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ TRUE);

	g_print("done\n");
}


static void
context_setup (TestContext* context, gconstpointer user_data)
{
	g_print("Creating component factory...\n");
	context->factory = (FulmComponentFactory*)fulm_component_factory_new();
	g_assert_nonnull(context->factory);
	fulm_component_factory_register(context->factory, fulm_constant_component_type_name(), fulm_constant_component_new);

}


static void
context_teardown(TestContext* context, gconstpointer user_data)
{
	g_object_unref(context->factory);
}


int
main(int argc, char** argv) 
{
	g_test_init(&argc, &argv, NULL);
	g_test_add("/constant-component-test/json", TestContext, NULL, context_setup, context_test_json, context_teardown);

	g_test_add("/constant-component-test/pins", TestContext, NULL, context_setup, context_test_pins, context_teardown);
	g_test_add("/constant-component-test/pins/subprocess/input-out-of-range", 
		TestContext, NULL, context_setup, context_test_pins_input_out_of_range, context_teardown);
	g_test_add("/constant-component-test/pins/subprocess/output-out-of-range", 
		TestContext, NULL, context_setup, context_test_pins_output_out_of_range, context_teardown);
	g_test_add("/constant-component-test/pins/subprocess/hidden-out-of-range", 
		TestContext, NULL, context_setup, context_test_pins_hidden_out_of_range, context_teardown);

	g_test_add("/constant-component-test/storage", TestContext, NULL, context_setup, context_test_storage, context_teardown);

	return g_test_run();
}
