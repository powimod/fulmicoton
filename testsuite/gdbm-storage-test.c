#include <glib.h>
#include "gdbm-storage.h"
#include "util.h"


typedef struct {
	GFile* db_file;
	FulmStorage* storage;
} TestContext;

static void
context_test_init(TestContext* context, gconstpointer user_data) 
{
	g_assert_true(context->storage != NULL);
	g_assert_true(FULM_IS_STORAGE(context->storage));
	g_assert_true(FULM_IS_GDBM_STORAGE(context->storage));
}

static void
context_test_key_generator(TestContext* context, gconstpointer user_data) 
{
	g_print("Generating unique key\n");
	gchar* key;
	GError* error = NULL;	
	FulmStorage* storage = context->storage;

	key = fulm_storage_generate_unique_key_with_prefix(storage, "Z", &error);
	g_assert_no_error(error);
	g_print("First Z key :[%s]", key);
	g_assert_cmpstr(key, ==, "Z#1");
	g_free(key);

	key = fulm_storage_generate_unique_key_with_prefix(storage, "Z", &error);
	g_assert_no_error(error);
	g_print("Second Z key :[%s]", key);
	g_assert_cmpstr(key, ==, "Z#2");
	g_free(key);

	key = fulm_storage_generate_unique_key_with_prefix(storage, "W", &error);
	g_assert_no_error(error);
	g_print("First W key :[%s]", key);
	g_assert_cmpstr(key, ==, "W#1");
	g_free(key);

	key = fulm_storage_generate_unique_key_with_prefix(storage, "Z", &error);
	g_assert_no_error(error);
	g_print("Third Z key :[%s]", key);
	g_assert_cmpstr(key, ==, "Z#3");
	g_free(key);

	key = fulm_storage_generate_unique_key_with_prefix(storage, "W", &error);
	g_assert_no_error(error);
	g_print("Second W key :[%s]", key);
	g_assert_cmpstr(key, ==, "W#2");
	g_free(key);

}

static void
context_test_string(TestContext* context, gconstpointer user_data) 
{
	FulmStorage* storage = context->storage;
	g_assert(storage != NULL);

	g_print("Storing string...\n");
	gchar* stored_value = g_strdup("this is a string");
	g_assert_true(fulm_storage_store_string(storage, "k1", stored_value, NULL));
	g_assert_nonnull(stored_value);
	g_print("done\n");

	g_print("Fetching string...\n");
	gchar* fetched_value = NULL;
	g_assert_true(fulm_storage_fetch_string(storage, "k1", &fetched_value, NULL));
	g_assert_nonnull(fetched_value);
	g_print("done\n");

	g_assert_cmpstr(stored_value, ==, fetched_value);
	g_print("Stored and fetching strings are equal\n");

	g_free(stored_value);
	g_free(fetched_value);
}

static void
context_test_json(TestContext* context, gconstpointer user_data)
{
	FulmStorage* storage = context->storage;
	g_assert(storage != NULL);

	g_print("Storing json...\n");

	JsonBuilder *json_builder = json_builder_new();
	json_builder_begin_object(json_builder);

	json_builder_set_member_name(json_builder, "a_string");
	json_builder_add_string_value(json_builder, "string_value");
	json_builder_set_member_name(json_builder, "a_integer");
	json_builder_add_int_value(json_builder, 22);

	json_builder_set_member_name(json_builder, "a_list");
	json_builder_begin_array(json_builder);
	json_builder_add_string_value(json_builder, "A");
	json_builder_add_string_value(json_builder, "B");
	json_builder_end_array(json_builder);

	json_builder_end_object(json_builder);

	JsonNode* json_node = json_builder_get_root(json_builder);

	JsonGenerator *json_generator = json_generator_new();
	json_generator_set_root(json_generator, json_node);
	gchar *str = json_generator_to_data (json_generator, NULL);
	g_print("Json:[%s]\n", str);
	g_object_unref(json_generator);
	g_free(str);

	g_assert_true(fulm_storage_store_json(storage, "k1", json_node , NULL));
	json_node_unref(json_node);

	g_print("done\n");


	g_print("Fetching json...\n");

	json_node = NULL;
	g_assert_true(fulm_storage_fetch_json(storage, "k1", &json_node , NULL));
	g_assert_nonnull(json_node);

	JsonReader* reader = json_reader_new(json_node);

	json_reader_read_member(reader, "a_string");
	str = (gchar*) json_reader_get_string_value(reader);
	g_assert(str != NULL);
	g_print("a_string=%s\n", str);
	g_assert_cmpstr(str , ==, "string_value");
	json_reader_end_member(reader);

	json_reader_read_member(reader, "a_integer");
	gint i = json_reader_get_int_value(reader);
	g_print("a_string=%s\n", str);
	g_assert_cmpint(i, ==, 22);
	json_reader_end_member(reader);


	json_reader_read_member(reader, "a_list");
	gint n = json_reader_count_elements(reader);
	g_assert_cmpint(n, ==, 2);

	g_assert_true(json_reader_read_element(reader,0));
	str = (gchar*) json_reader_get_string_value(reader);
	g_assert_cmpstr(str, ==, "A");
	json_reader_end_element(reader);

	g_assert_true(json_reader_read_element(reader,1));
	str = (gchar*) json_reader_get_string_value(reader);
	g_assert_cmpstr(str, ==, "B");
	json_reader_end_element(reader);

	json_reader_end_member(reader);

	g_object_unref(reader);
	g_print("done\n");
}


static void
context_test_delete(TestContext* context, gconstpointer user_data)
{
	gboolean result;
	GError* error = NULL;
	gchar* fetched_value = NULL;
	FulmStorage* storage = context->storage;
	g_assert(storage != NULL);

	g_print("Checking deletion by key...\n");

	g_print("Add tree records\n");
	g_assert_true(fulm_storage_store_string(storage, "k1", "v1", NULL));
	g_assert_true(fulm_storage_store_string(storage, "k2", "v2", NULL));
	g_assert_true(fulm_storage_store_string(storage, "k3", "v3", NULL));


	g_print("Delete second record\n");
	result = fulm_storage_delete_by_key(storage, "k2", &error);
	g_assert_no_error(error);
	g_assert(result);

	g_print("Check second record is deleted\n");
	result = fulm_storage_fetch_string(storage, "k2", &fetched_value, &error);
	g_assert_nonnull(error);
	g_assert_cmpint(error->domain, ==, FULM_TYPE_STORAGE_ERROR);
	g_assert_cmpint(error->code, ==, FULM_STORAGE_ERROR_KEY_NOT_FOUND);
	g_assert_false(result);
	g_assert_null(fetched_value);
	
	g_print("Check first record already exists\n");
	error = NULL;
	result = fulm_storage_fetch_string(storage, "k1", &fetched_value, &error);
	g_assert_no_error(error);
	g_assert_true(result);
	g_free(fetched_value);

	g_print("Check third record already exists\n");
	error = NULL;
	result = fulm_storage_fetch_string(storage, "k3", &fetched_value, &error);
	g_assert_no_error(error);
	g_assert_true(result);
	g_free(fetched_value);

}


static void
context_setup (TestContext* context, gconstpointer user_data)
{
	context->storage = fulm_testsuite_util_build_storage(TRUE);
}


static void
context_teardown(TestContext* context, gconstpointer user_data)
{
	fulm_testsuite_util_terminate_storage(context->storage, /*delete_file*/ TRUE);
}


int
main(int argc, char** argv) 
{
	g_test_init(&argc, &argv, NULL);
	g_test_add("/storage-test/init",   TestContext, NULL, context_setup, context_test_init,   context_teardown);
	g_test_add("/storage-test/keys",   TestContext, NULL, context_setup, context_test_key_generator,   context_teardown);
	g_test_add("/storage-test/string", TestContext, NULL, context_setup, context_test_string, context_teardown);
	g_test_add("/storage-test/json",   TestContext, NULL, context_setup, context_test_json,   context_teardown);
	g_test_add("/storage-test/delete", TestContext, NULL, context_setup, context_test_delete, context_teardown);
	return g_test_run();
}
