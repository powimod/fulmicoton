#include <glib.h>
#include "composite-component.h"
#include "constant-component.h"

#ifdef G_OS_WIN32
#include <fileapi.h> // Windows file attribute SetFileAttributes
#endif


typedef struct {
	FulmComponentFactory* factory;
} TestContext;


static void
context_test_json(TestContext* context, gconstpointer user_data) 
{
	g_print("\nSaving composite component to json :\n");

	FulmComponent* my_composite_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_composite_component_type_name()));
	g_assert_nonnull(my_composite_component);
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(my_composite_component));

	fulm_component_set_name(FULM_COMPONENT(my_composite_component), "my_composant");
	fulm_component_set_key(FULM_COMPONENT(my_composite_component), "#620");
	fulm_component_set_parent_key(FULM_COMPONENT(my_composite_component), "#610");

	g_print("- name=%s\n", fulm_component_get_name(FULM_COMPONENT(my_composite_component)));
	g_print("- key=%s\n", fulm_component_get_key(FULM_COMPONENT(my_composite_component)));
	g_print("- parent key=%s\n", fulm_component_get_parent_key(FULM_COMPONENT(my_composite_component)));

	for (gint i = 0; i < 3; i++) {
		FulmComponent* child_component = fulm_component_factory_build(context->factory, fulm_constant_component_type_name());
		g_assert(child_component != NULL);
		g_assert(FULM_IS_CONSTANT_COMPONENT(child_component));
		gchar* key = g_strdup_printf("#44%i", i);
		fulm_component_set_key(child_component, key);
		g_free(key);
		gchar* name = g_strdup_printf("component_%i", i);
		fulm_component_set_name(child_component, name);
		g_free(name);
		GError* error = NULL;
		g_print("Add component [%s] to composite [%s]\n", 
			fulm_component_get_key(child_component),
			fulm_component_get_key(my_composite_component));
		fulm_composite_component_add_component(FULM_COMPOSITE_COMPONENT(my_composite_component), FULM_COMPONENT(child_component), &error);
		g_assert_no_error(error);
		g_print(" - child [%s] = %s\n",
			fulm_component_get_key(child_component),
			fulm_component_get_name(child_component));
		g_object_unref(child_component);
	}

	JsonBuilder *json_builder = json_builder_new();
	fulm_component_save_to_json(FULM_COMPONENT(my_composite_component), json_builder);

	JsonNode* json_node = json_builder_get_root(json_builder);
	JsonGenerator *json_generator = json_generator_new();
	json_generator_set_root(json_generator, json_node);
	gchar *json_str = json_generator_to_data (json_generator, NULL);
	g_print("json=%s\n\n", json_str);

	g_object_unref(json_generator);
	g_object_unref(json_builder);
	g_object_unref(my_composite_component);




	g_print("\nLoading composite component from json :\n");

	my_composite_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_composite_component_type_name()));
	g_assert_nonnull(my_composite_component);
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(my_composite_component));

	JsonParser* json_parser = json_parser_new();
	g_assert(json_parser_load_from_data(json_parser, json_str, -1, NULL));
	json_node = json_parser_get_root (json_parser);
	JsonReader* json_reader = json_reader_new(json_node);
	fulm_component_load_from_json(my_composite_component, json_reader);

	const gchar* str = fulm_component_get_name(my_composite_component);
	g_assert(str != NULL);
	g_print("- name=%s\n", str);
	g_assert_cmpstr(str, ==, "my_composant"); 

	str = fulm_component_get_key(my_composite_component);
	g_assert(str != NULL);
	g_print("- key = %s\n", str);
	g_assert_cmpstr(str, ==, "#620"); 

	str = fulm_component_get_parent_key(my_composite_component);
	g_assert(str != NULL);
	g_print("- parent key = %s\n", str);
	g_assert_cmpstr(str, ==, "#610"); 

	gchar** keys_array = fulm_composite_component_get_child_component_keys(FULM_COMPOSITE_COMPONENT(my_composite_component));
	gint n = 0;
	for (gint i = 0; keys_array[i] != NULL; i++)  {
		g_print("- child key : %s\n", keys_array[i]);
		gchar* attempted_key= g_strdup_printf("#44%i", i);
		g_assert_cmpstr(keys_array[i], ==, attempted_key);
		g_free(attempted_key);
		n++;
	}
	g_assert_cmpint(n, ==, 3);
	g_assert_null(keys_array[3]); // array terminated with NULL

	g_free(keys_array);

	g_object_unref(json_reader);
	g_object_unref(json_parser);

	g_object_unref(my_composite_component);
	g_free(json_str);
}



static void
context_setup (TestContext* context, gconstpointer user_data)
{
	g_print("Creating component factory...\n");
	context->factory = (FulmComponentFactory*)fulm_component_factory_new();
	g_assert_nonnull(context->factory);
	fulm_component_factory_register(context->factory, fulm_composite_component_type_name(), fulm_composite_component_new);
	fulm_component_factory_register(context->factory, fulm_constant_component_type_name(), fulm_constant_component_new);

}


static void
context_teardown(TestContext* context, gconstpointer user_data)
{
	g_object_unref(context->factory);
}


int
main(int argc, char** argv) 
{
	g_test_init(&argc, &argv, NULL);
	g_test_add("/composite-component-test/json", TestContext, NULL, context_setup, context_test_json, context_teardown);
	return g_test_run();
}
