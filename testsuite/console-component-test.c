#include <glib.h>
#include "console-component.h"

#ifdef G_OS_WIN32
#include <fileapi.h> // Windows file attribute SetFileAttributes
#endif

#define CAPTURE G_TEST_SUBPROCESS_INHERIT_STDOUT | G_TEST_SUBPROCESS_INHERIT_STDERR

typedef struct {
	FulmComponentFactory* factory;
} TestContext;


static void
context_test_json(TestContext* context, gconstpointer user_data) 
{
	g_print("\nSaving console component to json...\n");

	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_console_component_type_name()));
	g_assert_nonnull(my_component);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(my_component));

	fulm_component_set_name(FULM_COMPONENT(my_component), "my_composant");
	fulm_component_set_key(FULM_COMPONENT(my_component), "#620");
	fulm_component_set_parent_key(FULM_COMPONENT(my_component), "#610");

	g_print("- name=%s\n", fulm_component_get_name(FULM_COMPONENT(my_component)));
	g_print("- key=%s\n", fulm_component_get_key(FULM_COMPONENT(my_component)));
	g_print("- parent key=%s\n", fulm_component_get_parent_key(FULM_COMPONENT(my_component)));

	JsonBuilder *json_builder = json_builder_new();
	fulm_component_save_to_json(FULM_COMPONENT(my_component), json_builder);

	JsonNode* json_node = json_builder_get_root(json_builder);
	JsonGenerator *json_generator = json_generator_new();
	json_generator_set_root(json_generator, json_node);
	gchar *json_str = json_generator_to_data (json_generator, NULL);
	g_print("json=%s\n\n", json_str);

	g_object_unref(json_generator);
	g_object_unref(json_builder);
	g_object_unref(my_component);




	g_print("\nLoading console component from json :\n");

	my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_console_component_type_name()));
	g_assert_nonnull(my_component);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(my_component));

	JsonParser* json_parser = json_parser_new();
	g_assert(json_parser_load_from_data(json_parser, json_str, -1, NULL));
	json_node = json_parser_get_root (json_parser);
	JsonReader* json_reader = json_reader_new(json_node);
	fulm_component_load_from_json(my_component, json_reader);

	const gchar* str = fulm_component_get_name(my_component);
	g_assert(str != NULL);
	g_print("- name=%s\n", str);
	g_assert_cmpstr(str, ==, "my_composant"); 

	str = fulm_component_get_key(my_component);
	g_assert(str != NULL);
	g_print("- key = %s\n", str);
	g_assert_cmpstr(str, ==, "#620"); 

	str = fulm_component_get_parent_key(my_component);
	g_assert(str != NULL);
	g_print("- parent key = %s\n", str);
	g_assert_cmpstr(str, ==, "#610"); 

	g_object_unref(json_reader);
	g_object_unref(json_parser);

	g_object_unref(my_component);
	g_free(json_str);
}



static void
context_test_pins(TestContext* context, gconstpointer user_data) 
{
	const gchar** str_array;
	FulmComponentPin* pin;

	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_console_component_type_name()));
	g_assert_nonnull(my_component);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(my_component));


	//===== INPUTS 

	g_print("\nTesting input pins...\n");

	g_assert_cmpint(fulm_component_get_pin_count(my_component, FULM_COMPONENT_PIN_TYPE_INPUT), ==, 1);
	str_array = fulm_component_get_pin_names(my_component, FULM_COMPONENT_PIN_TYPE_INPUT);
	g_assert_nonnull(str_array);
	g_assert_cmpstr(str_array[0], ==, "log");
	g_assert_null(str_array[1]);
	g_test_trap_subprocess( "/console-component-test/pins/subprocess/input-out-of-range", 0, 0);
	g_test_trap_assert_failed();


	// get a pin by index
	pin = fulm_component_get_pin_by_index(my_component, FULM_COMPONENT_PIN_TYPE_INPUT, 0);
	g_assert(pin != NULL);
	g_assert(FULM_IS_COMPONENT_PIN(pin));
	g_assert_cmpstr(fulm_component_pin_get_name(pin), ==, "log");

	// check search a invalid pin name search returns NULL
	pin = fulm_component_get_pin_by_name(my_component, FULM_COMPONENT_PIN_TYPE_INPUT, "foo");
	g_assert(pin == NULL);

	// get a pin by name
	pin = fulm_component_get_pin_by_name(my_component, FULM_COMPONENT_PIN_TYPE_INPUT, "log");
	g_assert(pin != NULL);
	g_assert(FULM_IS_COMPONENT_PIN(pin));
	g_assert_cmpstr(fulm_component_pin_get_name(pin), ==, "log");

	g_assert_null(fulm_component_pin_get_value(pin)); // FIXME log input pin has no initial value, set it to empty string ?
	fulm_component_pin_set_value(pin, "first_message");
	g_assert_cmpstr(fulm_component_pin_get_value(pin), ==, "first_message");
	fulm_component_pin_set_value(pin, "second_message");
	g_assert_cmpstr(fulm_component_pin_get_value(pin), ==, "second_message");


	//===== OUTPUTS 

	g_print("\nTesting empty output pins...\n");

	g_assert_cmpint(fulm_component_get_pin_count(my_component, FULM_COMPONENT_PIN_TYPE_OUTPUT), ==, 0);

	str_array = fulm_component_get_pin_names(my_component, FULM_COMPONENT_PIN_TYPE_OUTPUT);
	g_assert_nonnull(str_array);
	g_assert_null(str_array[0]);
	g_test_trap_subprocess( "/console-component-test/pins/subprocess/output-out-of-range", 0, 0);
	g_test_trap_assert_failed();

	// check search a invalid output pin name search returns NULL
	pin = fulm_component_get_pin_by_name(my_component, FULM_COMPONENT_PIN_TYPE_OUTPUT, "foo");
	g_assert(pin == NULL);


	//===== HIDDEN
	
	g_print("Testing hidden pins...\n");
	g_assert_cmpint(fulm_component_get_pin_count(my_component, FULM_COMPONENT_PIN_TYPE_HIDDEN), ==, 0);
	str_array = fulm_component_get_pin_names(my_component, FULM_COMPONENT_PIN_TYPE_HIDDEN);
	g_assert_nonnull(str_array);
	g_assert_null(str_array[0]);
	g_test_trap_subprocess( "/console-component-test/pins/subprocess/hidden-out-of-range", 0, 0);
	g_test_trap_assert_failed();

	// check search a invalid hidden pin name search returns NULL
	pin = fulm_component_get_pin_by_name(my_component, FULM_COMPONENT_PIN_TYPE_HIDDEN, "foo");
	g_assert(pin == NULL);
}

static void
my_log_handler (const gchar *log_domain, GLogLevelFlags log_level, const gchar *message, gpointer user_data)
{
	gboolean* p_success = user_data;
	if (g_strcmp0(message, "my log message") == 0){
		g_print("Message received\n");
		*p_success = TRUE;
	}
	else {
		g_print("Unattempted message : [%s]\n", message);
		*p_success = FALSE;
	}
}


static void
context_test_log_pin(TestContext* context, gconstpointer user_data) 
{
	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_console_component_type_name()));
	g_assert_nonnull(my_component);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(my_component));

	FulmComponentPin* pin = fulm_component_get_pin_by_index(my_component, FULM_COMPONENT_PIN_TYPE_INPUT, 0);
	g_assert(pin != NULL);
	g_assert(FULM_IS_COMPONENT_PIN(pin));

	// check message is printed in GLib structured log
	gboolean message_received = FALSE;
	g_print("Intercept log messages\n");
	guint log_handler_id = g_log_set_handler(NULL, G_LOG_LEVEL_MESSAGE, my_log_handler, &message_received);

	g_print("Sending log message...\n");
	fulm_component_pin_set_value(pin, "my log message");

	g_log_remove_handler(NULL, log_handler_id);

	g_print("Message received : %s\n", (message_received) ? "TRUE": "FALSE");
	g_assert_true(message_received);

}

static void context_test_pins_input_out_of_range(TestContext* context, gconstpointer user_data) 
{
	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_console_component_type_name()));
	fulm_component_get_pin_by_index(my_component, FULM_COMPONENT_PIN_TYPE_INPUT, 1); // input index out of range 
}

static void context_test_pins_output_out_of_range(TestContext* context, gconstpointer user_data) 
{
	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_console_component_type_name()));
	fulm_component_get_pin_by_index(my_component, FULM_COMPONENT_PIN_TYPE_OUTPUT, 0); // output index out of range 
}

static void context_test_pins_hidden_out_of_range(TestContext* context, gconstpointer user_data) 
{
	FulmComponent* my_component = FULM_COMPONENT(fulm_component_factory_build(context->factory, fulm_console_component_type_name()));
	fulm_component_get_pin_by_index(my_component, FULM_COMPONENT_PIN_TYPE_HIDDEN, 0); // hidden index out of range 
}



static void
context_setup (TestContext* context, gconstpointer user_data)
{
	g_print("Creating component factory...\n");
	context->factory = (FulmComponentFactory*)fulm_component_factory_new();
	g_assert_nonnull(context->factory);
	fulm_component_factory_register(context->factory, fulm_console_component_type_name(), fulm_console_component_new);
}


static void
context_teardown(TestContext* context, gconstpointer user_data)
{
	g_object_unref(context->factory);
}


int
main(int argc, char** argv) 
{
	g_test_init(&argc, &argv, NULL);
	g_test_add("/console-component-test/json", TestContext, NULL, context_setup, context_test_json, context_teardown);

	g_test_add("/console-component-test/pins", TestContext, NULL, context_setup, context_test_pins, context_teardown);
	g_test_add("/console-component-test/pins/subprocess/input-out-of-range", 
		TestContext, NULL, context_setup, context_test_pins_input_out_of_range, context_teardown);
	g_test_add("/console-component-test/pins/subprocess/output-out-of-range", 
		TestContext, NULL, context_setup, context_test_pins_output_out_of_range, context_teardown);
	g_test_add("/console-component-test/pins/subprocess/hidden-out-of-range", 
		TestContext, NULL, context_setup, context_test_pins_hidden_out_of_range, context_teardown);

	g_test_add("/console-component-test/log-pin", TestContext, NULL, context_setup, context_test_log_pin, context_teardown);

	return g_test_run();
}
