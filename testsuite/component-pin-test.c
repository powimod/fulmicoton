#include <glib.h>
#include "component-pin.h"

#ifdef G_OS_WIN32
#include <fileapi.h> // Windows file attribute SetFileAttributes
#endif

typedef struct {
} TestContext;


static void
context_test_pin_creation(TestContext* context, gconstpointer user_data) 
{
	FulmComponentPin* pin = NULL;
	g_print("\nChecking pin object creation\n");

	// name argument is mandatory
	if (g_test_subprocess()){
		pin = fulm_component_pin_new(FULM_COMPONENT_PIN_TYPE_INPUT, NULL, NULL);
		return;
	}
	g_test_trap_subprocess(NULL, 0, 0);
	g_test_trap_assert_failed();

	pin = fulm_component_pin_new(FULM_COMPONENT_PIN_TYPE_INPUT, "input_1", NULL);
	g_assert_cmpint(fulm_component_pin_get_pin_type(pin), ==, FULM_COMPONENT_PIN_TYPE_INPUT);
	g_assert_nonnull(pin);

	pin = fulm_component_pin_new(FULM_COMPONENT_PIN_TYPE_OUTPUT, "output_1", NULL);
	g_assert_cmpint(fulm_component_pin_get_pin_type(pin), ==, FULM_COMPONENT_PIN_TYPE_OUTPUT);
	g_assert_nonnull(pin);

	pin = fulm_component_pin_new(FULM_COMPONENT_PIN_TYPE_HIDDEN, "hidden_1", NULL);
	g_assert_cmpint(fulm_component_pin_get_pin_type(pin), ==, FULM_COMPONENT_PIN_TYPE_HIDDEN);
	g_assert_nonnull(pin);
}

static void
context_test_value_prop(TestContext* context, gconstpointer user_data) 
{
	FulmComponentPin* pin = NULL;
	pin = fulm_component_pin_new(FULM_COMPONENT_PIN_TYPE_INPUT, "input_1", NULL);
	g_assert_null(fulm_component_pin_get_value(pin)); // default value is NULL
	fulm_component_pin_set_value(pin, "5V");
	g_assert_cmpstr(fulm_component_pin_get_value(pin), ==, "5V");
	g_object_unref(pin);

	// value is mandatory
	g_print("Checking setting null value...\n");
	pin = fulm_component_pin_new(FULM_COMPONENT_PIN_TYPE_INPUT, "input_1", NULL);
	g_assert_nonnull(pin);
	if (g_test_subprocess()){
		g_print("set value to NULL\n");
		fulm_component_pin_set_value(pin, NULL);
		g_print("new value = %s\n", fulm_component_pin_get_value(pin));
		g_object_unref(pin);
		return;
	}
	g_test_trap_subprocess(NULL, 0, G_TEST_SUBPROCESS_INHERIT_STDERR | G_TEST_SUBPROCESS_INHERIT_STDOUT);
	g_test_trap_assert_failed();
	g_object_unref(pin);
}


static void
context_test_connection(TestContext* context, gconstpointer user_data) 
{
	FulmComponentPin* pin = NULL;
	pin = fulm_component_pin_new(FULM_COMPONENT_PIN_TYPE_INPUT, "input_1", NULL);

	// by default, no connection : component = NULL and pin_index = -1
	g_assert_null(fulm_component_pin_get_connected_component_key(pin));
	g_assert_null(fulm_component_pin_get_connected_pin_name(pin));

	// connect to component #624 pin N°3
	fulm_component_pin_set_connection(pin, "#624", "A");
	g_assert_cmpstr(fulm_component_pin_get_connected_component_key(pin), ==, "#624");
	g_assert_cmpstr(fulm_component_pin_get_connected_pin_name(pin), ==, "A");

	// disconnect pin by setting component to NULL 
	fulm_component_pin_set_connection(pin, NULL, "A");
	g_assert_null(fulm_component_pin_get_connected_component_key(pin));
	g_assert_null(fulm_component_pin_get_connected_pin_name(pin));

	// reconnect to component #624 pin N°3
	fulm_component_pin_set_connection(pin, "#624", "B");
	g_assert_cmpstr(fulm_component_pin_get_connected_component_key(pin), ==, "#624");
	g_assert_cmpstr(fulm_component_pin_get_connected_pin_name(pin), ==, "B");

	// disconnect pin by setting pin to -1
	fulm_component_pin_set_connection(pin, "#624", NULL);
	g_assert_null(fulm_component_pin_get_connected_component_key(pin));
	g_assert_null(fulm_component_pin_get_connected_pin_name(pin));


	g_object_unref(pin);
}

static void
context_setup (TestContext* context, gconstpointer user_data)
{
}


static void
context_teardown(TestContext* context, gconstpointer user_data)
{
}


int
main(int argc, char** argv) 
{
	g_test_init(&argc, &argv, NULL);
	g_test_add("/component-pin-test/creation", TestContext, NULL, context_setup, context_test_pin_creation, context_teardown);
	g_test_add("/component-pin-test/name-prop", TestContext, NULL, context_setup, context_test_value_prop, context_teardown);
	g_test_add("/component-pin-test/connection", TestContext, NULL, context_setup, context_test_connection, context_teardown);
	return g_test_run();
}
