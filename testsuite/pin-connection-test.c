#include <glib.h>
#include "data-manager.h"
#include "composite-component.h"
#include "constant-component.h"
#include "console-component.h"
#include "util.h"

typedef struct {
	FulmStorage *storage;
	FulmDataManager *data_manager;
	FulmCompositeComponent* composite_component;
	FulmCompositeComponent* another_container;
} TestContext;



static void
context_test_pin_connection (TestContext* context, gconstpointer user_data) 
{
	g_debug("Testing pin connection...");

	g_debug("Get root container");
	FulmCompositeComponent *container = context->composite_component;

	g_debug("Build source component");
	FulmComponent* source_component = fulm_data_manager_create_component(context->data_manager, fulm_constant_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSTANT_COMPONENT(source_component));
	const gchar* source_component_key =  fulm_component_get_key(source_component);

	g_debug("Build target component");
	FulmComponent* target_component = fulm_data_manager_create_component(context->data_manager, fulm_console_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(target_component));
	const gchar* target_component_key =  fulm_component_get_key(target_component);
	
	g_debug("Get source pin");
	FulmComponentPin* source_pin = fulm_component_get_pin_by_name(source_component, FULM_COMPONENT_PIN_TYPE_OUTPUT, "value");
	g_assert_nonnull(source_pin);
	g_assert(FULM_IS_COMPONENT_PIN(source_pin));
	g_assert_cmpstr(fulm_component_pin_get_name(source_pin), ==, "value");

	g_debug("Get target pin");
	FulmComponentPin* target_pin = fulm_component_get_pin_by_name(target_component, FULM_COMPONENT_PIN_TYPE_INPUT, "log");
	g_assert_nonnull(target_pin);
	g_assert(FULM_IS_COMPONENT_PIN(target_pin));
	g_assert_cmpstr(fulm_component_pin_get_name(target_pin), ==, "log");

	GError* error = NULL;	
	gboolean res = fulm_data_manager_connect_pins(context->data_manager, source_pin, target_pin, &error);
	g_assert_no_error(error);
	g_assert_true(res);

	g_assert_cmpstr(fulm_component_pin_get_connected_component_key(source_pin), ==, target_component_key);
	g_assert_cmpstr(fulm_component_pin_get_connected_pin_name(source_pin), ==, "log");

	g_assert_cmpstr(fulm_component_pin_get_connected_component_key(target_pin), ==, source_component_key);
	g_assert_cmpstr(fulm_component_pin_get_connected_pin_name(target_pin), ==, "value");


	g_object_unref(source_component);
	g_object_unref(target_component);

}

static void
context_test_different_container(TestContext* context, gconstpointer user_data) 
{
	g_debug("Trying to connect a source component which is not a child of the composite component...");

	FulmCompositeComponent *container = context->composite_component;
	FulmCompositeComponent *container_2 = context->another_container;

	FulmComponent* source_component = fulm_data_manager_create_component(context->data_manager, fulm_constant_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSTANT_COMPONENT(source_component));

	FulmComponent* target_component = fulm_data_manager_create_component(context->data_manager, fulm_console_component_type_name(), container_2, NULL);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(target_component));
	//const gchar* target_component_key =  fulm_component_get_key(target_component);

	FulmComponentPin* source_pin = fulm_component_get_pin_by_name(source_component, FULM_COMPONENT_PIN_TYPE_OUTPUT, "value");
	g_assert_nonnull(source_pin);
	g_assert(FULM_IS_COMPONENT_PIN(source_pin));
	g_assert_cmpstr(fulm_component_pin_get_name(source_pin), ==, "value");

	FulmComponentPin* target_pin = fulm_component_get_pin_by_name(target_component, FULM_COMPONENT_PIN_TYPE_INPUT, "log");
	g_assert_nonnull(target_pin);
	g_assert(FULM_IS_COMPONENT_PIN(target_pin));
	g_assert_cmpstr(fulm_component_pin_get_name(target_pin), ==, "log");


	GError* error = NULL;	
	gboolean res = fulm_data_manager_connect_pins(context->data_manager, source_pin, target_pin, &error);
	g_assert_error(error, FULM_TYPE_DATA_MANAGER_ERROR, FULM_DATA_MANAGER_ERROR_DIFFERENT_CONTAINER);
	g_assert_false(res);

}



static void
context_test_source_pin_already_connected (TestContext* context, gconstpointer user_data) 
{
	g_debug("Trying to connect a source pin which is already connected...");

	g_debug("Get root container");
	FulmCompositeComponent *container = context->composite_component;

	g_debug("Build source component");
	FulmComponent* source_component = fulm_data_manager_create_component(context->data_manager, fulm_constant_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSTANT_COMPONENT(source_component));

	g_debug("Build target component");
	FulmComponent* target_component = fulm_data_manager_create_component(context->data_manager, fulm_console_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(target_component));
	
	g_debug("Get source pin");
	FulmComponentPin* source_pin = fulm_component_get_pin_by_name(source_component, FULM_COMPONENT_PIN_TYPE_OUTPUT, "value");
	g_assert(FULM_IS_COMPONENT_PIN(source_pin));

	g_debug("Get target pin");
	FulmComponentPin* target_pin = fulm_component_get_pin_by_name(target_component, FULM_COMPONENT_PIN_TYPE_INPUT, "log");
	g_assert(FULM_IS_COMPONENT_PIN(target_pin));

	GError* error = NULL;	
	gboolean res = fulm_data_manager_connect_pins(context->data_manager, source_pin, target_pin, &error);
	g_assert_no_error(error);
	g_assert_true(res);


	g_debug("Create a second target component");
	FulmComponent* target_component_2 = fulm_data_manager_create_component(context->data_manager, fulm_console_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(target_component_2));
	g_debug("Get input pin of second component");
	FulmComponentPin* target_pin_2 = fulm_component_get_pin_by_name(target_component_2, FULM_COMPONENT_PIN_TYPE_INPUT, "log");
	g_assert(FULM_IS_COMPONENT_PIN(target_pin_2));

	g_debug("Try to connect source pin to another pin");
	res = fulm_data_manager_connect_pins(context->data_manager, source_pin, target_pin_2, &error);
	g_assert_error(error, FULM_TYPE_DATA_MANAGER_ERROR, FULM_DATA_MANAGER_ERROR_PIN_ALREADY_CONNECTED);
	g_assert_false(res);

	g_object_unref(source_component);
	g_object_unref(target_component_2);
	g_object_unref(target_component);

}

static void
context_test_target_pin_already_connected (TestContext* context, gconstpointer user_data) 
{
	g_debug("Trying to connect a target pin which is already connected...");


	g_debug("Get root container");
	FulmCompositeComponent *container = context->composite_component;

	g_debug("Build source component");
	FulmComponent* source_component = fulm_data_manager_create_component(context->data_manager, fulm_constant_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSTANT_COMPONENT(source_component));

	g_debug("Build target component");
	FulmComponent* target_component = fulm_data_manager_create_component(context->data_manager, fulm_console_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(target_component));
	
	g_debug("Get source pin");
	FulmComponentPin* source_pin = fulm_component_get_pin_by_name(source_component, FULM_COMPONENT_PIN_TYPE_OUTPUT, "value");
	g_assert(FULM_IS_COMPONENT_PIN(source_pin));

	g_debug("Get target pin");
	FulmComponentPin* target_pin = fulm_component_get_pin_by_name(target_component, FULM_COMPONENT_PIN_TYPE_INPUT, "log");
	g_assert(FULM_IS_COMPONENT_PIN(target_pin));

	GError* error = NULL;	
	gboolean res = fulm_data_manager_connect_pins(context->data_manager, source_pin, target_pin, &error);
	g_assert_no_error(error);
	g_assert_true(res);


	g_debug("Create a second source component");
	FulmComponent* source_component_2 = fulm_data_manager_create_component(context->data_manager, fulm_constant_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSTANT_COMPONENT(source_component_2));
	g_debug("Get output pin of second component");
	FulmComponentPin* source_pin_2 = fulm_component_get_pin_by_name(source_component_2, FULM_COMPONENT_PIN_TYPE_OUTPUT, "value");
	g_assert(FULM_IS_COMPONENT_PIN(source_pin_2));

	g_debug("Try to connect target pin to another pin");
	res = fulm_data_manager_connect_pins(context->data_manager, source_pin_2, target_pin, &error);
	g_assert_error(error, FULM_TYPE_DATA_MANAGER_ERROR, FULM_DATA_MANAGER_ERROR_PIN_ALREADY_CONNECTED);
	g_assert_false(res);

	g_object_unref(source_component);
	g_object_unref(source_component_2);
	g_object_unref(target_component);

}

void
context_test_persistence(void)
{
	FulmStorage* storage;
	FulmDataManager* data_manager;
	FulmComponent* root_component;
	FulmCompositeComponent* container;
	FulmComponent* source_component;
	FulmComponent* target_component;
	FulmComponentPin* source_pin;
	FulmComponentPin* target_pin;

	g_debug("Checking pin connection persistence...");

	g_debug("Build and connnect to components");
	storage = fulm_testsuite_util_build_storage(/*delete_previous_file*/ TRUE);
	g_assert_true(FULM_IS_GDBM_STORAGE(storage));

	data_manager = fulm_data_manager_new(storage);
	g_assert_true(FULM_IS_DATA_MANAGER(data_manager));

	root_component = FULM_COMPONENT(fulm_data_manager_get_root_component(data_manager, NULL));
	g_assert_nonnull(root_component);

	container = FULM_COMPOSITE_COMPONENT(fulm_data_manager_create_component(data_manager, 
		fulm_composite_component_type_name(), 
		FULM_COMPOSITE_COMPONENT(root_component),
		NULL));
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(container));


	source_component = fulm_data_manager_create_component(data_manager, fulm_constant_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSTANT_COMPONENT(source_component));

	target_component = fulm_data_manager_create_component(data_manager, fulm_console_component_type_name(), container, NULL);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(target_component));

	source_pin = fulm_component_get_pin_by_name(source_component, FULM_COMPONENT_PIN_TYPE_OUTPUT, "value");
	g_assert(FULM_IS_COMPONENT_PIN(source_pin));

	target_pin = fulm_component_get_pin_by_name(target_component, FULM_COMPONENT_PIN_TYPE_INPUT, "log");
	g_assert(FULM_IS_COMPONENT_PIN(target_pin));


	g_assert_true(fulm_data_manager_connect_pins(data_manager, source_pin, target_pin, NULL));

	g_object_unref(source_component);
	g_object_unref(target_component);
	g_object_unref(data_manager);
	fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ FALSE);


	g_debug("Reload and check pin connections");

	storage = fulm_testsuite_util_build_storage(/*delete_previous_file*/ FALSE);
	g_assert_true(FULM_IS_GDBM_STORAGE(storage));

	data_manager = fulm_data_manager_new(storage);
	g_assert_true(FULM_IS_DATA_MANAGER(data_manager));

	root_component = FULM_COMPONENT(fulm_data_manager_get_root_component(data_manager, NULL));
	g_assert_nonnull(root_component);


	gchar** component_keys = fulm_composite_component_get_child_component_keys(FULM_COMPOSITE_COMPONENT(root_component));
	g_assert_nonnull(component_keys);
	gchar* container_key = component_keys[0];
	g_assert_nonnull(container_key);
	g_free(component_keys);


	GError* error = NULL;
	container = FULM_COMPOSITE_COMPONENT(fulm_data_manager_load_component(data_manager, container_key, &error));
	g_assert_no_error(error);
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(container));


	component_keys = fulm_composite_component_get_child_component_keys(FULM_COMPOSITE_COMPONENT(container));
	g_assert_nonnull(component_keys);
	gchar* source_component_key = component_keys[0];
	g_assert_nonnull(source_component_key);
	gchar* target_component_key = component_keys[1];
	g_assert_nonnull(target_component_key);
	g_free(component_keys);

	source_component = fulm_data_manager_load_component(data_manager, source_component_key, &error);
	g_assert_no_error(error);
	g_assert_true(FULM_IS_CONSTANT_COMPONENT(source_component));

	target_component = fulm_data_manager_load_component(data_manager, target_component_key, &error);
	g_assert_no_error(error);
	g_assert_true(FULM_IS_CONSOLE_COMPONENT(target_component));

	source_pin = fulm_component_get_pin_by_name(source_component, FULM_COMPONENT_PIN_TYPE_OUTPUT, "value");
	g_assert_true(FULM_IS_COMPONENT_PIN(source_pin));
	target_pin = fulm_component_get_pin_by_name(target_component, FULM_COMPONENT_PIN_TYPE_INPUT, "log");
	g_assert_true(FULM_IS_COMPONENT_PIN(target_pin));

	g_assert_cmpstr(fulm_component_pin_get_connected_component_key(source_pin), ==, target_component_key);
	g_assert_cmpstr(fulm_component_pin_get_connected_pin_name(source_pin), ==, "log");

	g_assert_cmpstr(fulm_component_pin_get_connected_component_key(target_pin), ==, source_component_key);
	g_assert_cmpstr(fulm_component_pin_get_connected_pin_name(target_pin), ==, "value");

	g_object_unref(data_manager);
	fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ FALSE);
}

static void
context_setup (TestContext* context, gconstpointer user_data)
{
	g_debug("Building test context...");

	context->storage = fulm_testsuite_util_build_storage(/*delete_previous_file*/ TRUE);
	g_assert_nonnull(context->storage);
	g_assert_true(FULM_IS_GDBM_STORAGE(context->storage));

	g_debug("build data-manager");
	context->data_manager = fulm_data_manager_new(context->storage);
	g_assert_nonnull(context->data_manager);
	g_assert_true(FULM_IS_DATA_MANAGER(context->data_manager));

	g_debug("get root-component");
	FulmComponent* root_component = FULM_COMPONENT(fulm_data_manager_get_root_component(context->data_manager, NULL));
	g_assert_nonnull(root_component);

	g_debug("create composite-component");
	context->composite_component = FULM_COMPOSITE_COMPONENT(fulm_data_manager_create_component(context->data_manager, 
		fulm_composite_component_type_name(), 
		FULM_COMPOSITE_COMPONENT(root_component),
		NULL));
	g_assert_nonnull(context->composite_component);
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(context->composite_component));
	g_assert_nonnull(fulm_component_get_key(FULM_COMPONENT(context->composite_component)));
	g_assert_cmpstr(fulm_component_get_parent_key(FULM_COMPONENT(context->composite_component)), ==, "#root");

	g_debug("create another composite-component");
	context->another_container = FULM_COMPOSITE_COMPONENT(fulm_data_manager_create_component(context->data_manager, 
		fulm_composite_component_type_name(), 
		FULM_COMPOSITE_COMPONENT(root_component),
		NULL));
	g_assert_nonnull(context->another_container);
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(context->another_container));


	g_debug("Test context built");
}


static void
context_teardown(TestContext* context, gconstpointer user_data)
{
	g_object_unref(context->another_container);
	g_object_unref(context->composite_component);
	g_object_unref(context->data_manager);
	fulm_testsuite_util_terminate_storage(context->storage, /*delete_file*/ TRUE);
}




int
main(int argc, char** argv) 
{
	g_test_init(&argc, &argv, NULL);
	g_test_add("/pin-connection-test/connection", TestContext, NULL, context_setup, context_test_pin_connection, context_teardown);

	g_test_add("/pin-connection-test/different_container", 
		TestContext, NULL, context_setup, context_test_different_container, context_teardown);

	g_test_add("/pin-connection-test/source_pin_already_connected", 
		TestContext, NULL, context_setup, context_test_source_pin_already_connected, context_teardown);
	g_test_add("/pin-connection-test/target_pin_already_connected", 
		TestContext, NULL, context_setup, context_test_target_pin_already_connected, context_teardown);

	g_test_add_func("/pin-connection-test/persistence", context_test_persistence);

	return g_test_run();
}
