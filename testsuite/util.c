#include "util.h"

#ifdef G_OS_WIN32
#include <fileapi.h> // Windows file attribute SetFileAttributes
#endif


static gchar* context_test_get_storage_path()
{
	return g_build_filename(g_get_tmp_dir(), "fulmicoton-testsuite.db", NULL);
}

FulmStorage*
fulm_testsuite_util_build_storage(gboolean delete_previous_file)
{
	GError* error = NULL;
	gboolean result;
	gchar* db_path = context_test_get_storage_path();
	g_print("DB Path : %s\n", db_path);
	GFile* db_file =  g_file_new_for_path(db_path);

	// delete temporary DB file if it was not deleted in a previous test case
	if (delete_previous_file)
	{
		if (g_file_query_exists(db_file, NULL)) {
#ifdef G_OS_WIN32
			// on Windows, read file attribute is set when file is not closed properly
			SetFileAttributes(db_path, FILE_ATTRIBUTE_NORMAL); 
#endif
			result = g_file_delete(db_file, NULL, &error);
			g_assert_no_error(error);
			g_assert_true(result);
		}
	}
	g_free(db_path);
	FulmStorage* storage = fulm_storage_new(FULM_STORAGE_TYPE_GDBM, db_file, NULL);
	g_object_unref(db_file);
	fulm_storage_initialize(storage, NULL);
	g_assert_true(fulm_storage_open(storage, NULL));
	return FULM_STORAGE(storage);
}

void
fulm_testsuite_util_terminate_storage(FulmStorage* storage, gboolean delete_file)
{
	GError* error = NULL;
	gboolean result;
	g_assert(FULM_IS_STORAGE(storage));
	g_assert_true(fulm_storage_close(storage, NULL));
	if (delete_file){
		gchar* db_path = context_test_get_storage_path();
		GFile* db_file =  g_file_new_for_path(db_path);
		if (g_file_query_exists(db_file, NULL)) {
#ifdef G_OS_WIN32
			// on Windows, read file attribute is set when file is not closed properly
			SetFileAttributes(db_path, FILE_ATTRIBUTE_NORMAL); 
#endif
			result = g_file_delete(db_file, NULL, &error);
			g_assert_no_error(error);
			g_assert_true(result);
		}
		g_object_unref(db_file);
		g_free(db_path);
	}
	g_object_unref(storage);
}
