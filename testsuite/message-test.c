#include <glib.h>
#include "message.h"
#include "message-manager.h"
#include "util.h"


#define CAPTURE G_TEST_SUBPROCESS_INHERIT_STDOUT | G_TEST_SUBPROCESS_INHERIT_STDERR

typedef struct {
} TestContext;


static void
context_test_init(TestContext* context, gconstpointer user_data) 
{
	FulmMessage* message;
	g_print("\nTesting Message object initialization...\n");

	message = fulm_message_new("msg1");
	g_assert_nonnull(message);
	g_assert_true(FULM_IS_MESSAGE(message));
	
	g_assert_cmpstr(fulm_message_get_key(message), ==, "msg1");
	g_assert_null(fulm_message_get_component_key(message));
	g_assert_null(fulm_message_get_pin_name(message));
	g_assert_null(fulm_message_get_value(message));

	g_object_unref(message);


	message = fulm_message_new_full("msg2", "#624", "input", "a_value");
	g_assert_nonnull(message);
	g_assert_true(FULM_IS_MESSAGE(message));
	
	g_assert_cmpstr(fulm_message_get_key(message), ==, "msg2");
	g_assert_cmpstr(fulm_message_get_component_key(message), ==, "#624");
	g_assert_cmpstr(fulm_message_get_pin_name(message), ==, "input");
	g_assert_cmpstr(fulm_message_get_value(message), ==, "a_value");

	g_object_unref(message);
}


static void
context_test_json(TestContext* context, gconstpointer user_data) 
{
	gchar *json_str;
	g_print("\nSaving message to json...\n");

	FulmMessage* my_message = fulm_message_new_full("msg1", "#624", "input", "a_value");

	JsonBuilder *json_builder = json_builder_new();
	fulm_message_save_to_json(my_message, json_builder);

	JsonNode* json_node = json_builder_get_root(json_builder);
	JsonGenerator *json_generator = json_generator_new();
	json_generator_set_root(json_generator, json_node);
	json_str = json_generator_to_data (json_generator, NULL);
	g_print("json=%s\n\n", json_str);

	g_object_unref(json_generator);
	g_object_unref(json_builder);
	g_object_unref(my_message);

	const gchar* attempted_json = "{\"key\":\"msg1\",\"ckey\":\"#624\",\"pin\":\"input\",\"val\":\"a_value\"}";
	g_assert_cmpstr(json_str, ==, attempted_json);


	g_print("\nLoading message from json...\n");
	my_message = fulm_message_new("msg1");
	g_assert_nonnull(my_message);
	g_assert_true(FULM_IS_MESSAGE(my_message));

	JsonParser* json_parser = json_parser_new();
	g_assert(json_parser_load_from_data(json_parser, json_str, -1, NULL));
	json_node = json_parser_get_root (json_parser);
	JsonReader* json_reader = json_reader_new(json_node);
	fulm_message_load_from_json(my_message, json_reader);

	g_assert_cmpstr(fulm_message_get_key(my_message), ==, "msg1"); 
	g_assert_cmpstr(fulm_message_get_component_key(my_message), ==, "#624"); 
	g_assert_cmpstr(fulm_message_get_pin_name(my_message), ==, "input"); 
	g_assert_cmpstr(fulm_message_get_value(my_message), ==, "a_value"); 

	g_object_unref(json_reader);
	g_object_unref(json_parser);

	g_object_unref(my_message);

	g_free(json_str);
}


static void
context_test_storage(TestContext* context, gconstpointer user_data) 
{
	GError* error = NULL;
	FulmStorage* storage;
	gboolean result;

	g_print("\nSaving message component to storage...\n");
	storage = fulm_testsuite_util_build_storage(TRUE);

	FulmMessage* my_message = fulm_message_new_full("msg2", "#325", "input", "a_test_value");

	result = fulm_storable_store(FULM_STORABLE(my_message), storage, &error);
	g_assert_no_error(error);
	g_assert_true(result);
	g_print("done\n");

	g_print("Checking stored json...\n");
	gchar* json = NULL;
	result = fulm_storage_fetch_string(storage, "msg2", &json, &error);
	g_assert_no_error(error);
	g_assert_true(result);
	g_print("Stored JSON=[%s]\n", json);
	const gchar* attempted_json = "{\"key\":\"msg2\",\"ckey\":\"#325\",\"pin\":\"input\",\"val\":\"a_test_value\"}";
	g_assert_nonnull(json);
	g_assert_cmpstr(json, ==, attempted_json);

	fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ FALSE);
	g_object_unref(my_message);


	g_print("\nLoading message from storage...\n");
	storage = fulm_testsuite_util_build_storage(FALSE);
	my_message = fulm_message_new("msg2");

	result = fulm_storable_fetch(FULM_STORABLE(my_message), storage, &error);
	g_assert_no_error(error);
	g_assert_true(result);

	g_assert_cmpstr(fulm_message_get_key(my_message), ==, "msg2");
	g_assert_cmpstr(fulm_message_get_component_key(my_message), ==, "#325");
	g_assert_cmpstr(fulm_message_get_pin_name(my_message), ==, "input");
	g_assert_cmpstr(fulm_message_get_value(my_message), ==, "a_test_value");

	g_object_unref(my_message);
	fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ TRUE);
}


static void
context_message_test_init (TestContext* context, gconstpointer user_data) 
{
	//GError* error = NULL;
	//gboolean result;
	FulmStorage* storage;
	FulmMessage* message;
	gchar** key_array;
	const gchar* first_key;

	g_print("\nPosting in message manager...\n");
	storage = fulm_testsuite_util_build_storage(TRUE);
	FulmMessageManager* message_manager = fulm_message_manager_new(storage);
	g_assert_nonnull(message_manager);
	g_assert_true(FULM_IS_MESSAGE_MANAGER(message_manager));

	g_assert_cmpint(fulm_message_manager_get_message_count(message_manager), ==, 0);

	g_print("Post first message\n");
	message = fulm_message_manager_post_message(message_manager, "#321", "input", "a_value");
	g_assert(message != NULL);
	g_assert_true(FULM_IS_MESSAGE(message));
	g_assert_cmpstr(fulm_message_get_key(message), ==, "msg#1");
	g_assert_cmpstr(fulm_message_get_component_key(message), ==, "#321");
	g_assert_cmpstr(fulm_message_get_pin_name(message), ==, "input");
	g_assert_cmpstr(fulm_message_get_value(message), ==, "a_value");
	g_object_unref(message);

	g_assert_cmpint(fulm_message_manager_get_message_count(message_manager), ==, 1);

	key_array = fulm_message_manager_get_message_keys(message_manager);
	g_assert_nonnull(key_array);
	g_assert_cmpstr(key_array[0], ==, "msg#1");
	g_assert_null(key_array[1]);
	g_free(key_array);

	g_assert_cmpstr(fulm_message_manager_get_message_key_by_index(message_manager, 0), ==, "msg#1");
	g_assert_null(fulm_message_manager_get_message_key_by_index(message_manager, 1));

	first_key = fulm_message_manager_get_first_message_key(message_manager);
	g_assert_nonnull(first_key);
	g_assert_cmpstr(first_key, ==, "msg#1");


	g_print("Post second message\n");
	message = fulm_message_manager_post_message(message_manager, "#267", "top", "true");
	g_assert(message != NULL);
	g_assert_true(FULM_IS_MESSAGE(message));
	g_assert_cmpstr(fulm_message_get_key(message), ==, "msg#2");
	g_assert_cmpstr(fulm_message_get_component_key(message), ==, "#267");
	g_assert_cmpstr(fulm_message_get_pin_name(message), ==, "top");
	g_assert_cmpstr(fulm_message_get_value(message), ==, "true");
	g_object_unref(message);

	g_assert_cmpint(fulm_message_manager_get_message_count(message_manager), ==, 2);

	key_array = fulm_message_manager_get_message_keys(message_manager);
	g_assert_nonnull(key_array);
	g_assert_cmpstr(key_array[0], ==, "msg#1");
	g_assert_cmpstr(key_array[1], ==, "msg#2");
	g_assert_null(key_array[2]);
	g_free(key_array);

	g_assert_cmpstr(fulm_message_manager_get_message_key_by_index(message_manager, 0), ==, "msg#1");
	g_assert_cmpstr(fulm_message_manager_get_message_key_by_index(message_manager, 1), ==, "msg#2");
	g_assert_null(fulm_message_manager_get_message_key_by_index(message_manager, 2));

	first_key = fulm_message_manager_get_first_message_key(message_manager);
	g_assert_nonnull(first_key);
	g_assert_cmpstr(first_key, ==, "msg#1");

	g_object_unref(message_manager);
	fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ TRUE);

}

static void
context_message_manager_test_json(TestContext* context, gconstpointer user_data) 
{
	FulmStorage* storage;
	FulmMessage* message;
	FulmMessageManager* message_manager;
	gchar** key_array;

	g_print("\nSaving message manager to json...\n");
	storage = fulm_testsuite_util_build_storage(TRUE);
	message_manager = fulm_message_manager_new(storage);

	message = fulm_message_manager_post_message(message_manager, "#321", "input", "a_value");
	g_object_unref(message);
	message = fulm_message_manager_post_message(message_manager, "#322", "timerr", "625");
	g_object_unref(message);

	g_assert_cmpint(fulm_message_manager_get_message_count(message_manager), ==, 2);


	JsonBuilder *json_builder = json_builder_new();
	fulm_message_manager_save_to_json(FULM_MESSAGE_MANAGER(message_manager), json_builder);

	JsonNode* json_node = json_builder_get_root(json_builder);
	JsonGenerator *json_generator = json_generator_new();
	json_generator_set_root(json_generator, json_node);
	gchar *json_str = json_generator_to_data (json_generator, NULL);
	g_print("json=%s\n\n", json_str);
	g_object_unref(json_generator);
	g_object_unref(json_builder);

	const gchar* attempted_json = "{\"msg_keys\":[\"msg#1\",\"msg#2\"]}";
	g_assert_cmpstr(json_str, ==, attempted_json);

	fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ TRUE);



	g_print("\nLoading message manager from json...\n");
	storage = fulm_testsuite_util_build_storage(TRUE);
	message_manager = fulm_message_manager_new(storage);
	g_assert_cmpint(fulm_message_manager_get_message_count(message_manager), ==, 0);

	JsonParser* json_parser = json_parser_new();
	g_assert(json_parser_load_from_data(json_parser, json_str, -1, NULL));
	json_node = json_parser_get_root (json_parser);
	JsonReader* json_reader = json_reader_new(json_node);
	fulm_message_manager_load_from_json(message_manager, json_reader);

	g_assert_cmpint(fulm_message_manager_get_message_count(message_manager), ==, 2);

	key_array = fulm_message_manager_get_message_keys(message_manager);
	g_assert_nonnull(key_array);
	g_assert_cmpstr(key_array[0], ==, "msg#1");
	g_assert_cmpstr(key_array[1], ==, "msg#2");
	g_assert_null(key_array[2]);
	g_free(key_array);

	g_free(json_str);
	g_object_unref(message_manager);
	fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ TRUE);


}


static void
context_message_manager_test_storage(TestContext* context, gconstpointer user_data) 
{
/*
	GError* error = NULL;
	FulmStorage* storage;
	gboolean result;

	g_print("\nSaving message component to storage...\n");
	storage = fulm_testsuite_util_build_storage(TRUE);

	FulmMessage* my_message = fulm_message_new_full("msg2", "#325", "input", "a_test_value");

	result = fulm_storable_store(FULM_STORABLE(my_message), storage, &error);
	g_assert_no_error(error);
	g_assert_true(result);
	g_print("done\n");

	g_print("Checking stored json...\n");
	gchar* json = NULL;
	result = fulm_storage_fetch_string(storage, "msg2", &json, &error);
	g_assert_no_error(error);
	g_assert_true(result);
	g_print("Stored JSON=[%s]\n", json);
	const gchar* attempted_json = "{\"key\":\"msg2\",\"ckey\":\"#325\",\"pin\":\"input\",\"val\":\"a_test_value\"}";
	g_assert_nonnull(json);
	g_assert_cmpstr(json, ==, attempted_json);

*/
	//fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ FALSE);
/*
	g_object_unref(my_message);


	g_print("\nLoading message from storage...\n");
	storage = fulm_testsuite_util_build_storage(FALSE);
	my_message = fulm_message_new("msg2");

	result = fulm_storable_fetch(FULM_STORABLE(my_message), storage, &error);
	g_assert_no_error(error);
	g_assert_true(result);

	g_assert_cmpstr(fulm_message_get_key(my_message), ==, "msg2");
	g_assert_cmpstr(fulm_message_get_component_key(my_message), ==, "#325");
	g_assert_cmpstr(fulm_message_get_pin_name(my_message), ==, "input");
	g_assert_cmpstr(fulm_message_get_value(my_message), ==, "a_test_value");

	g_object_unref(my_message);
*/
	//fulm_testsuite_util_terminate_storage(storage, /*delete_file*/ TRUE);
}

static void
context_setup (TestContext* context, gconstpointer user_data)
{
}


static void
context_teardown(TestContext* context, gconstpointer user_data)
{
}


int
main(int argc, char** argv) 
{
	g_test_init(&argc, &argv, NULL);
	g_test_add("/message-test/init", TestContext, NULL, context_setup, context_test_init, context_teardown);
	g_test_add("/message-test/json", TestContext, NULL, context_setup, context_test_json, context_teardown);
	g_test_add("/message-test/storage", TestContext, NULL, context_setup, context_test_storage, context_teardown);
	g_test_add("/message-manager-test/init", TestContext, NULL, context_setup, context_message_test_init, context_teardown);
	g_test_add("/message-manager-test/json", TestContext, NULL, context_setup, context_message_manager_test_json, context_teardown);
	g_test_add("/message-manager-test/storage", TestContext, NULL, context_setup, context_message_manager_test_storage, context_teardown);

	return g_test_run();
}
