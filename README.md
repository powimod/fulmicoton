# Fulmicoton


## General information

*Fulmicoton* is an effort to produce a workflow application to automate processes. 

Automation will be done by connecting components together.

The name *Fulmicoton* comes from the novel *Voyage au centre de la terre* by the french writer *Jules Vernes* where 
the hero decides to blow up the obstacle that blocks the cave using this explosive !

Authors :

  - Dominique Parisot


Official GitLab :

  - https://gitlab.gnome.org/powimod/fulmicoton.git



## Building

In order to compile *Fulmicoton* you will need :

 - GCC Compiler
 - Meson
 - Ninja 

And the following dependencies : 

 - GLib
 - GDMB Lib
 
To compile :

```sh
$ meson builddir .
$ cd builddir
$ ninja
```

To run test suite : 
```sh
$ meson test
```

## Licensing terms

*Fulmicoton* is released under the terms of the GNU Lesser General Public License,
version 3 or, at your option, any later version, as published by the Free
Software Foundation.

Please, see the [`COPYING`](./COPYING) file for further information.


