/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:fulm_engine
 * @Title: FulmEngine
 * @short_description:
 *
 * #FulmEngine is an abstract class for all engines which can be used in Fulmicoton.
 *
 */

#include "engine.h"


typedef struct {
	FulmDataManager* data_manager;
	FulmMessageManager* message_manager;
} FulmEnginePrivate;

static void fulm_engine_edition_observer_init(FulmEditionObserverInterface* interface);

G_DEFINE_TYPE_WITH_CODE(FulmEngine, fulm_engine, G_TYPE_OBJECT,
	G_ADD_PRIVATE(FulmEngine)
	G_IMPLEMENT_INTERFACE(FULM_TYPE_EDITION_OBSERVER, fulm_engine_edition_observer_init)
);


static void
fulm_engine_init(FulmEngine* engine)
{
	FulmEnginePrivate* priv = fulm_engine_get_instance_private(engine);
	priv->data_manager = NULL;
	priv->message_manager = NULL;
}

static void
fulm_engine_dispose(GObject* object)
{
	g_return_if_fail(FULM_IS_ENGINE(object));
	FulmEngine* self = FULM_ENGINE(object);
	FulmEnginePrivate *priv = fulm_engine_get_instance_private(self);
	if (priv->data_manager != NULL)
		g_object_unref(priv->data_manager);
	if (priv->message_manager != NULL)
		g_object_unref(priv->message_manager);
	G_OBJECT_CLASS(fulm_engine_parent_class)->dispose(object);
}


static void
fulm_engine_finalize(GObject* object)
{
	//FulmEngine* engine = FULM_ENGINE(object);
	//FulmEnginePrivate *priv = fulm_engine_get_instance_private(engine);
	G_OBJECT_CLASS(fulm_engine_parent_class)->finalize(object);
}


static void
fulm_engine_class_init(FulmEngineClass* engine_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(engine_class);
	object_class->dispose = fulm_engine_dispose;
	object_class->finalize = fulm_engine_finalize;
}




FulmEngine*
fulm_engine_new(FulmDataManager * data_manager, FulmMessageManager* message_manager)
{
	g_assert(FULM_IS_DATA_MANAGER(data_manager));
	g_assert(FULM_IS_MESSAGE_MANAGER(message_manager));
	FulmEngine* self = g_object_new(FULM_TYPE_ENGINE, NULL);
	FulmEnginePrivate* priv = fulm_engine_get_instance_private(self);
	priv->data_manager = data_manager;
	priv->message_manager = message_manager;
	fulm_data_manager_set_edition_observer(data_manager, FULM_EDITION_OBSERVER(self));
	g_object_ref(priv->data_manager);
	g_object_ref(priv->message_manager);
	return self;
}

static void fulm_engine_edition_observer_component_created(FulmEditionObserver* edition_observer, FulmComponent* component)
{
	g_error("not implemented");
}

static void fulm_engine_edition_observer_component_deleted(FulmEditionObserver* edition_observer, FulmComponent* component)
{
	g_error("not implemented");
}
static void fulm_engine_edition_observer_pins_connected (FulmEditionObserver* edition_observer, FulmComponentPin* source_pin, FulmComponentPin* target_pin)
{
	g_error("not implemented");
}

static void fulm_engine_edition_observer_pins_disconnected (FulmEditionObserver* edition_observer, FulmComponentPin* source_pin, FulmComponentPin* target_pin)
{
	g_error("not implemented");
}

static void
fulm_engine_edition_observer_init(FulmEditionObserverInterface* interface)
{
	interface->component_created = fulm_engine_edition_observer_component_created;
	interface->component_deleted = fulm_engine_edition_observer_component_deleted;
	interface->pins_connected = fulm_engine_edition_observer_pins_connected;
	interface->pins_disconnected = fulm_engine_edition_observer_pins_disconnected;

}
