/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __FULM_CONSOLE_COMPONENT__
#define __FULM_CONSOLE_COMPONENT__

#include <glib-object.h>
#include "global.h"
#include "component.h"

#define FULM_TYPE_CONSOLE_COMPONENT fulm_console_component_get_type()

G_DECLARE_FINAL_TYPE(FulmConsoleComponent, fulm_console_component, FULM, CONSOLE_COMPONENT, FulmComponent);

FulmComponent* fulm_console_component_new();

void fulm_console_component_log(FulmConsoleComponent* console_component, const gchar* message);

const gchar* fulm_console_component_type_name();

#endif /*__FULM_CONSOLE_COMPONENT__*/

