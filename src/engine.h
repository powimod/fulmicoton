/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FULM_ENGINE__
#define __FULM_ENGINE__

#include <glib-object.h>
#include "global.h"
#include "data-manager.h"
#include "message-manager.h"

#define FULM_TYPE_ENGINE fulm_engine_get_type()

G_DECLARE_DERIVABLE_TYPE (FulmEngine, fulm_engine, FULM, ENGINE, GObject);

struct _FulmEngineClass
{
	GObjectClass parent_class;
};

FulmEngine* fulm_engine_new(FulmDataManager * data_manager, FulmMessageManager* message_manager);


#endif /*__FULM_ENGINE__*/
