/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:fulm_component
 * @Title: FulmComponent
 * @short_description:
 *
 * #FulmComponent is an abstract class for all components which can be used in Fulmicoton.
 *
 */

#include "component.h"

#define _TYPE_FIELD "type"
#define _NAME_FIELD "name"
#define _KEY_FIELD "key"
#define _PARENT_KEY_FIELD "pkey"
#define _PIN_CONNECTION_LIST_FIELD "cnx"
#define _PIN_FIELD "pin"

const gchar* fulm_component_empty_pin_array [] = { NULL };
static const gint pin_type_array[] = {FULM_COMPONENT_PIN_TYPE_INPUT, FULM_COMPONENT_PIN_TYPE_OUTPUT, FULM_COMPONENT_PIN_TYPE_HIDDEN, -1};
static const gchar* pin_type_letter_array[] = {"i", "o", "h", NULL};

typedef struct {
	gchar* key;
	gchar* name;
	gchar* parent_key;
} FulmComponentPrivate;


static void fulm_component_storable_interface_init(FulmStorableInterface *interface);
static void fulm_component_real_load_from_json (FulmComponent* component, JsonReader *json_reader);
static void fulm_component_real_save_to_json (FulmComponent* self, JsonBuilder *json_builder);

static const gchar** fulm_component_real_get_pin_names(FulmComponent* component, FulmComponentPinType pin_type);
static gint fulm_component_real_get_pin_count(FulmComponent* component, FulmComponentPinType pin_type);
static FulmComponentPin* fulm_component_real_get_pin_by_index (FulmComponent* component, FulmComponentPinType pin_type, gint pin_index);
static FulmComponentPin* fulm_component_real_get_pin_by_name (FulmComponent* component, FulmComponentPinType pin_type, const gchar* pin_name);


static gboolean fulm_component_storable_real_store (FulmStorable* storable, FulmStorage* storage, GError** error);
static gboolean fulm_component_storable_real_fetch (FulmStorable* storable, FulmStorage* storage, GError** error);

static GError* fulm_component_real_validate_pin_value(FulmComponent* component, FulmComponentPin* pin, const gchar* value);
static GError* fulm_component_real_process_pin_update (FulmComponent* component, FulmComponentPin* pin);


G_DEFINE_TYPE_WITH_CODE(FulmComponent, fulm_component, G_TYPE_OBJECT,
	G_ADD_PRIVATE(FulmComponent)
	G_IMPLEMENT_INTERFACE(FULM_TYPE_STORABLE, fulm_component_storable_interface_init)
);



static void
fulm_component_init(FulmComponent* component)
{
	FulmComponentPrivate* priv = fulm_component_get_instance_private(component);
	priv->key = NULL;
	priv->parent_key = NULL;
	priv->name = g_strdup("?");
}

static void
fulm_component_dispose(GObject* object)
{
	g_return_if_fail(FULM_IS_COMPONENT(object));
	G_OBJECT_CLASS(fulm_component_parent_class)->dispose(object);
}


static void
fulm_component_finalize(GObject* object)
{
	FulmComponent* component = FULM_COMPONENT(object);
	FulmComponentPrivate *priv = fulm_component_get_instance_private(component);
	if (priv->key != NULL)
		g_free(priv->key);
	if (priv->parent_key != NULL)
		g_free(priv->parent_key);
	g_free(priv->name);
	G_OBJECT_CLASS(fulm_component_parent_class)->finalize(object);
}


const gchar*
fulm_component_get_key(FulmComponent* component)
{
	g_assert_true(FULM_IS_COMPONENT(component));
	FulmComponentPrivate *priv = fulm_component_get_instance_private(component);
	return (const gchar*)priv->key;
}

void
fulm_component_set_key(FulmComponent* component, const gchar* key)
{
	g_assert_true(FULM_IS_COMPONENT(component));
	g_assert_nonnull(key);
	g_return_if_fail(g_utf8_strlen(key, -1) > 0);
	FulmComponentPrivate *priv = fulm_component_get_instance_private(component);
	g_assert(priv->key == NULL); // one-time set function
	priv->key = g_strdup(key);

}

const gchar*
fulm_component_get_parent_key(FulmComponent* component)
{
	g_assert_true(FULM_IS_COMPONENT(component));
	FulmComponentPrivate *priv = fulm_component_get_instance_private(component);
	// parent key can be NULL (root component)
	return (const gchar*)priv->parent_key;
}

void
fulm_component_set_parent_key(FulmComponent* component, const gchar* parent_key)
{
	g_assert_true(FULM_IS_COMPONENT(component));
	g_assert_nonnull(parent_key);
	g_return_if_fail(g_utf8_strlen(parent_key, -1) > 0);
	FulmComponentPrivate *priv = fulm_component_get_instance_private(component);
	g_assert(priv->parent_key == NULL); // one-time set function
	priv->parent_key = g_strdup(parent_key);
}



const gchar*
fulm_component_get_name(FulmComponent* component)
{
	FulmComponentPrivate *priv = fulm_component_get_instance_private(component);
	return (const gchar*)priv->name;
}

void
fulm_component_set_name(FulmComponent* component, const gchar* name)
{
	g_return_if_fail(name != NULL);
	g_return_if_fail(g_utf8_strlen(name, -1) > 0);
	FulmComponentPrivate *priv = fulm_component_get_instance_private(component);
	g_free(priv->name);
	priv->name = g_strdup(name);
}


static void
fulm_component_class_init(FulmComponentClass* component_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(component_class);
	object_class->dispose = fulm_component_dispose;
	object_class->finalize = fulm_component_finalize;
	component_class->get_type_name = NULL;
	component_class->load_from_json =  fulm_component_real_load_from_json;
	component_class->save_to_json =  fulm_component_real_save_to_json;

	component_class->get_pin_names = fulm_component_real_get_pin_names;
	component_class->get_pin_count = fulm_component_real_get_pin_count;
	component_class->get_pin_by_index = fulm_component_real_get_pin_by_index;
	component_class->get_pin_by_name = fulm_component_real_get_pin_by_name;

	component_class->storable_store = fulm_component_storable_real_store;
	component_class->storable_fetch = fulm_component_storable_real_fetch;

	component_class->validate_pin_value = fulm_component_real_validate_pin_value;
	component_class->process_pin_update = fulm_component_real_process_pin_update;
}



static gboolean
fulm_component_storable_real_store (FulmStorable* storable, FulmStorage* storage, GError** perror)
{
	g_return_val_if_fail(storable, FALSE);
	g_return_val_if_fail(FULM_IS_COMPONENT(storable), FALSE);
	g_return_val_if_fail(storage, FALSE);
	g_return_val_if_fail(FULM_IS_STORAGE(storage), FALSE);

	FulmComponent* self = FULM_COMPONENT(storable);
	JsonBuilder *json_builder = json_builder_new();
	fulm_component_save_to_json (self, json_builder);

	JsonNode* json_node = json_builder_get_root(json_builder);
	const gchar* key = fulm_component_get_key(FULM_COMPONENT(self)); 
	gboolean success = fulm_storage_store_json(storage, key, json_node , perror);

	json_node_unref(json_node);
	g_object_unref(json_builder);
	return success;
}

static gboolean
fulm_component_storable_store (FulmStorable* self, FulmStorage* storage, GError** perror)
{
        g_return_val_if_fail(FULM_IS_STORABLE(self), FALSE);
	g_return_val_if_fail(storage!= NULL, FALSE);
        g_return_val_if_fail(FULM_IS_STORAGE(storage), FALSE);
        FulmComponentClass *component_class = FULM_COMPONENT_GET_CLASS(self);
        g_return_val_if_fail(component_class->storable_store != NULL, FALSE);
        return component_class->storable_store(self, storage, perror);
}



static gboolean
fulm_component_storable_real_fetch (FulmStorable* storable, FulmStorage* storage, GError** perror)
{
	g_return_val_if_fail(storable, FALSE);
	g_return_val_if_fail(FULM_IS_COMPONENT(storable), FALSE);
	g_return_val_if_fail(storage, FALSE);
	g_return_val_if_fail(FULM_IS_STORAGE(storage), FALSE);


	FulmComponent* self = FULM_COMPONENT(storable);
	const gchar* key = fulm_component_get_key(FULM_COMPONENT(self)); 
	g_assert_nonnull(key);

	perror = NULL;
	JsonNode* json_node = NULL;
	if (!fulm_storage_fetch_json(storage, key, &json_node , perror))
		return FALSE;
	g_assert_nonnull(json_node);

	JsonReader* json_reader = json_reader_new(json_node);
	fulm_component_load_from_json (self, json_reader);

	return TRUE;
}

static gboolean
fulm_component_storable_fetch (FulmStorable* self, FulmStorage* storage, GError** error)
{
        g_return_val_if_fail(FULM_IS_STORABLE(self), FALSE);
	g_return_val_if_fail(storage!= NULL, FALSE);
        g_return_val_if_fail(FULM_IS_STORAGE(storage), FALSE);
        FulmComponentClass *component_class = FULM_COMPONENT_GET_CLASS(self);
        g_return_val_if_fail(component_class->storable_fetch != NULL, FALSE);
        return component_class->storable_fetch(self, storage, error);
}


static void
fulm_component_storable_interface_init(FulmStorableInterface *interface)
{
	interface->store = fulm_component_storable_store; 
	interface->fetch = fulm_component_storable_fetch; 
}


const gchar* fulm_component_get_type_name(FulmComponent* component)
{
	FulmComponentClass* component_class = FULM_COMPONENT_GET_CLASS(component);
	g_return_val_if_fail(component_class->get_type_name != NULL, NULL);
	return component_class->get_type_name(component);
}


static void
fulm_component_real_save_to_json (FulmComponent* self, JsonBuilder *json_builder)
{

	json_builder_set_member_name(json_builder, _KEY_FIELD);
	json_builder_add_string_value(json_builder, fulm_component_get_key(self));

	json_builder_set_member_name(json_builder, _TYPE_FIELD);
	json_builder_add_string_value(json_builder, fulm_component_get_type_name(self));

	json_builder_set_member_name(json_builder, _NAME_FIELD);
	json_builder_add_string_value(json_builder, fulm_component_get_name(self));

	const gchar* parent_key = fulm_component_get_parent_key(self);
	if (parent_key != NULL) {
		json_builder_set_member_name(json_builder, _PARENT_KEY_FIELD);
		json_builder_add_string_value(json_builder, parent_key);
	}

	// save pin connections
	json_builder_set_member_name(json_builder, _PIN_CONNECTION_LIST_FIELD);
	json_builder_begin_array(json_builder);
	for (gint i_pin_type = 0; pin_type_array[i_pin_type] != -1; i_pin_type++)
	{
		// FIXME use a fulm_component_get_pins function which returns directly ComponentPin object
		const gchar** pin_name_array = fulm_component_get_pin_names(self, pin_type_array[i_pin_type]);
		if (pin_name_array == NULL) 
			continue;
		for (gint i_pin = 0; pin_name_array[i_pin] != NULL; i_pin++){
			FulmComponentPin* pin = fulm_component_get_pin_by_index(self, pin_type_array[i_pin_type], i_pin);
			g_assert_true(FULM_IS_COMPONENT_PIN(pin));
			const gchar* connected_component_key = fulm_component_pin_get_connected_component_key(pin);;
			if (connected_component_key == NULL)
				continue;
			const gchar* connected_pin_name = fulm_component_pin_get_connected_pin_name(pin);
			g_assert_nonnull(connected_pin_name);
			json_builder_begin_object(json_builder);
			json_builder_set_member_name(json_builder, _TYPE_FIELD);
			json_builder_add_string_value(json_builder, pin_type_letter_array[i_pin_type]);
			json_builder_set_member_name(json_builder, _NAME_FIELD);
			json_builder_add_string_value(json_builder, pin_name_array[i_pin]);
			json_builder_set_member_name(json_builder, _KEY_FIELD);
			json_builder_add_string_value(json_builder, connected_component_key);
			json_builder_set_member_name(json_builder, _PIN_FIELD);
			json_builder_add_string_value(json_builder,connected_pin_name);
			json_builder_end_object(json_builder); }
	}
	json_builder_end_array(json_builder);
}

void
fulm_component_save_to_json (FulmComponent* self, JsonBuilder *json_builder)
{
	g_return_if_fail(FULM_IS_COMPONENT(self));
	g_return_if_fail(json_builder != NULL && JSON_IS_BUILDER(json_builder));
	FulmComponentClass *component_class = FULM_COMPONENT_GET_CLASS(self);
	g_return_if_fail(component_class->save_to_json != NULL);
	component_class->save_to_json(self, json_builder);
}



static void
fulm_component_real_load_from_json (FulmComponent* self, JsonReader *json_reader)
{ 
	FulmComponentPrivate* priv = fulm_component_get_instance_private(self);

	// TODO what to do if read member fails ?
	json_reader_read_member(json_reader, _NAME_FIELD );
	const gchar* name = json_reader_get_string_value(json_reader);
	json_reader_end_member(json_reader);
	fulm_component_set_name(self, name);

	json_reader_read_member(json_reader, _KEY_FIELD );
	const gchar* key = json_reader_get_string_value(json_reader);
	json_reader_end_member(json_reader);
	if (priv->key != NULL && g_strcmp0(priv->key, key) != 0) 
		g_error("Component key [%s] differs from JSON key [%s]", priv->key, key);
	if (priv->key != NULL) g_free(priv->key);
	priv->key = g_strdup(key);

	json_reader_read_member(json_reader, _PARENT_KEY_FIELD );
	const gchar* parent_key = json_reader_get_string_value(json_reader);
	json_reader_end_member(json_reader);
	if (priv->parent_key != NULL && g_strcmp0(priv->parent_key, parent_key) != 0)
		g_error("Component parent key [%s] differs from JSON parent key [%s]", priv->parent_key, parent_key);
	if (priv->parent_key != NULL) g_free(priv->parent_key);
	priv->parent_key = g_strdup(parent_key);

	// load pin connections
	if (!json_reader_read_member(json_reader, _PIN_CONNECTION_LIST_FIELD))
		g_error("Can't read [%s]", _PIN_CONNECTION_LIST_FIELD);
	gint n = json_reader_count_elements(json_reader);
	for (gint i = 0; i < n; i++) {
		if (! json_reader_read_element(json_reader,i))
			g_error("Can't read pin connection n°%i", i);

		g_assert_true(json_reader_read_member(json_reader, _TYPE_FIELD));
		const gchar* pin_type_letter = json_reader_get_string_value(json_reader);
		json_reader_end_member(json_reader);

		g_assert_true(json_reader_read_member(json_reader, _NAME_FIELD));
		const gchar* pin_name = json_reader_get_string_value(json_reader);
		json_reader_end_member(json_reader);

		g_assert_true(json_reader_read_member(json_reader, _KEY_FIELD));
		const gchar* cnx_component_key = json_reader_get_string_value(json_reader);
		json_reader_end_member(json_reader);

		g_assert_true(json_reader_read_member(json_reader, _PIN_FIELD));
		const gchar* cnx_pin_name = json_reader_get_string_value(json_reader);
		json_reader_end_member(json_reader);

		gint pin_type = -1;
		for (gint i_pin_type = 0; pin_type_letter_array[i_pin_type] != NULL; i_pin_type++) {
			if (g_strcmp0(pin_type_letter, pin_type_letter_array[i_pin_type]) == 0){
				pin_type = pin_type_array[i_pin_type];
				break;
			}
		}
		g_assert(pin_type != -1);

 
		FulmComponentPin* pin = fulm_component_get_pin_by_name(self, pin_type, pin_name);
		g_assert_nonnull(pin);
		g_assert_true(FULM_IS_COMPONENT_PIN(pin));
		fulm_component_pin_set_connection(pin, cnx_component_key, cnx_pin_name);

		json_reader_end_element(json_reader);
	}
	json_reader_end_member(json_reader);
}

void
fulm_component_load_from_json (FulmComponent* self, JsonReader *json_reader)
{
	g_return_if_fail(FULM_IS_COMPONENT(self));
	g_return_if_fail(json_reader != NULL && JSON_IS_READER(json_reader));
	FulmComponentClass *component_class = FULM_COMPONENT_GET_CLASS(self);
	g_return_if_fail(component_class->load_from_json != NULL);
	component_class->load_from_json(self, json_reader);
}



static const gchar**
fulm_component_real_get_pin_names(FulmComponent* self, FulmComponentPinType pin_type)
{
	return NULL;
}


const gchar**
fulm_component_get_pin_names(FulmComponent* self, FulmComponentPinType pin_type)
{
        g_return_val_if_fail(FULM_IS_COMPONENT(self), NULL);
        FulmComponentClass *component_class = FULM_COMPONENT_GET_CLASS(self);
        g_return_val_if_fail(component_class->get_pin_names != NULL, NULL);
        return component_class->get_pin_names(self, pin_type);
}



static gint
fulm_component_real_get_pin_count(FulmComponent* self, FulmComponentPinType pin_type)
{
	g_assert_not_reached();
	return -1;
}


gint
fulm_component_get_pin_count(FulmComponent* self, FulmComponentPinType pin_type)
{
        g_return_val_if_fail(FULM_IS_COMPONENT(self), -1);
        FulmComponentClass *component_class = FULM_COMPONENT_GET_CLASS(self);
        g_return_val_if_fail(component_class->get_pin_count != NULL, -1);
        return component_class->get_pin_count(self, pin_type);
}


static FulmComponentPin*
fulm_component_real_get_pin_by_index (FulmComponent* component, FulmComponentPinType pin_type, gint pin_index)
{
	g_assert_not_reached();
	return NULL;
}


FulmComponentPin*
fulm_component_get_pin_by_index (FulmComponent* self, FulmComponentPinType pin_type, gint pin_index)
{
        g_return_val_if_fail(FULM_IS_COMPONENT(self), NULL);
        FulmComponentClass *component_class = FULM_COMPONENT_GET_CLASS(self);
        g_return_val_if_fail(component_class->get_pin_by_index != NULL, NULL);
        return component_class->get_pin_by_index(self, pin_type, pin_index);
}



static FulmComponentPin*
fulm_component_real_get_pin_by_name (FulmComponent* component, FulmComponentPinType pin_type, const gchar* pin_name)
{
	const gchar** pin_names = fulm_component_get_pin_names(component, pin_type);
	g_assert(pin_names != NULL);
	for (gint index = 0; pin_names[index] != NULL; index++) {
		if (g_strcmp0(pin_name, pin_names[index]) == 0)
			return fulm_component_get_pin_by_index(component, pin_type, index);
	}
	return NULL; // not found
}

/*
 * fulm_component_get_pin_by_name :
 * @component : the @FulmComponent in which searching a pin
 * @pin_name : the name of the pin to search
 *
 * Returns: (transfer null) (nullable): the found @FulmComponentPin or @NULL if not found
 */
FulmComponentPin*
fulm_component_get_pin_by_name (FulmComponent* self, FulmComponentPinType pin_type, const gchar* pin_name)
{

        g_return_val_if_fail(FULM_IS_COMPONENT(self), NULL);
	g_return_val_if_fail(pin_name != NULL, NULL);
        FulmComponentClass *component_class = FULM_COMPONENT_GET_CLASS(self);
        g_return_val_if_fail(component_class->get_pin_by_name != NULL, NULL);
        return component_class->get_pin_by_name(self, pin_type, pin_name);
}


static GError*
fulm_component_real_validate_pin_value(FulmComponent* self, FulmComponentPin* pin, const gchar* value)
{
	return NULL; // by default, accept all values
}


/**
 * fulm_component_validate_pin_value:
 * @self: a #FulmComponent object
 * @pin : a #FulmComponentPin object which represents a component's pin 
 *
 * This is a internal function which is called by the pin to ask its component to validate
 * the new pin's value before changing it.
 *
 * Returns: (nullable) (transferr null): location for a #GError, or %NULL
 */
GError*
fulm_component_validate_pin_value(FulmComponent* self, FulmComponentPin* pin, const gchar* value)
{
	g_return_val_if_fail(FULM_IS_COMPONENT(self), NULL);
	g_return_val_if_fail(pin != NULL, NULL);
	g_return_val_if_fail(value != NULL, NULL);
	FulmComponentClass *component_class = FULM_COMPONENT_GET_CLASS(self);
	g_return_val_if_fail(component_class->validate_pin_value != NULL, NULL);
	return component_class->validate_pin_value(self, pin, value);
}




static GError* fulm_component_real_process_pin_update (FulmComponent* component, FulmComponentPin* pin)
{
	return NULL; // nothing to do 
}

/**
 * fulm_component_process_pin_update:
 * @self: a #FulmComponent object
 * @pin : a #FulmComponentPin object which represents a component's pin 
 *
 * This is a internal function which is called by the pin to ask its component to update
 * himself following a change in pin value.
 *
 * Returns: (nullable) (transferr null): location for a #GError, or %NULL
 */
GError* fulm_component_process_pin_update (FulmComponent* self, FulmComponentPin* pin)
{
	g_return_val_if_fail(FULM_IS_COMPONENT(self), NULL);
	g_return_val_if_fail(pin != NULL, NULL);
	FulmComponentClass *component_class = FULM_COMPONENT_GET_CLASS(self);
	g_return_val_if_fail(component_class->process_pin_update != NULL, NULL);
	return component_class->process_pin_update(self, pin);
}

