/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:fulm_message
 * @Title: FulmMessage
 * @short_description:
 *
 * #FulmMessage is an abstract class for all messages which can be used in Fulmicoton.
 *
 */

#include "message.h"

#define _KEY_FIELD "key"
#define _COMPONENT_KEY_FIELD "ckey"
#define _PIN_NAME_FIELD "pin"
#define _VALUE_FIELD "val"


typedef struct {
	gchar* key;
	gchar* component_key;
	gchar* pin_name;
	gchar* value;
} FulmMessagePrivate;


static void fulm_message_storable_interface_init(FulmStorableInterface *interface);
static gboolean fulm_message_storable_store (FulmStorable* storable, FulmStorage* storage, GError** perror);
static gboolean fulm_message_storable_fetch (FulmStorable* storable, FulmStorage* storage, GError** perror);

G_DEFINE_TYPE_WITH_CODE(FulmMessage, fulm_message, G_TYPE_OBJECT,
	G_ADD_PRIVATE(FulmMessage)
	G_IMPLEMENT_INTERFACE(FULM_TYPE_STORABLE, fulm_message_storable_interface_init)
);



static void
fulm_message_init(FulmMessage* message)
{
	FulmMessagePrivate* priv = fulm_message_get_instance_private(message);
	priv->key = NULL;
	priv->component_key = NULL;
	priv->pin_name = NULL;
	priv->value = NULL;
}

static void
fulm_message_dispose(GObject* object)
{
	g_return_if_fail(FULM_IS_MESSAGE(object));
	G_OBJECT_CLASS(fulm_message_parent_class)->dispose(object);
}


static void
fulm_message_finalize(GObject* object)
{
	FulmMessage* message = FULM_MESSAGE(object);
	FulmMessagePrivate *priv = fulm_message_get_instance_private(message);
	if (priv->key != NULL)
		g_free(priv->key);
	if (priv->component_key != NULL)
		g_free(priv->component_key);
	if (priv->pin_name != NULL)
		g_free(priv->pin_name);
	g_free(priv->value);
	G_OBJECT_CLASS(fulm_message_parent_class)->finalize(object);
}


const gchar*
fulm_message_get_key(FulmMessage* message)
{
	FulmMessagePrivate *priv = fulm_message_get_instance_private(message);
	return (const gchar*)priv->key;
}


const gchar*
fulm_message_get_component_key(FulmMessage* message)
{
	FulmMessagePrivate *priv = fulm_message_get_instance_private(message);
	return (const gchar*)priv->component_key;
}


const gchar*
fulm_message_get_pin_name(FulmMessage* message)
{
	FulmMessagePrivate *priv = fulm_message_get_instance_private(message);
	return (const gchar*)priv->pin_name;
}

const gchar*
fulm_message_get_value(FulmMessage* message)
{
	FulmMessagePrivate *priv = fulm_message_get_instance_private(message);
	return (const gchar*)priv->value;
}



static void
fulm_message_class_init(FulmMessageClass* message_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(message_class);
	object_class->dispose = fulm_message_dispose;
	object_class->finalize = fulm_message_finalize;
	message_class->storable_store = fulm_message_storable_store;
	message_class->storable_fetch = fulm_message_storable_fetch;
}



static gboolean
fulm_message_storable_store (FulmStorable* storable, FulmStorage* storage, GError** perror)
{
	g_return_val_if_fail(storable, FALSE);
	g_return_val_if_fail(FULM_IS_MESSAGE(storable), FALSE);
	g_return_val_if_fail(storage, FALSE);
	g_return_val_if_fail(FULM_IS_STORAGE(storage), FALSE);

	FulmMessage* self = FULM_MESSAGE(storable);
	JsonBuilder *json_builder = json_builder_new();
	fulm_message_save_to_json (self, json_builder);

	JsonNode* json_node = json_builder_get_root(json_builder);
	const gchar* key = fulm_message_get_key(FULM_MESSAGE(self)); 
	gboolean success = fulm_storage_store_json(storage, key, json_node, perror);

	json_node_unref(json_node);
	g_object_unref(json_builder);
	return success;
}



static gboolean
fulm_message_storable_fetch (FulmStorable* storable, FulmStorage* storage, GError** perror)
{
	g_return_val_if_fail(storable, FALSE);
	g_return_val_if_fail(FULM_IS_MESSAGE(storable), FALSE);
	g_return_val_if_fail(storage, FALSE);
	g_return_val_if_fail(FULM_IS_STORAGE(storage), FALSE);


	FulmMessage* self = FULM_MESSAGE(storable);
	const gchar* key = fulm_message_get_key(FULM_MESSAGE(self)); 
	g_assert_nonnull(key);

	perror = NULL;
	JsonNode* json_node = NULL;
	if (!fulm_storage_fetch_json(storage, key, &json_node , perror))
		return FALSE;
	g_assert_nonnull(json_node);

	JsonReader* json_reader = json_reader_new(json_node);
	fulm_message_load_from_json (self, json_reader);

	return TRUE;
}


static void
fulm_message_storable_interface_init(FulmStorableInterface *interface)
{
	interface->store = fulm_message_storable_store; 
	interface->fetch = fulm_message_storable_fetch; 
}


void
fulm_message_save_to_json (FulmMessage* self, JsonBuilder *json_builder)
{
	json_builder_begin_object(json_builder);

	json_builder_set_member_name(json_builder, _KEY_FIELD);
	json_builder_add_string_value(json_builder, fulm_message_get_key(self));

	json_builder_set_member_name(json_builder, _COMPONENT_KEY_FIELD);
	json_builder_add_string_value(json_builder, fulm_message_get_component_key(self));

	json_builder_set_member_name(json_builder, _PIN_NAME_FIELD);
	json_builder_add_string_value(json_builder, fulm_message_get_pin_name(self));

	json_builder_set_member_name(json_builder, _VALUE_FIELD);
	json_builder_add_string_value(json_builder, fulm_message_get_value(self));

	json_builder_end_object(json_builder);
}


void
fulm_message_load_from_json (FulmMessage* self, JsonReader *json_reader)
{ 
	FulmMessagePrivate* priv = fulm_message_get_instance_private(self);

	json_reader_read_member(json_reader, _KEY_FIELD );
	const gchar* key = json_reader_get_string_value(json_reader);
	json_reader_end_member(json_reader);
	if (priv->key != NULL) g_free(priv->key);
	priv->key = g_strdup(key);

	json_reader_read_member(json_reader, _COMPONENT_KEY_FIELD);
	const gchar* component_key = json_reader_get_string_value(json_reader);
	json_reader_end_member(json_reader);
	if (priv->component_key != NULL) g_free(priv->component_key);
	priv->component_key = g_strdup(component_key);

	json_reader_read_member(json_reader, _PIN_NAME_FIELD);
	const gchar* pin_name = json_reader_get_string_value(json_reader);
	json_reader_end_member(json_reader);
	if (priv->pin_name != NULL) g_free(priv->pin_name);
	priv->pin_name = g_strdup(pin_name);

	json_reader_read_member(json_reader, _VALUE_FIELD);
	const gchar* value = json_reader_get_string_value(json_reader);
	json_reader_end_member(json_reader);
	if (priv->value != NULL) g_free(priv->value);
	priv->value= g_strdup(value);
}

FulmMessage*
fulm_message_new_full(const gchar* key, const gchar* component_key, const gchar* pin_name, const gchar* value)
{
	g_assert_nonnull(key);
	FulmMessage* self = g_object_new(FULM_TYPE_MESSAGE, NULL);
	FulmMessagePrivate* priv = fulm_message_get_instance_private(self);
	priv->key = g_strdup(key);
	priv->component_key = g_strdup(component_key);
	priv->pin_name= g_strdup(pin_name);
	priv->value = g_strdup(value);
	return self;
}

FulmMessage*
fulm_message_new(const gchar* key)
{
	g_assert_nonnull(key);
	return fulm_message_new_full(key, NULL, NULL, NULL);
}
