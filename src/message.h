/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FULM_MESSAGE__
#define __FULM_MESSAGE__

#include <glib-object.h>
#include "global.h"
#include "storable.h"
#include "application.h"

#define FULM_TYPE_MESSAGE fulm_message_get_type()

G_DECLARE_DERIVABLE_TYPE (FulmMessage, fulm_message, FULM, MESSAGE, GObject);


struct _FulmMessageClass
{
	GObjectClass parent_class;

	// interface FulmStorable 
	gboolean (*storable_fetch) (FulmStorable* storable, FulmStorage* storage, GError** error);
	gboolean (*storable_store) (FulmStorable* storable, FulmStorage* storage, GError** error);
};



void fulm_message_save_to_json (FulmMessage* message, JsonBuilder *json_builder);
void fulm_message_load_from_json (FulmMessage* message, JsonReader *json_reader);

FulmMessage* fulm_message_new(const gchar* key);
FulmMessage* fulm_message_new_full(const gchar* key, const gchar* component_key, const gchar* pin_name, const gchar* value);
const gchar* fulm_message_get_key(FulmMessage* message);
const gchar* fulm_message_get_component_key(FulmMessage* message);
const gchar* fulm_message_get_pin_name(FulmMessage* message);
const gchar* fulm_message_get_value(FulmMessage* message);


#endif /*__FULM_MESSAGE__*/
