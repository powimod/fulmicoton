/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __FULM_EDITION_OBSERVER__
#define __FULM_EDITION_OBSERVER__

#include <glib-object.h>
#include "global.h"
#include "component.h"

#define FULM_TYPE_EDITION_OBSERVER fulm_edition_observer_get_type()

G_DECLARE_INTERFACE(FulmEditionObserver, fulm_edition_observer, FULM, EDITION_OBSERVER, GObject)

struct _FulmEditionObserverInterface
{
	GTypeInterface parent_iface;
	void (*component_created) (FulmEditionObserver* edition_observer, FulmComponent* component);
	void (*component_deleted) (FulmEditionObserver* edition_observer, FulmComponent* component);
	void (*pins_connected) (FulmEditionObserver* edition_observer, FulmComponentPin* source_pin, FulmComponentPin* target_pin);
	void (*pins_disconnected) (FulmEditionObserver* edition_observer, FulmComponentPin* source_pin, FulmComponentPin* target_pin);
};

void fulm_edition_observer_component_created(FulmEditionObserver* edition_observer, FulmComponent* component);
void fulm_edition_observer_component_deleted(FulmEditionObserver* edition_observer, FulmComponent* component);
void fulm_edition_observer_pins_connected (FulmEditionObserver* edition_observer, FulmComponentPin* source_pin, FulmComponentPin* target_pin);
void fulm_edition_observer_pins_disconnected (FulmEditionObserver* edition_observer, FulmComponentPin* source_pin, FulmComponentPin* target_pin);

#endif /*__FULM_EDITION_OBSERVER__*/
