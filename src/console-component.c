/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "console-component.h"

#define _NAME_FIELD "name"
#define _PARENT_KEY_FIELD "pkey"

#define FULM_CONSOLE_COMPONENT_INPUT_PIN_COUNT 1
#define FULM_CONSOLE_COMPONENT_OUTPUT_PIN_COUNT 0
#define FULM_CONSOLE_COMPONENT_HIDDEN_PIN_COUNT 0

static const gchar* input_pin_names[] = { "log", NULL };

struct _FulmConsoleComponent
{
	FulmComponent parent;
	FulmComponentPin* log_input_pin;
};


static const gchar* fulm_console_component_get_type_name(FulmComponent* component);
static void fulm_console_component_load_from_json (FulmComponent* component, JsonReader *json_reader);
static void fulm_console_component_save_to_json (FulmComponent* component, JsonBuilder *json_builder);

static const gchar** fulm_console_component_get_pin_names(FulmComponent* component, FulmComponentPinType pin_type);
static gint fulm_console_component_get_pin_count(FulmComponent* component, FulmComponentPinType pin_type);
static FulmComponentPin* fulm_console_component_get_pin_by_index (FulmComponent* component, FulmComponentPinType pin_type, gint pin_index);
static GError* fulm_console_component_process_pin_update (FulmComponent* self, FulmComponentPin* pin);


G_DEFINE_TYPE(FulmConsoleComponent, fulm_console_component, FULM_TYPE_COMPONENT);

static void
fulm_console_component_init(FulmConsoleComponent* self)
{
	self->log_input_pin = fulm_component_pin_new(FULM_COMPONENT_PIN_TYPE_INPUT, input_pin_names[0], FULM_COMPONENT(self));
}

static void
fulm_console_component_dispose(GObject* object)
{
	FulmConsoleComponent* console_component = FULM_CONSOLE_COMPONENT(object);
	g_object_unref(console_component->log_input_pin);
	G_OBJECT_CLASS(fulm_console_component_parent_class)->dispose(object);
}

static void
fulm_console_component_finalize(GObject* object)
{
/*
	FulmConsoleComponent* console_component = FULM_CONSOLE_COMPONENT(object);
	G_OBJECT_CLASS(fulm_console_component_parent_class)->finalize(object);
*/
}

static void
fulm_console_component_class_init(FulmConsoleComponentClass* console_component_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(console_component_class);
	FulmComponentClass* component_class = FULM_COMPONENT_CLASS(console_component_class);
	object_class->dispose= fulm_console_component_dispose;
	object_class->finalize = fulm_console_component_finalize;
	component_class->get_type_name = fulm_console_component_get_type_name;
	component_class->save_to_json = fulm_console_component_save_to_json;
	component_class->load_from_json = fulm_console_component_load_from_json;

	component_class->get_pin_names = fulm_console_component_get_pin_names;
	component_class->get_pin_count = fulm_console_component_get_pin_count;
	component_class->get_pin_by_index = fulm_console_component_get_pin_by_index;

	component_class->process_pin_update = fulm_console_component_process_pin_update;
}


/* TODO remove this
static gboolean
fulm_console_component_storable_store (FulmStorable* storable, FulmStorage* storage, GError** error)
{
	FulmConsoleComponent* self = FULM_CONSOLE_COMPONENT(storable);

	JsonBuilder *builder = json_builder_new();
	json_builder_begin_object(builder);

	// TODO code factorisation in fuml_component function (key and name saving)
	json_builder_set_member_name(builder, _NAME_FIELD);
	json_builder_add_string_value(builder, fulm_component_get_name(FULM_COMPONENT(self)));

	json_builder_set_member_name(builder, _PARENT_KEY_FIELD);
	json_builder_add_string_value(builder, fulm_component_get_parent_key(FULM_COMPONENT(self)));

	json_builder_end_object(builder);

	JsonNode* json_node = json_builder_get_root(builder);

	g_object_unref(builder);


	const gchar* key = fulm_component_get_key(FULM_COMPONENT(self)); 
	fulm_storage_store_json(storage, key, json_node , NULL);
	json_node_free(json_node);

	return TRUE;
}
*/

/* TODO remove this
static gboolean
fulm_console_component_storable_fetch (FulmStorable* storable, FulmStorage* storage, GError** error)
{
	FulmConsoleComponent* self = FULM_CONSOLE_COMPONENT(storable);

	// TODO code factorisation in fuml_component function (key and name loading)
	JsonNode* json_node = NULL;
	const gchar* key = fulm_component_get_key(FULM_COMPONENT(self)); 
	fulm_storage_fetch_json(storage, key, &json_node , NULL);

	JsonReader* reader = json_reader_new(json_node);

	json_reader_read_member(reader, _NAME_FIELD );
	const gchar* name = json_reader_get_string_value(reader);
	json_reader_end_member(reader);
	fulm_component_set_name(FULM_COMPONENT(self), name);

	json_reader_read_member(reader, _PARENT_KEY_FIELD);
	const gchar* value = json_reader_get_string_value(reader);
	json_reader_end_member(reader);
	fulm_component_set_parent_key(FULM_COMPONENT(self), value);

	g_object_unref(reader);
	json_node_free(json_node);

	return TRUE;
}
*/


FulmComponent*
fulm_console_component_new()
{
	return FULM_COMPONENT(g_object_new(FULM_TYPE_CONSOLE_COMPONENT, NULL));
}

void
fulm_console_component_log(FulmConsoleComponent* self, const gchar* message)
{
	g_return_if_fail(message != NULL);
	fulm_component_pin_set_value(self->log_input_pin, message);
}

const gchar*
fulm_console_component_type_name()
{
	return "console-component";
}

static const gchar*
fulm_console_component_get_type_name(FulmComponent* component)
{
	return fulm_console_component_type_name();
}


static void
fulm_console_component_save_to_json (FulmComponent* component, JsonBuilder *json_builder)
{
	//FulmConsoleComponent* self = FULM_CONSOLE_COMPONENT(component);
	json_builder_begin_object(json_builder);
	FULM_COMPONENT_CLASS(fulm_console_component_parent_class)->save_to_json(component, json_builder);
	// nothing else to save
	json_builder_end_object(json_builder);
}


static void
fulm_console_component_load_from_json (FulmComponent* component, JsonReader *json_reader)
{
	//FulmConsoleComponent* self = FULM_CONSOLE_COMPONENT(component);
	FULM_COMPONENT_CLASS(fulm_console_component_parent_class)->load_from_json(component, json_reader);
	// nothing else to load 
}



// pin functions 

static const gchar**
fulm_console_component_get_pin_names(FulmComponent* component, FulmComponentPinType pin_type)
{
        g_return_val_if_fail(FULM_IS_CONSOLE_COMPONENT(component), NULL);
	switch(pin_type)
	{
		case FULM_COMPONENT_PIN_TYPE_INPUT:
			return input_pin_names;
		case FULM_COMPONENT_PIN_TYPE_OUTPUT:
			return FULM_COMPONENT_EMPTY_PIN_ARRAY ;
		case FULM_COMPONENT_PIN_TYPE_HIDDEN:
			return FULM_COMPONENT_EMPTY_PIN_ARRAY ;
	}
	g_assert_not_reached();
	return NULL;
}


static gint
fulm_console_component_get_pin_count(FulmComponent* component, FulmComponentPinType pin_type)
{
        g_return_val_if_fail(FULM_IS_CONSOLE_COMPONENT(component), -1);
	//FulmConsoleComponent* self = FULM_CONSOLE_COMPONENT(component);
        //FulmComponentClass *component_class = FULM_CONSOLE_COMPONENT_GET_CLASS(self);
	switch(pin_type)
	{
		case FULM_COMPONENT_PIN_TYPE_INPUT:
			return FULM_CONSOLE_COMPONENT_INPUT_PIN_COUNT;
		case FULM_COMPONENT_PIN_TYPE_OUTPUT:
			return FULM_CONSOLE_COMPONENT_OUTPUT_PIN_COUNT;
		case FULM_COMPONENT_PIN_TYPE_HIDDEN:
			return FULM_CONSOLE_COMPONENT_HIDDEN_PIN_COUNT;
	}
	g_assert_not_reached();
}



static FulmComponentPin*
fulm_console_component_get_pin_by_index (FulmComponent* component, FulmComponentPinType pin_type, gint pin_index)
{
        g_return_val_if_fail(FULM_IS_CONSOLE_COMPONENT(component), NULL);
	g_return_val_if_fail(pin_index >= 0, NULL);
	FulmConsoleComponent* self = FULM_CONSOLE_COMPONENT(component);
	switch(pin_type)
	{
		case FULM_COMPONENT_PIN_TYPE_INPUT:
			g_return_val_if_fail(pin_index < FULM_CONSOLE_COMPONENT_INPUT_PIN_COUNT, NULL);
			return self->log_input_pin; 

		case FULM_COMPONENT_PIN_TYPE_OUTPUT:
			g_return_val_if_reached(NULL);

		case FULM_COMPONENT_PIN_TYPE_HIDDEN:
			g_return_val_if_reached(NULL);
	}
	g_assert_not_reached();
	return NULL;
}


static GError*
fulm_console_component_process_pin_update (FulmComponent* component, FulmComponentPin* pin)
{
	FulmConsoleComponent* self = FULM_CONSOLE_COMPONENT(component);
        g_return_val_if_fail(FULM_IS_CONSOLE_COMPONENT(self), NULL);
        g_return_val_if_fail(FULM_IS_COMPONENT_PIN(pin), NULL);
	g_return_val_if_fail(pin == self->log_input_pin, NULL);
	g_message(fulm_component_pin_get_value(pin));
	return NULL;
}

