/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */



#include "storable.h"

G_DEFINE_INTERFACE(FulmStorable, fulm_storable, G_TYPE_OBJECT)


gboolean
fulm_storable_store (FulmStorable* storable, FulmStorage* storage, GError** error)
{
	FulmStorableInterface* interface = FULM_STORABLE_GET_IFACE(storable);
	g_return_val_if_fail(interface->store != NULL, FALSE);
	interface->store(storable, storage, error);
	return TRUE;
}

gboolean
fulm_storable_fetch (FulmStorable* storable, FulmStorage* storage, GError** error)
{
	FulmStorableInterface* interface = FULM_STORABLE_GET_IFACE(storable);
	g_return_val_if_fail(interface->fetch != NULL, FALSE);
	interface->fetch(storable, storage, error);
	return TRUE;
}

static void
fulm_storable_default_init(FulmStorableInterface *interface)
{
	interface->store = fulm_storable_store;
	interface->fetch = fulm_storable_fetch;
}
