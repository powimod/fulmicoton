/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FULM_COMPONENT_FACTORY__
#define __FULM_COMPONENT_FACTORY__

#include <glib-object.h>
#include "global.h"


#define FULM_TYPE_COMPONENT_FACTORY fulm_component_factory_get_type()

G_DECLARE_FINAL_TYPE (FulmComponentFactory, fulm_component_factory, FULM, COMPONENT_FACTORY, GObject)

struct _FulmComponentFactoryClass
{
	GObjectClass parent_class;
};

FulmComponentFactory* fulm_component_factory_new();

typedef struct _FulmComponent FulmComponent;
typedef FulmComponent* (*FulmComponentFactoryInstanciateFunction)();
gboolean fulm_component_factory_register(FulmComponentFactory* factory, const gchar* type_name, 
	FulmComponentFactoryInstanciateFunction instanciate_function);

FulmComponent* fulm_component_factory_build(FulmComponentFactory* factory, const gchar* type_name);

#endif /*__FULM_COMPONENT_FACTORY__*/
