/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __FULM_GDBM_STORAGE_H__
#define __FULM_GDBM_STORAGE_H__

#include <glib-object.h>
#include <gio/gio.h>
#include "global.h"
#include "storage.h"

#define FULM_TYPE_GDBM_STORAGE fulm_gdbm_storage_get_type()

G_DECLARE_FINAL_TYPE(FulmGdbmStorage, fulm_gdbm_storage, FULM, GDBM_STORAGE, FulmStorage)

FulmGdbmStorage* fulm_gdbm_storage_new(GFile* storage_file);


#endif /*__FULM_GDBM_STORAGE_H__*/
