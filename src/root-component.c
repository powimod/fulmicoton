/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "root-component.h"


struct _FulmRootComponent
{
	FulmCompositeComponent parent;
};

static const gchar* fulm_root_component_get_type_name(FulmComponent* component);
static gboolean fulm_root_component_add_component(FulmCompositeComponent* composite_component, FulmComponent* child_component, GError **perror);

G_DEFINE_TYPE(FulmRootComponent, fulm_root_component, FULM_TYPE_COMPOSITE_COMPONENT);

GQuark
fulm_root_component_error_quark(void)
{
	return g_quark_from_static_string("fulm-type-root-component-error-quark");
}


static void
fulm_root_component_init(FulmRootComponent* root_component)
{
}

static void
fulm_root_component_finalize(GObject* object)
{
	//FulmRootComponent* root_component = FULM_ROOT_COMPONENT(object);
	G_OBJECT_CLASS(fulm_root_component_parent_class)->finalize(object);
}

static void
fulm_root_component_class_init(FulmRootComponentClass* root_component_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(root_component_class);
	FulmComponentClass* component_class = FULM_COMPONENT_CLASS(root_component_class);
	FulmCompositeComponentClass* composite_component_class = FULM_COMPOSITE_COMPONENT_CLASS(root_component_class);
	object_class->finalize = fulm_root_component_finalize;
	component_class->get_type_name = fulm_root_component_get_type_name;
	composite_component_class->add_component = fulm_root_component_add_component;
}


FulmComponent*
fulm_root_component_new()
{
	return FULM_COMPONENT(g_object_new(FULM_TYPE_ROOT_COMPONENT, NULL));
}



static gboolean
fulm_root_component_add_component(FulmCompositeComponent* composite_component, FulmComponent* child_component, GError **perror)
{
	g_assert(composite_component != NULL);
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(composite_component));
	g_assert(child_component != NULL);
	g_assert_true(FULM_IS_COMPONENT(child_component));
	if (! FULM_IS_COMPOSITE_COMPONENT(child_component)){
		if (perror != NULL) 
			g_set_error(perror,
				FULM_ROOT_COMPONENT_ERROR, 
				FULM_ROOT_COMPONENT_ERROR_ONLY_COMPOSITE,
				"Only composite component can be add to root component"
			);
		return FALSE;
	}
	return FULM_COMPOSITE_COMPONENT_CLASS(fulm_root_component_parent_class)->add_component(composite_component, child_component, perror);
}

const gchar*
fulm_root_component_type_name()
{
	return "root-component";
}

static const gchar*
fulm_root_component_get_type_name(FulmComponent* component)
{
	return fulm_root_component_type_name();
}



gboolean
fulm_root_component_connect_pins(FulmRootComponent* self, 
	FulmComponentPin* source_pin, FulmComponentPin* target_pin, GError **perror)
{
	g_assert(FULM_IS_ROOT_COMPONENT(self));
	g_assert(FULM_IS_COMPONENT_PIN(source_pin));
	g_assert(FULM_IS_COMPONENT_PIN(target_pin));

	g_error("not implemented");
	return FALSE;
}
