/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "component-pin.h"


struct _FulmComponentPin
{
	GObject parent;
	FulmComponentPinType pin_type;
	gchar* name;
	gchar* value;
	gchar* connected_component_key;
	gchar* connected_pin_name;
	FulmComponent* parent_component;
};

G_DEFINE_TYPE(FulmComponentPin, fulm_component_pin, G_TYPE_OBJECT)

static void
fulm_component_pin_init(FulmComponentPin* self)
{
	self->name = NULL;
	self->value = NULL;
	self->connected_component_key = NULL;
	self->connected_pin_name = NULL;
	self->parent_component = NULL;
}

static void
fulm_component_pin_finalize(GObject* object)
{
	FulmComponentPin* self = FULM_COMPONENT_PIN(object);
	g_return_if_fail(FULM_IS_COMPONENT_PIN(object));
	if (self->name != NULL)
		g_free(self->name);
	if (self->value != NULL)
		g_free(self->value);
	if (self->connected_component_key != NULL)
		g_free(self->connected_component_key);
	if (self->connected_pin_name != NULL)
		g_free(self->connected_pin_name);
}

static void
fulm_component_pin_class_init(FulmComponentPinClass* class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(class);
	object_class->finalize = fulm_component_pin_finalize;
}

FulmComponentPin*
fulm_component_pin_new(FulmComponentPinType pin_type, const gchar* name, FulmComponent* parent_component)
{
	g_return_val_if_fail(name != NULL, NULL);
	FulmComponentPin* self = g_object_new(FULM_TYPE_COMPONENT_PIN, NULL);
	self->pin_type = pin_type;
	if (self->name != NULL)
		g_free(self->name);
	self->name = g_strdup(name);
	self->parent_component = parent_component; // can be NULL
	return self;
}


FulmComponentPinType
fulm_component_pin_get_pin_type(FulmComponentPin* self)
{
	return self->pin_type;
}

/**
 * fulm_component_pin_get_name:
 * @self : A #FulmComponentPin 
 * 
 * Returns: (transfer null) : the name of the pin 
 */

const gchar*
fulm_component_pin_get_name(FulmComponentPin* self)
{
	return self->name;
}


/**
 * fulm_component_pin_get_value:
 * @self : A #FulmComponentPin 
 * 
 * Returns: (transfer null) : the value of the pin 
 */
const gchar*
fulm_component_pin_get_value(FulmComponentPin* self)
{
	return self->value;
}

// FIXME external prototype duplication
GError* fulm_component_validate_pin_value(FulmComponent* component, FulmComponentPin* pin, const gchar* value);
GError* fulm_component_process_pin_update (FulmComponent* self, FulmComponentPin* pin);

void
fulm_component_pin_set_value(FulmComponentPin* self, const gchar* value)
{
	g_return_if_fail(value != NULL);
	if (self->parent_component != NULL) {
		GError* error = fulm_component_validate_pin_value(self->parent_component, self, value);
		g_return_if_fail(error == NULL);
	}
	if (self->value != NULL)
		g_free(self->value);
	self->value = g_strdup(value);
	if (self->parent_component != NULL) {
		GError* error = fulm_component_process_pin_update(self->parent_component, self);
		g_return_if_fail(error == NULL);
	}
}


const gchar*
fulm_component_pin_get_connected_component_key(FulmComponentPin* self)
{
	return self->connected_component_key;
}

const gchar*
fulm_component_pin_get_connected_pin_name(FulmComponentPin* self)
{
	return self->connected_pin_name;
}

/**
 * fulm_component_pin_set_connection:
 * @self : a #FulmComponentPin
 * @component_key: (nullable) (transfer null) : key of the #FulmComponent
 * @pin_name: (nullable) (transfer null) : name of the pin of the #FulmComponent
 *
 * Set (or clear) a connection between this pin and the pin of another component. 
 * Previous connection is cleared if @component_key or @pin_name is @NULL.
 */
void
fulm_component_pin_set_connection(FulmComponentPin* self, const gchar* component_key, const gchar* pin_name)
{
	if (self->connected_component_key != NULL)
		g_free(self->connected_component_key);
	if (self->connected_pin_name != NULL)
		g_free(self->connected_pin_name);
	if (component_key == NULL || pin_name == NULL) {
		self->connected_component_key = NULL;
		self->connected_pin_name = NULL;
	}
	else {
		self->connected_component_key = g_strdup(component_key);
		self->connected_pin_name = g_strdup(pin_name);
	}
}

/**
 * fulm_component_pin_get_component:
 * @self : a #FulmComponentPin
 * Returns: (nullable) (transfer null): The #FulmComponent which owns this pin.
 */
FulmComponent*
fulm_component_pin_get_component(FulmComponentPin* self)
{
	g_assert(FULM_IS_COMPONENT_PIN(self));
	return self->parent_component;
}
