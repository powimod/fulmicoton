/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "component-factory.h"

typedef struct 
{
	gchar* type_name;
	FulmComponentFactoryInstanciateFunction instanciate_function;
} FulmComponentFactoryEntry;

struct _FulmComponentFactory
{
	GObject parent;
	GSList *component_factory_entries;
};

G_DEFINE_TYPE(FulmComponentFactory, fulm_component_factory, G_TYPE_OBJECT)


static void
fulm_component_factory_init(FulmComponentFactory* self)
{
	self->component_factory_entries = NULL;
}

static void
fulm_component_factory_dispose(GObject* object)
{
	g_return_if_fail(FULM_IS_COMPONENT_FACTORY(object));
	G_OBJECT_CLASS(fulm_component_factory_parent_class)->dispose(object);
}

static void
fulm_component_factory_finalize(GObject* object)
{
	g_return_if_fail(FULM_IS_COMPONENT_FACTORY(object));
	FulmComponentFactory* self = FULM_COMPONENT_FACTORY(object);
	// FIXME type name string is not freed...
	g_slist_free_full(self->component_factory_entries, g_free);
	G_OBJECT_CLASS(fulm_component_factory_parent_class)->finalize(object);
}

static void
fulm_component_factory_class_init(FulmComponentFactoryClass* class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(class);
	object_class->dispose = fulm_component_factory_dispose;
	object_class->finalize = fulm_component_factory_finalize;
}

FulmComponentFactory*
fulm_component_factory_new(void)
{
	FulmComponentFactory * self = g_object_new(FULM_TYPE_COMPONENT_FACTORY, NULL);
	return self;
}


gboolean
fulm_component_factory_register(FulmComponentFactory* self, const gchar* type_name, 
	FulmComponentFactoryInstanciateFunction instanciate_function)
{
	g_return_val_if_fail(self != NULL, FALSE);
	g_return_val_if_fail(type_name != NULL, FALSE);
	g_return_val_if_fail(instanciate_function != NULL, FALSE);
	FulmComponentFactoryEntry* entry = g_malloc(sizeof(FulmComponentFactoryEntry));
	entry->type_name = g_strdup(type_name);
	entry->instanciate_function = instanciate_function;
	self->component_factory_entries = g_slist_append(self->component_factory_entries, entry);
	return TRUE;
}

FulmComponent*
fulm_component_factory_build(FulmComponentFactory* self, const gchar* type_name)
{
	g_assert(type_name != NULL);
	GSList *list_entry = self->component_factory_entries;
	//g_print("Component factory build [%s]\n", type_name);
	while (list_entry != NULL){
		FulmComponentFactoryEntry* entry = (FulmComponentFactoryEntry*)list_entry->data;
		//g_print("- Compare to [%s]\n", entry->type_name);
		if (g_strcmp0(type_name, entry->type_name) == 0)
			return entry->instanciate_function();
		list_entry = g_slist_next(list_entry);
	}
	g_warning("Component [%s] not found\n", type_name);
	return NULL;
}
