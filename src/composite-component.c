/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "composite-component.h"

#define _NAME_FIELD "name"
#define _PARENT_KEY_FIELD "pkey"
#define _COMPONENT_KEYS_FIELD "comp_keys"



typedef struct
{
	GSList *component_key_list;
} FulmCompositeComponentPrivate;

static void fulm_composite_component_storable_interface_init(FulmStorableInterface *interface);
static const gchar* fulm_composite_component_get_type_name(FulmComponent* component);
static void fulm_composite_component_save_to_json (FulmComponent* component, JsonBuilder *json_builder);
static void fulm_composite_component_load_from_json (FulmComponent* component, JsonReader *json_reader);
static gboolean fulm_composite_component_real_add_component(FulmCompositeComponent* self, FulmComponent* child_component, GError **perror);

G_DEFINE_TYPE_WITH_CODE(FulmCompositeComponent, fulm_composite_component, FULM_TYPE_COMPONENT,
	G_ADD_PRIVATE(FulmCompositeComponent)
	G_IMPLEMENT_INTERFACE(FULM_TYPE_STORABLE, fulm_composite_component_storable_interface_init)
);

GQuark
fulm_composite_component_error_quark(void)
{
	return g_quark_from_static_string("fulm-type-composite-component-error-quark");
}


static void
fulm_composite_component_init(FulmCompositeComponent* composite_component)
{
}

static void
fulm_composite_component_finalize(GObject* object)
{
	//FulmCompositeComponent* composite_component = FULM_COMPOSITE_COMPONENT(object);
	G_OBJECT_CLASS(fulm_composite_component_parent_class)->finalize(object);
}

static void
fulm_composite_component_class_init(FulmCompositeComponentClass* composite_component_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(composite_component_class);
	FulmComponentClass* component_class = FULM_COMPONENT_CLASS(composite_component_class);
	object_class->finalize = fulm_composite_component_finalize;
	component_class->get_type_name = fulm_composite_component_get_type_name;
	component_class->save_to_json = fulm_composite_component_save_to_json;
	component_class->load_from_json = fulm_composite_component_load_from_json;
	composite_component_class->add_component = fulm_composite_component_real_add_component;
}


static gboolean
fulm_composite_component_storable_store (FulmStorable* storable, FulmStorage* storage, GError** error)
{
	FulmCompositeComponent* self = FULM_COMPOSITE_COMPONENT(storable);
	FulmCompositeComponentPrivate *priv = fulm_composite_component_get_instance_private(self);

	JsonBuilder *builder = json_builder_new();
	json_builder_begin_object(builder);

	json_builder_set_member_name(builder, _NAME_FIELD);
	json_builder_add_string_value(builder, fulm_component_get_name(FULM_COMPONENT(self)));

	const gchar* parent_key = fulm_component_get_parent_key(FULM_COMPONENT(self));
	if (parent_key != NULL) {
		json_builder_set_member_name(builder, _PARENT_KEY_FIELD);
		json_builder_add_string_value(builder, parent_key);
	}

	json_builder_set_member_name(builder, _COMPONENT_KEYS_FIELD);
	json_builder_begin_array(builder);

	GSList *key_pointer = priv->component_key_list;
	while (key_pointer != NULL){
		gchar* key = key_pointer->data;
		json_builder_add_string_value(builder, key);
		key_pointer = g_slist_next(key_pointer);
	}
	json_builder_end_array(builder);

	json_builder_end_object(builder);
	JsonNode* json_node = json_builder_get_root(builder);
	g_object_unref(builder);

	const gchar* key = fulm_component_get_key(FULM_COMPONENT(self)); 
	fulm_storage_store_json(storage, key, json_node , NULL);
	json_node_free(json_node);

	return TRUE;
}

static gboolean
fulm_composite_component_storable_fetch (FulmStorable* storable, FulmStorage* storage, GError** perror)
{
	FulmCompositeComponent* self = FULM_COMPOSITE_COMPONENT(storable);
	FulmCompositeComponentPrivate *priv = fulm_composite_component_get_instance_private(self);

	JsonNode* json_node = NULL;
	const gchar* key = fulm_component_get_key(FULM_COMPONENT(self)); 
	if (! fulm_storage_fetch_json(storage, key, &json_node , perror))
		return FALSE;
	g_assert(json_node != NULL);

	JsonReader* reader = json_reader_new(json_node);

	json_reader_read_member(reader, _NAME_FIELD );
	const gchar* name = json_reader_get_string_value(reader);
	json_reader_end_member(reader);
	fulm_component_set_name(FULM_COMPONENT(self), name);

	// (root component does not have a parent key)
	if (json_reader_read_member(reader, _PARENT_KEY_FIELD)) {
		const gchar* value = json_reader_get_string_value(reader);
		fulm_component_set_parent_key(FULM_COMPONENT(self), value);
	}
	json_reader_end_member(reader);

	g_assert(priv->component_key_list == NULL);
	json_reader_read_member(reader, _COMPONENT_KEYS_FIELD);
	gint n = json_reader_count_elements(reader);
	for (gint i = 0; i < n; i++) {
		if (! json_reader_read_element(reader,i))
			g_error("Can't read key n°%i", i);
		const gchar* component_key = json_reader_get_string_value(reader);
		priv->component_key_list = g_slist_append(priv->component_key_list, g_strdup(component_key));
		json_reader_end_element(reader);
	}
	json_reader_end_member(reader); // _COMPONENT_KEY_FIELD 

	g_object_unref(reader);
	//json_node_unref(json_node); // FIXME necessary ?
	json_node_free(json_node);

	return TRUE;
}



static void
fulm_composite_component_storable_interface_init(FulmStorableInterface *interface)
{
	interface->store = fulm_composite_component_storable_store; 
	interface->fetch = fulm_composite_component_storable_fetch; 
}

FulmComponent*
fulm_composite_component_new()
{
	return FULM_COMPONENT(g_object_new(FULM_TYPE_COMPOSITE_COMPONENT, NULL));
}



static gboolean
fulm_composite_component_real_add_component(FulmCompositeComponent* self, FulmComponent* child_component, GError **perror)
{
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(self));
	g_assert(child_component != NULL);
	g_assert_true(FULM_IS_COMPONENT(child_component));
	if (fulm_component_get_parent_key(child_component) != NULL) {
		if (perror != NULL) 
			g_set_error(perror,
				FULM_COMPOSITE_COMPONENT_ERROR, 
				FULM_COMPOSITE_COMPONENT_ERROR_COMPONENT_ALREADY_LINKED,
				"Component [%s] is already linked to component [%s]",
				fulm_component_get_key(child_component),
				fulm_component_get_parent_key(child_component)
			);
		return FALSE;
	}
	const gchar* parent_key = fulm_component_get_key(FULM_COMPONENT(self));
	g_assert_nonnull(parent_key);
	FulmCompositeComponentPrivate *priv = fulm_composite_component_get_instance_private(self);
	// TODO check component name is not already in list
	gchar* child_key = g_strdup(fulm_component_get_key(child_component));
	priv->component_key_list = g_slist_append(priv->component_key_list, child_key);
	// FIXME can't store composite_component here !
	return TRUE;
}

gboolean
fulm_composite_component_add_component(FulmCompositeComponent* self, FulmComponent* child_component, GError **perror)
{
	g_return_val_if_fail(FULM_IS_COMPOSITE_COMPONENT(self), FALSE);
	g_return_val_if_fail(FULM_IS_COMPONENT(self), FALSE);
	FulmCompositeComponentClass *composite_component_class = FULM_COMPOSITE_COMPONENT_GET_CLASS(self);
	g_return_val_if_fail(composite_component_class->add_component != NULL, FALSE);
	return composite_component_class->add_component(self, child_component, perror);
}

const gchar*
fulm_composite_component_type_name()
{
	return "composite-component";
}

static const gchar*
fulm_composite_component_get_type_name(FulmComponent* component)
{
	return fulm_composite_component_type_name();
}



static void
fulm_composite_component_save_to_json (FulmComponent* component, JsonBuilder *json_builder)
{
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(component));
	FulmCompositeComponent* self = FULM_COMPOSITE_COMPONENT(component);
	FulmCompositeComponentPrivate *priv = fulm_composite_component_get_instance_private(self);
	json_builder_begin_object(json_builder);
	FULM_COMPONENT_CLASS(fulm_composite_component_parent_class)->save_to_json(component, json_builder);
	json_builder_set_member_name(json_builder, _COMPONENT_KEYS_FIELD);
	json_builder_begin_array(json_builder);
	GSList *key_pointer = priv->component_key_list;
	while (key_pointer != NULL){
		gchar* key = key_pointer->data;
		json_builder_add_string_value(json_builder, key);
		key_pointer = g_slist_next(key_pointer);
	}
	json_builder_end_array(json_builder);
	json_builder_end_object(json_builder);
}


static void
fulm_composite_component_load_from_json (FulmComponent* component, JsonReader *json_reader)
{
	FulmCompositeComponent* self = FULM_COMPOSITE_COMPONENT(component);
	FULM_COMPONENT_CLASS(fulm_composite_component_parent_class)->load_from_json(component, json_reader);
	FulmCompositeComponentPrivate *priv = fulm_composite_component_get_instance_private(self);
	g_assert(priv->component_key_list == NULL);
	json_reader_read_member(json_reader, _COMPONENT_KEYS_FIELD);
	gint n = json_reader_count_elements(json_reader);
	for (gint i = 0; i < n; i++) {
		if (! json_reader_read_element(json_reader,i))
			g_error("Can't read key n°%i", i);
		const gchar* component_key = json_reader_get_string_value(json_reader);
		priv->component_key_list = g_slist_append(priv->component_key_list, g_strdup(component_key));
		json_reader_end_element(json_reader);
	}
	json_reader_end_member(json_reader); // _COMPONENT_KEY_FIELD 
}

/**
 * fulm_composite_component_get_child_component_keys
 *  @self : a @FulmCompositeComponent*
 *  Returns : (container) : a *NULL terminated array with child's keys
 */
gchar**
fulm_composite_component_get_child_component_keys(FulmCompositeComponent *self)
{
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(self));
	FulmCompositeComponentPrivate *priv = fulm_composite_component_get_instance_private(self);
	guint length = g_slist_length(priv->component_key_list);
	gchar** keys_array = g_malloc(sizeof(const gchar*) * (length + 1));
	GSList *key_pointer = priv->component_key_list;
	gint i = 0;
	while (key_pointer != NULL){
		keys_array[i++] = key_pointer->data;
		key_pointer = g_slist_next(key_pointer);
	}
	keys_array[i] = NULL; // array terminated with NULL pointer
	return keys_array;
}

