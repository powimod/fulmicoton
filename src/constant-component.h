/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __FULM_CONSTANT_COMPONENT__
#define __FULM_CONSTANT_COMPONENT__

#include <glib-object.h>
#include "global.h"
#include "component.h"

#define FULM_TYPE_CONSTANT_COMPONENT fulm_constant_component_get_type()

G_DECLARE_FINAL_TYPE(FulmConstantComponent, fulm_constant_component, FULM, CONSTANT_COMPONENT, FulmComponent);

FulmComponent* fulm_constant_component_new();

const gchar* fulm_constant_component_get_value(FulmConstantComponent* constant_component);
void fulm_constant_component_set_value(FulmConstantComponent* constant_component, const gchar* value);

const gchar* fulm_constant_component_type_name();

#endif /*__FULM_CONSTANT_COMPONENT__*/

