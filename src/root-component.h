/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FULM_ROOT_COMPONENT__
#define __FULM_ROOT_COMPONENT__

#include <glib-object.h>
#include "global.h"
#include "composite-component.h"

#define FULM_TYPE_ROOT_COMPONENT fulm_root_component_get_type()

G_DECLARE_FINAL_TYPE(FulmRootComponent, fulm_root_component, FULM, ROOT_COMPONENT, FulmCompositeComponent);


typedef enum {
	FULM_ROOT_COMPONENT_ERROR_ONLY_COMPOSITE,
} FulmRootComponentError;

#define FULM_ROOT_COMPONENT_ERROR fulm_root_component_error_quark()
GQuark fulm_root_component_error_quark(void);


FulmComponent* fulm_root_component_new();

const gchar* fulm_root_component_type_name();


#endif /*__FULM_ROOT_COMPONENT__*/

