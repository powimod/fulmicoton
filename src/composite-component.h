/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FULM_COMPOSITE_COMPONENT__
#define __FULM_COMPOSITE_COMPONENT__

#include <glib-object.h>
#include "global.h"
#include "component.h"

#define FULM_TYPE_COMPOSITE_COMPONENT fulm_composite_component_get_type()

G_DECLARE_DERIVABLE_TYPE(FulmCompositeComponent, fulm_composite_component, FULM, COMPOSITE_COMPONENT, FulmComponent);

struct _FulmCompositeComponentClass
{
	FulmComponentClass parent_class;
	gboolean (*add_component)(FulmCompositeComponent* composite_component, FulmComponent* component, GError **perror);
};


typedef enum {
	FULM_COMPOSITE_COMPONENT_ERROR_COMPONENT_ALREADY_LINKED
} FulmCompositeComponentError;

#define FULM_COMPOSITE_COMPONENT_ERROR fulm_composite_component_error_quark()
GQuark fulm_composite_component_error_quark(void);


FulmComponent* fulm_composite_component_new();
const gchar* fulm_composite_component_get_value(FulmCompositeComponent* composite_component);
// TODO remove value 
void fulm_composite_component_set_value(FulmCompositeComponent* composite_component, const gchar* value);

gboolean fulm_composite_component_add_component(FulmCompositeComponent* composite_component, FulmComponent* component, GError **perror);

const gchar* fulm_composite_component_type_name();

gchar** fulm_composite_component_get_child_component_keys(FulmCompositeComponent *component);

#endif /*__FULM_COMPOSITE_COMPONENT__*/
