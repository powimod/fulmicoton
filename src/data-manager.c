/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:fulm_data_manager
 * @Title: FulmDataManager
 * @short_description:
 *
 */

#include "data-manager.h"
#include "constant-component.h"
#include "console-component.h"

#define _COMPONENT_KEY_PREFIX "cmp"
#define _ROOT_COMPONENT_KEY "#root"
#define _COMPONENT_TYPE_FIELD "type"

typedef struct {
	FulmStorage* storage;
	FulmComponentFactory* component_factory;
	FulmRootComponent* root_component;
	FulmEditionObserver* edition_observer;
} FulmDataManagerPrivate;


G_DEFINE_TYPE_WITH_CODE(FulmDataManager, fulm_data_manager, G_TYPE_OBJECT,
	G_ADD_PRIVATE(FulmDataManager)
);

GQuark
fulm_data_manager_error_quark(void)
{
	return g_quark_from_static_string("fulm-data-manager-error-quark");
}



static void
fulm_data_manager_init(FulmDataManager* data_manager)
{
	FulmDataManagerPrivate* priv = fulm_data_manager_get_instance_private(data_manager);
	priv->storage = NULL;

	priv->component_factory = fulm_component_factory_new();
	fulm_component_factory_register(priv->component_factory, fulm_composite_component_type_name(), fulm_composite_component_new);
	fulm_component_factory_register(priv->component_factory, fulm_root_component_type_name(), fulm_root_component_new);
	fulm_component_factory_register(priv->component_factory, fulm_constant_component_type_name(), fulm_constant_component_new);
	fulm_component_factory_register(priv->component_factory, fulm_console_component_type_name(), fulm_console_component_new);

	priv->root_component = NULL; // loaded by get_root_component function
	priv->edition_observer = NULL;
}

static void
fulm_data_manager_dispose(GObject* object)
{
	g_return_if_fail(FULM_IS_DATA_MANAGER(object));
	FulmDataManager* self = FULM_DATA_MANAGER(object);
	FulmDataManagerPrivate *priv = fulm_data_manager_get_instance_private(self);
	if (priv->storage != NULL)
		g_object_unref(priv->storage);
	if (priv->root_component != NULL)
		g_object_unref(priv->root_component);
	if (priv->edition_observer != NULL)
		g_object_unref(priv->edition_observer);
	g_object_unref(priv->component_factory);
	G_OBJECT_CLASS(fulm_data_manager_parent_class)->dispose(object);
}


static void
fulm_data_manager_finalize(GObject* object)
{
	//FulmDataManager* data_manager = FULM_DATA_MANAGER(object);
	//FulmDataManagerPrivate *priv = fulm_data_manager_get_instance_private(data_manager);
	G_OBJECT_CLASS(fulm_data_manager_parent_class)->finalize(object);
}


static void
fulm_data_manager_class_init(FulmDataManagerClass* data_manager_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(data_manager_class);
	object_class->dispose = fulm_data_manager_dispose;
	object_class->finalize = fulm_data_manager_finalize;
}

FulmDataManager*
fulm_data_manager_new(FulmStorage* storage)
{
	g_assert_nonnull(storage);
	g_assert(FULM_IS_STORAGE(storage));
	FulmDataManager* self = g_object_new(FULM_TYPE_DATA_MANAGER, NULL);
	FulmDataManagerPrivate* priv = fulm_data_manager_get_instance_private(self);
	priv->storage = storage;
	g_object_ref(priv->storage);
	return self;
}

FulmRootComponent*
fulm_data_manager_get_root_component(FulmDataManager* self, GError** perror)
{
	g_assert_true(FULM_IS_DATA_MANAGER(self));
	FulmComponent* root_component;
	FulmDataManagerPrivate* priv = fulm_data_manager_get_instance_private(self);
	if (priv->root_component == NULL)
	{
		GError* internal_error = NULL;
		root_component = fulm_data_manager_load_component(self, _ROOT_COMPONENT_KEY , &internal_error);
		if (internal_error != NULL) 
		{
			if (g_error_matches(internal_error, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_KEY_NOT_FOUND)) {
				// database first load : root node not yet created
				g_error_free(internal_error);
				g_debug("Database first load : create empty root node");
				root_component = fulm_component_factory_build(priv->component_factory, fulm_root_component_type_name());
				fulm_component_set_key(root_component, _ROOT_COMPONENT_KEY);
				// TODO save root component now 
			}
			else {
				g_object_unref(root_component);
				if (perror != NULL) 
					g_propagate_error(perror, internal_error);
				g_error_free(internal_error);
				return NULL;
			}
		}
		g_assert(root_component != NULL);
		g_assert(FULM_IS_ROOT_COMPONENT(root_component));
		g_assert_cmpstr(fulm_component_get_key(root_component), ==, _ROOT_COMPONENT_KEY );
		priv->root_component = FULM_ROOT_COMPONENT(root_component);
	}
	return priv->root_component;
}

FulmComponent*
fulm_data_manager_create_component(FulmDataManager* self, const gchar* type_name, FulmCompositeComponent *container, GError** perror)
{
	g_assert_true(FULM_IS_DATA_MANAGER(self));
	g_assert_nonnull(type_name);
	g_assert_true(FULM_IS_COMPOSITE_COMPONENT(container));
	FulmDataManagerPrivate* priv = fulm_data_manager_get_instance_private(self);
	FulmComponent* new_component = fulm_component_factory_build(priv->component_factory, type_name);

	g_clear_error(perror);
	gchar* unique_key = fulm_storage_generate_unique_key_with_prefix(priv->storage, _COMPONENT_KEY_PREFIX, perror);
	if (unique_key == NULL)
		return NULL;
	g_assert_null(fulm_component_get_key(new_component));
	fulm_component_set_key(new_component, unique_key);
	

	if (! fulm_composite_component_add_component(container, new_component, perror))
	{
		// TODO cleanup ?
		return NULL;
	}
	g_assert_null(fulm_component_get_parent_key(new_component));
	fulm_component_set_parent_key(new_component, fulm_component_get_key(FULM_COMPONENT(container)));

	g_debug("Saving container component [%s]...", fulm_component_get_key(FULM_COMPONENT(container)));
	if (! fulm_data_manager_save_component(self, FULM_COMPONENT(container), perror))
	{
		// TODO cleanup ?
		return NULL;
	}

	g_debug("Saving child component [%s]...", fulm_component_get_key(FULM_COMPONENT(new_component)));
	if (! fulm_data_manager_save_component(self, new_component, perror))
	{
		// TODO cleanup ?
		return NULL;
	}

	if (priv->edition_observer != NULL)
		fulm_edition_observer_component_created(priv->edition_observer, new_component);
	return new_component;
}


FulmComponent*
fulm_data_manager_load_component(FulmDataManager* self, const gchar* component_key, GError** perror)
{
	g_assert_true(FULM_IS_DATA_MANAGER(self));
	g_assert_nonnull(component_key);
	FulmDataManagerPrivate* priv = fulm_data_manager_get_instance_private(self);

	JsonNode* json_node = NULL;
	g_assert_nonnull(priv->storage);
	g_assert_true(FULM_IS_GDBM_STORAGE(priv->storage));
	if (!fulm_storage_fetch_json(priv->storage, component_key, &json_node , perror))
		return FALSE;
	
	g_assert_nonnull(json_node);

	JsonReader* json_reader = json_reader_new(json_node);

	if (! json_reader_read_member(json_reader, _COMPONENT_TYPE_FIELD)){
		g_object_unref(json_reader);
		if (*perror != NULL) 
			g_set_error(perror,  FULM_TYPE_DATA_MANAGER_ERROR, FULM_DATA_MANAGER_ERROR_COMPONENT_TYPE_FIELD_NOT_FOUND,
				"Can't find component type field");
		return FALSE;
	}
	const gchar* component_type_name = json_reader_get_string_value(json_reader);
	json_reader_end_member(json_reader);

	FulmComponent* new_component = fulm_component_factory_build(priv->component_factory, component_type_name);
	g_assert_nonnull(new_component);
	fulm_component_load_from_json (new_component, json_reader);

	g_object_unref(json_reader);

	return new_component;
}


gboolean
fulm_data_manager_save_component(FulmDataManager* self, FulmComponent* component, GError** perror)
{
	g_assert_true(FULM_IS_DATA_MANAGER(self));
	g_assert_true(FULM_IS_COMPONENT(component));
	FulmDataManagerPrivate* priv = fulm_data_manager_get_instance_private(self);

	JsonBuilder *json_builder = json_builder_new();
	//json_builder_begin_object(json_builder);
	//json_builder_set_member_name(json_builder, _COMPONENT_TYPE_FIELD);
	//json_builder_add_string_value(json_builder, fulm_component_get_type_name(component));
	fulm_component_save_to_json (component, json_builder);
	//json_builder_end_object(json_builder);

	JsonNode* json_node = json_builder_get_root(json_builder);
	const gchar* key = fulm_component_get_key(component); 
	gboolean success = fulm_storage_store_json(priv->storage, key, json_node , perror);

	json_node_unref(json_node);
	g_object_unref(json_builder);

	return success;
}


/**
 * fulm_data_manager_connect_pins:
 * @self : a #FulmDataManager
 * @source_pin : the source #FulmComponentPin of the link 
 * @source_target : the target #FulmComponentPin of the link 
 * @error: (allow-none): return location for a #GError, or %NULL
 *
 * Create a link between to child component's pins.
 * Source and target components must be in the same #FulmCompositeComponent.
 * 
 * An error occurs for the following reasons : 
 *
 *  - Source pin is not an output pin.
 *  - Target pin is not an input pin.
 *  - Source pin or target pin are already connected.
 *  - Source and target components are not child of the same #FulmCompositeComponent.
 * 
 * Target and source pins are saved on storage.
 * 
 * Returns: %TRUE if an error occures.
 */

gboolean fulm_data_manager_connect_pins(FulmDataManager* self, 
	FulmComponentPin* source_pin, FulmComponentPin* target_pin, GError **perror)
{
	g_assert_true(FULM_IS_DATA_MANAGER(self));
	g_assert_true(FULM_IS_COMPONENT_PIN(source_pin));
	g_assert_true(FULM_IS_COMPONENT_PIN(target_pin));
	FulmDataManagerPrivate* priv = fulm_data_manager_get_instance_private(self);

	// check source pin is not already connected
	if (fulm_component_pin_get_connected_component_key(source_pin) != NULL) {
		if (perror != NULL) 
			g_set_error(perror,
				FULM_TYPE_DATA_MANAGER_ERROR, 
				FULM_DATA_MANAGER_ERROR_PIN_ALREADY_CONNECTED,
				"Source pin is already connected");
		return FALSE;
	}
	// check target pin is not already connected
	if (fulm_component_pin_get_connected_component_key(target_pin) != NULL) {
		if (perror != NULL) 
			g_set_error(perror,
				FULM_TYPE_DATA_MANAGER_ERROR, 
				FULM_DATA_MANAGER_ERROR_PIN_ALREADY_CONNECTED,
				"Target pin is already connected");
		return FALSE;
	}

	// check source pin is an output pin
	if (fulm_component_pin_get_pin_type(source_pin) != FULM_COMPONENT_PIN_TYPE_OUTPUT) {
		if (perror != NULL) 
			g_set_error(perror,
				FULM_TYPE_DATA_MANAGER_ERROR, 
				FULM_DATA_MANAGER_ERROR_PIN_INVALID_PIN_TYPE,
				"Source pin is not an output pin");
		return FALSE;
	}

	// check target pin is an input pin
	if (fulm_component_pin_get_pin_type(target_pin) != FULM_COMPONENT_PIN_TYPE_INPUT) {
		if (perror != NULL) 
			g_set_error(perror,
				FULM_TYPE_DATA_MANAGER_ERROR, 
				FULM_DATA_MANAGER_ERROR_PIN_INVALID_PIN_TYPE,
				"Target pin is not an input pin");
		return FALSE;
	}

	FulmComponent* source_component = fulm_component_pin_get_component(source_pin);
	g_assert(source_component != NULL);
	FulmComponent* target_component = fulm_component_pin_get_component(target_pin);
	g_assert(target_component != NULL);

	const gchar* source_component_key = fulm_component_get_key(source_component);
	g_assert(source_component_key != NULL);
	const gchar* target_component_key = fulm_component_get_key(target_component);
	g_assert(target_component_key != NULL);

	const gchar* source_parent = fulm_component_get_parent_key(source_component);
	g_assert_nonnull(source_parent);
	const gchar* target_parent = fulm_component_get_parent_key(target_component);
	g_assert_nonnull(target_parent);

	if (g_strcmp0(source_parent, target_parent) != 0) {
		if (perror != NULL) 
			g_set_error(perror,
				FULM_TYPE_DATA_MANAGER_ERROR, 
				FULM_DATA_MANAGER_ERROR_DIFFERENT_CONTAINER,
				"Target and source components are not in the same container");
		return FALSE;
	}

	const gchar* source_pin_name = fulm_component_pin_get_name(source_pin);
	g_assert(source_pin_name != NULL);
	fulm_component_pin_set_connection(target_pin, source_component_key, source_pin_name);

	const gchar* target_pin_name = fulm_component_pin_get_name(target_pin);
	g_assert(target_pin_name != NULL);
	fulm_component_pin_set_connection(source_pin, target_component_key, target_pin_name);

	// store source and target component to save pin connection
	if (! fulm_data_manager_save_component(self, FULM_COMPONENT(source_component), perror))
		return FALSE;
	if (! fulm_data_manager_save_component(self, FULM_COMPONENT(target_component), perror))
		return FALSE;

	if (priv->edition_observer != NULL)
		fulm_edition_observer_pins_connected(priv->edition_observer, source_pin, target_pin);

	return TRUE;
}

void
fulm_data_manager_set_edition_observer(FulmDataManager* self, FulmEditionObserver* edition_observer)
{
	g_assert_true(FULM_IS_DATA_MANAGER(self));
	g_assert_true(FULM_IS_EDITION_OBSERVER(edition_observer));
	FulmDataManagerPrivate* priv = fulm_data_manager_get_instance_private(self);
	g_assert_null(priv->edition_observer); // one time set function
	priv->edition_observer = edition_observer;
	g_object_ref(priv->edition_observer);
}
