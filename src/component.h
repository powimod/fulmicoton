/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FULM_COMPONENT__
#define __FULM_COMPONENT__

#include <glib-object.h>
#include "global.h"
#include "storable.h"
#include "application.h"
#include "component-pin.h"

#define FULM_TYPE_COMPONENT fulm_component_get_type()

G_DECLARE_DERIVABLE_TYPE (FulmComponent, fulm_component, FULM, COMPONENT, GObject);


struct _FulmComponentClass
{
	GObjectClass parent_class;
	const gchar* (*get_type_name) (FulmComponent* component);
	void (*save_to_json) (FulmComponent* component, JsonBuilder *json_builder);
	void (*load_from_json) (FulmComponent* component, JsonReader* json_reader);

	const gchar** (*get_pin_names) (FulmComponent* component, FulmComponentPinType pin_type);
	gint (*get_pin_count) (FulmComponent* component, FulmComponentPinType pin_type);

	FulmComponentPin* (*get_pin_by_index) (FulmComponent* component, FulmComponentPinType pin_type, gint pin_index);
	FulmComponentPin* (*get_pin_by_name) (FulmComponent* component, FulmComponentPinType pin_type, const gchar* pin_name);

	// interface FulmStorable 
	gboolean (*storable_fetch) (FulmStorable* storable, FulmStorage* storage, GError** error);
	gboolean (*storable_store) (FulmStorable* storable, FulmStorage* storage, GError** error);

	GError* (*validate_pin_value) (FulmComponent* component, FulmComponentPin* pin, const gchar* value);
	GError* (*process_pin_update) (FulmComponent* component, FulmComponentPin* pin);
};


#define FULM_COMPONENT_EMPTY_PIN_ARRAY fulm_component_empty_pin_array
extern const gchar* fulm_component_empty_pin_array[];

void fulm_component_set_key(FulmComponent* component, const gchar* key);
void fulm_component_set_parent_key(FulmComponent* component, const gchar* key);


const gchar* fulm_component_get_key(FulmComponent* component);
const gchar* fulm_component_get_parent_key(FulmComponent* component);
void fulm_component_set_parent_key(FulmComponent* component, const gchar* parent_key);

const gchar* fulm_component_get_name(FulmComponent* component);
void fulm_component_set_name(FulmComponent* component, const gchar* name);

const gchar* fulm_component_get_type_name(FulmComponent* component);

void fulm_component_save_to_json (FulmComponent* component, JsonBuilder *json_builder);
void fulm_component_load_from_json (FulmComponent* component, JsonReader *json_reader);


/* pin functions */

const gchar** fulm_component_get_pin_names(FulmComponent* component, FulmComponentPinType pin_type);

gint fulm_component_get_pin_count(FulmComponent* component, FulmComponentPinType pin_type);

FulmComponentPin* fulm_component_get_pin_by_index (FulmComponent* component, FulmComponentPinType pin_type, gint pin_index);
FulmComponentPin* fulm_component_get_pin_by_name (FulmComponent* component, FulmComponentPinType pin_type, const gchar* pin_name);


GError* fulm_component_validate_pin_value(FulmComponent* component, FulmComponentPin* pin, const gchar* value);
GError* fulm_component_process_pin_update (FulmComponent* self, FulmComponentPin* pin);

#endif /*__FULM_COMPONENT__*/
