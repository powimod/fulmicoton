/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */



#ifndef __FULM_STORABLE__
#define __FULM_STORABLE__

#include <glib-object.h>
#include "global.h"
#include "storage.h"

#define FULM_TYPE_STORABLE fulm_storable_get_type()

G_DECLARE_INTERFACE(FulmStorable, fulm_storable, FULM, STORABLE, GObject)

struct _FulmStorableInterface
{
	GTypeInterface parent_iface;
	gboolean (*store) (FulmStorable* storable, FulmStorage* storage, GError** error);
	gboolean (*fetch) (FulmStorable* storable, FulmStorage* storage, GError** error);
};

gboolean fulm_storable_store (FulmStorable* storable, FulmStorage* storage, GError** error);
gboolean fulm_storable_fetch (FulmStorable* storable, FulmStorage* storage, GError** error);

#endif /*__FULM_STORABLE__*/
