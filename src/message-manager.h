/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FULM_MESSAGE_MANAGER__
#define __FULM_MESSAGE_MANAGER__

#include <glib-object.h>
#include "global.h"
#include "storable.h"
#include "application.h"
#include "message.h"

#define FULM_TYPE_MESSAGE_MANAGER fulm_message_manager_get_type()

G_DECLARE_DERIVABLE_TYPE (FulmMessageManager, fulm_message_manager, FULM, MESSAGE_MANAGER, GObject);


struct _FulmMessageManagerClass
{
	GObjectClass parent_class;
};

FulmMessageManager* fulm_message_manager_new(FulmStorage* message_storage);
gchar** fulm_message_manager_get_message_keys(FulmMessageManager* message_manager);
const gchar* fulm_message_manager_get_message_key_by_index(FulmMessageManager* message_manager, guint index);
const gchar* fulm_message_manager_get_first_message_key(FulmMessageManager* message_manager);
guint fulm_message_manager_get_message_count(FulmMessageManager* message_manager);
FulmMessage* fulm_message_manager_post_message(FulmMessageManager* message_manager, const gchar* component_key, const gchar* pin_name, const gchar* value);

// FIXME make these functions private ?
void fulm_message_manager_save_to_json (FulmMessageManager* message_manager, JsonBuilder *json_builder);
void fulm_message_manager_load_from_json (FulmMessageManager* message_manager, JsonReader *json_reader);


#endif /*__FULM_MESSAGE_MANAGER__*/
