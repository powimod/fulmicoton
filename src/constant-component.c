/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "constant-component.h"

#define _NAME_FIELD "name"
#define _PARENT_KEY_FIELD "pkey"
#define _VALUE_FIELD "value"

#define FULM_CONSTANT_COMPONENT_INPUT_PIN_COUNT 0
#define FULM_CONSTANT_COMPONENT_OUTPUT_PIN_COUNT 1
#define FULM_CONSTANT_COMPONENT_HIDDEN_PIN_COUNT 0

static const gchar* output_pin_names[] = { "value", NULL };

struct _FulmConstantComponent
{
	FulmComponent parent;
	FulmComponentPin* value_pin;
};


static const gchar* fulm_constant_component_get_type_name(FulmComponent* component);
static void fulm_constant_component_load_from_json (FulmComponent* component, JsonReader *json_reader);
static void fulm_constant_component_save_to_json (FulmComponent* component, JsonBuilder *json_builder);

static const gchar** fulm_constant_component_get_pin_names(FulmComponent* component, FulmComponentPinType pin_type);
static gint fulm_constant_component_get_pin_count(FulmComponent* component, FulmComponentPinType pin_type);
static FulmComponentPin* fulm_constant_component_get_pin_by_index (FulmComponent* component, FulmComponentPinType pin_type, gint pin_index);

G_DEFINE_TYPE(FulmConstantComponent, fulm_constant_component, FULM_TYPE_COMPONENT);

static void
fulm_constant_component_init(FulmConstantComponent* self)
{
	self->value_pin = fulm_component_pin_new(FULM_COMPONENT_PIN_TYPE_OUTPUT, output_pin_names[0], FULM_COMPONENT(self));
	fulm_component_pin_set_value(self->value_pin, "");
}

static void
fulm_constant_component_dispose(GObject* object)
{
	FulmConstantComponent* constant_component = FULM_CONSTANT_COMPONENT(object);
	g_object_unref(constant_component->value_pin);
	G_OBJECT_CLASS(fulm_constant_component_parent_class)->dispose(object);
}

static void
fulm_constant_component_finalize(GObject* object)
{
/*
	FulmConstantComponent* constant_component = FULM_CONSTANT_COMPONENT(object);
	G_OBJECT_CLASS(fulm_constant_component_parent_class)->finalize(object);
*/
}

static void
fulm_constant_component_class_init(FulmConstantComponentClass* constant_component_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(constant_component_class);
	FulmComponentClass* component_class = FULM_COMPONENT_CLASS(constant_component_class);
	object_class->dispose= fulm_constant_component_dispose;
	object_class->finalize = fulm_constant_component_finalize;
	component_class->get_type_name = fulm_constant_component_get_type_name;
	component_class->save_to_json = fulm_constant_component_save_to_json;
	component_class->load_from_json = fulm_constant_component_load_from_json;

	component_class->get_pin_names = fulm_constant_component_get_pin_names;
	component_class->get_pin_count = fulm_constant_component_get_pin_count;
	component_class->get_pin_by_index = fulm_constant_component_get_pin_by_index;
}


FulmComponent*
fulm_constant_component_new()
{
	return FULM_COMPONENT(g_object_new(FULM_TYPE_CONSTANT_COMPONENT, NULL));
}

const gchar*
fulm_constant_component_get_value(FulmConstantComponent* self)
{
	return fulm_component_pin_get_value(self->value_pin);
}

void
fulm_constant_component_set_value(FulmConstantComponent* self, const gchar* value)
{
	g_return_if_fail(value != NULL);
	fulm_component_pin_set_value(self->value_pin, value);
}

const gchar*
fulm_constant_component_type_name()
{
	return "constant-component";
}

static const gchar*
fulm_constant_component_get_type_name(FulmComponent* component)
{
	return fulm_constant_component_type_name();
}


static void
fulm_constant_component_save_to_json (FulmComponent* component, JsonBuilder *json_builder)
{
	FulmConstantComponent* self = FULM_CONSTANT_COMPONENT(component);
	json_builder_begin_object(json_builder);
	FULM_COMPONENT_CLASS(fulm_constant_component_parent_class)->save_to_json(component, json_builder);
	json_builder_set_member_name(json_builder, _VALUE_FIELD);
	json_builder_add_string_value(json_builder, fulm_constant_component_get_value(self));
	json_builder_end_object(json_builder);
}


static void
fulm_constant_component_load_from_json (FulmComponent* component, JsonReader *json_reader)
{
	FulmConstantComponent* self = FULM_CONSTANT_COMPONENT(component);
	FULM_COMPONENT_CLASS(fulm_constant_component_parent_class)->load_from_json(component, json_reader);

	json_reader_read_member(json_reader, _VALUE_FIELD );
	const gchar* value = json_reader_get_string_value(json_reader);
	json_reader_end_member(json_reader);
	fulm_constant_component_set_value(self, value);
}



// pin functions 

static const gchar**
fulm_constant_component_get_pin_names(FulmComponent* component, FulmComponentPinType pin_type)
{
        g_return_val_if_fail(FULM_IS_CONSTANT_COMPONENT(component), NULL);
	switch(pin_type)
	{
		case FULM_COMPONENT_PIN_TYPE_INPUT:
			return FULM_COMPONENT_EMPTY_PIN_ARRAY ;
		case FULM_COMPONENT_PIN_TYPE_OUTPUT:
			return output_pin_names;
		case FULM_COMPONENT_PIN_TYPE_HIDDEN:
			return FULM_COMPONENT_EMPTY_PIN_ARRAY ;
		default:
			g_error("Invalid pin type [%i]", pin_type);
	}
	g_assert_not_reached();
	return NULL;
}


static gint
fulm_constant_component_get_pin_count(FulmComponent* component, FulmComponentPinType pin_type)
{
        g_return_val_if_fail(FULM_IS_CONSTANT_COMPONENT(component), -1);
	//FulmConstantComponent* self = FULM_CONSTANT_COMPONENT(component);
        //FulmComponentClass *component_class = FULM_CONSTANT_COMPONENT_GET_CLASS(self);
	switch(pin_type)
	{
		case FULM_COMPONENT_PIN_TYPE_INPUT:
			return FULM_CONSTANT_COMPONENT_INPUT_PIN_COUNT;
		case FULM_COMPONENT_PIN_TYPE_OUTPUT:
			return FULM_CONSTANT_COMPONENT_OUTPUT_PIN_COUNT;
		case FULM_COMPONENT_PIN_TYPE_HIDDEN:
			return FULM_CONSTANT_COMPONENT_HIDDEN_PIN_COUNT;
	}
	g_assert_not_reached();
}



static FulmComponentPin*
fulm_constant_component_get_pin_by_index (FulmComponent* component, FulmComponentPinType pin_type, gint pin_index)
{
        g_return_val_if_fail(FULM_IS_CONSTANT_COMPONENT(component), NULL);
	g_return_val_if_fail(pin_index >= 0, NULL);
	FulmConstantComponent* self = FULM_CONSTANT_COMPONENT(component);
	switch(pin_type)
	{
		case FULM_COMPONENT_PIN_TYPE_INPUT:
			g_return_val_if_reached(NULL);

		case FULM_COMPONENT_PIN_TYPE_OUTPUT:
			g_return_val_if_fail(pin_index < FULM_CONSTANT_COMPONENT_OUTPUT_PIN_COUNT, NULL);
			return self->value_pin; 

		case FULM_COMPONENT_PIN_TYPE_HIDDEN:
			g_return_val_if_reached(NULL);
	}
	g_assert_not_reached();
	return NULL;
}


