/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FULM_DATA_MANAGER__
#define __FULM_DATA_MANAGER__

#include <glib-object.h>
#include "global.h"
#include "storable.h"
#include "component-factory.h"
#include "root-component.h"
#include "edition-observer.h"

#define FULM_TYPE_DATA_MANAGER fulm_data_manager_get_type()

G_DECLARE_DERIVABLE_TYPE (FulmDataManager, fulm_data_manager, FULM, DATA_MANAGER, GObject);


typedef enum {
	FULM_DATA_MANAGER_ERROR_COMPONENT_TYPE_FIELD_NOT_FOUND,
	FULM_DATA_MANAGER_ERROR_PIN_ALREADY_CONNECTED,
	FULM_DATA_MANAGER_ERROR_PIN_INVALID_PIN_TYPE,
	FULM_DATA_MANAGER_ERROR_DIFFERENT_CONTAINER,
	FULM_DATA_MANAGER_ERROR_COMPONENT_ALREADY_LINKED
} FulmDataManagerError;

#define FULM_TYPE_DATA_MANAGER_ERROR fulm_data_manager_error_quark()
GQuark fulm_data_manager_error_quark(void);


struct _FulmDataManagerClass
{
	GObjectClass parent_class;
};


FulmDataManager* fulm_data_manager_new(FulmStorage* data_storage);
FulmComponent* fulm_data_manager_create_component(FulmDataManager* data_manager, const gchar* type_name, FulmCompositeComponent *container, GError** perror);
FulmRootComponent* fulm_data_manager_get_root_component(FulmDataManager* data_manager, GError** perror);

FulmComponent* fulm_data_manager_load_component(FulmDataManager* data_manager, const gchar* component_key, GError** perror);
gboolean fulm_data_manager_save_component(FulmDataManager* data_manager, FulmComponent* component, GError** perror);

gboolean fulm_data_manager_connect_pins(FulmDataManager* data_manager, 
	FulmComponentPin* source_pin, FulmComponentPin* target_pin, GError **perror);
void fulm_data_manager_set_edition_observer(FulmDataManager* data_manager, FulmEditionObserver* edition_observer);

#endif /*__FULM_DATA_MANAGER__*/
