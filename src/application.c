/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"
#include "composite-component.h"
#include "constant-component.h"
#include "console-component.h"
#include <stdlib.h> // FIXME useful ?

#define FULM_APPLICATION_ID "org.gnome.powimod.fulmicoton" 

struct _FulmApplication
{
	GApplication parent;
	FulmStorage* storage;
	FulmComponentFactory* component_factory;
	FulmCompositeComponent* root_component;
};

G_DEFINE_TYPE(FulmApplication, fulm_application, G_TYPE_APPLICATION)

static void fulm_application_create_sample(FulmApplication* self);
static gboolean _fulm_application_timeout(gpointer data);

static void
fulm_application_init(FulmApplication* self)
{
	gchar* db_path = g_build_filename(g_get_user_data_dir(), "fulmicoton.db", NULL);
	g_print("DB Path : %s\n", db_path);
        GFile* db_file =  g_file_new_for_path(db_path);
	g_free(db_path);

	g_print("dOm Creating component factory...\n");
	self->component_factory = fulm_component_factory_new();
	fulm_component_factory_register(self->component_factory, fulm_constant_component_type_name(), fulm_constant_component_new);
	fulm_component_factory_register(self->component_factory, fulm_composite_component_type_name(), fulm_composite_component_new);
	fulm_component_factory_register(self->component_factory, fulm_console_component_type_name(), fulm_console_component_new);
	g_print("dOm done\n");

	FulmComponent* root_component = fulm_component_factory_build(self->component_factory, fulm_composite_component_type_name());
	g_assert(root_component != NULL);
	g_assert(FULM_IS_COMPOSITE_COMPONENT(root_component));
	self->root_component = FULM_COMPOSITE_COMPONENT(root_component);
	fulm_component_set_key(root_component, "#root");
	

        GError* error = NULL;
        FulmGdbmStorage* gdbm_storage = fulm_gdbm_storage_new(db_file);
        FulmStorage* storage = FULM_STORAGE(gdbm_storage);
        if (! g_file_query_exists(db_file, NULL)){
		g_print("Creating database\n");
                fulm_storage_initialize(storage, &error);
                if (G_UNLIKELY(error != NULL)) {
                        g_error("DB init error : %s\n", error->message);
                        g_error_free(error);
                }

		fulm_component_set_name(FULM_COMPONENT(root_component), "root");
		fulm_storable_store(FULM_STORABLE(root_component), storage, &error);
		if (G_UNLIKELY(error != NULL)) {
                        g_error("Root component fetching error : %s\n", error->message);
                        g_error_free(error);
                }

		self->storage = storage;
		fulm_application_create_sample(self);
        }
        else {
		g_print("Opening database\n");
                fulm_storage_open(storage, &error);
		if (G_UNLIKELY(error != NULL)) {
                        g_error("DB open error : %s\n", error->message);
                        g_error_free(error);
                }
		fulm_storable_fetch(FULM_STORABLE(root_component), storage, &error);
		if (G_UNLIKELY(error != NULL)) {
                        g_error("Root component loading error : %s\n", error->message);
                        g_error_free(error);
                }
		self->storage = storage;
        }


	g_print("dOm root component loaded : %s\n", fulm_component_get_name(FULM_COMPONENT(root_component)));

	g_timeout_add(1000, _fulm_application_timeout, self);

}

static void
fulm_application_dispose(GObject* object)
{
	g_return_if_fail(FULM_IS_APPLICATION(object));
	FulmApplication* self = FULM_APPLICATION(object);
	g_print("fulm_application_dispose\n");

	g_object_unref(self->component_factory);
	g_object_unref(self->root_component);

	GError* error =  NULL;
	fulm_storage_close(self->storage, &error);
	g_object_unref(self->storage);

	G_OBJECT_CLASS(fulm_application_parent_class)->dispose(object);
}

static void
fulm_application_finalize(GObject* object)
{
	g_return_if_fail(FULM_IS_APPLICATION(object));
	g_print("fulm_application_finalize\n");
	G_OBJECT_CLASS(fulm_application_parent_class)->finalize(object);
}

static void
quit_action(GAction* action, GVariant* parameter, gpointer data)
{
	GApplication* g_application = G_APPLICATION(data);
	g_print("Quit action launched...\n");
	g_application_release(g_application); /* release lock which was done in fulm_application_activate*/
}

static void
_fulm_application_declare_actions(FulmApplication* application)
{
	GSimpleAction* action;
	action = g_simple_action_new("quit", NULL);
	g_signal_connect(action, "activate", G_CALLBACK(quit_action), application);
	g_action_map_add_action(G_ACTION_MAP(application), G_ACTION(action));
	g_object_unref(action);

}

static void
fulm_application_startup(GApplication *g_application)
{
	FulmApplication* application = FULM_APPLICATION(g_application);
	g_print("fulm_application_startup\n");
	G_APPLICATION_CLASS(fulm_application_parent_class)->startup(g_application);
	_fulm_application_declare_actions(application);
}

static void
fulm_application_shutdown(GApplication *g_application)
{
	g_print("fulm_application_shutdown\n");
	G_APPLICATION_CLASS(fulm_application_parent_class)->shutdown(g_application);
}

static void
fulm_application_activate(GApplication *g_application)
{
	g_print("fulm_application_activate\n");
	G_APPLICATION_CLASS(fulm_application_parent_class)->shutdown(g_application);
	g_application_hold(g_application); // force application to stay in execution (until quit action)
}


static void
fulm_application_class_init(FulmApplicationClass* class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(class);
	object_class->dispose = fulm_application_dispose;
	object_class->finalize = fulm_application_finalize;

	GApplicationClass* g_application_class = G_APPLICATION_CLASS(class);
	g_application_class->startup = fulm_application_startup;
	g_application_class->shutdown = fulm_application_shutdown;
	g_application_class->activate = fulm_application_activate;
}

FulmApplication*
fulm_application_new(void)
{
	FulmApplication* self = g_object_new(FULM_TYPE_APPLICATION, 
		"application-id", FULM_APPLICATION_ID,
		"flags", G_APPLICATION_FLAGS_NONE/*G_APPLICATION_IS_SERVICE*/, 
		NULL);
	return self;
}

const FulmStorage*
fulm_application_get_storage(FulmApplication* self)
{
	return self->storage;
}

const FulmComponentFactory*
fulm_application_get_component_factory(FulmApplication* self)
{
	return self->component_factory;
}


static void fulm_application_create_sample(FulmApplication* self)
{
	g_print("Hello world sample creation...\n");
	g_assert(self->storage != NULL);

	GError* error = NULL;

	const gchar* parent_key = fulm_component_get_key(FULM_COMPONENT(self->root_component));

	FulmComponent* constant_component = fulm_component_factory_build(self->component_factory, fulm_constant_component_type_name());
	g_assert(constant_component != NULL);
	g_assert(FULM_IS_CONSTANT_COMPONENT(constant_component));

	gchar* key = fulm_storage_generate_unique_key(self->storage, NULL);
	fulm_component_set_key(constant_component, key);
	fulm_component_set_parent_key(constant_component, parent_key);
	g_free(key);
	fulm_component_set_name(constant_component, "message");
	fulm_constant_component_set_value(FULM_CONSTANT_COMPONENT(constant_component), "Hello World !");
	fulm_storable_store(FULM_STORABLE(constant_component), self->storage, NULL);


	FulmComponent* constant_component_2 = fulm_component_factory_build(self->component_factory, fulm_constant_component_type_name());
	g_assert(constant_component_2 != NULL);
	g_assert(FULM_IS_CONSTANT_COMPONENT(constant_component_2));
	key = fulm_storage_generate_unique_key(self->storage, NULL);
	fulm_component_set_key(constant_component_2, key);
	fulm_component_set_parent_key(constant_component_2, parent_key);

	g_free(key);
	fulm_component_set_name(constant_component_2, "comment");
	fulm_constant_component_set_value(FULM_CONSTANT_COMPONENT(constant_component_2), "a simple example");
	fulm_storable_store(FULM_STORABLE(constant_component_2), self->storage, NULL);

	fulm_composite_component_add_component(self->root_component, FULM_COMPONENT(constant_component_2), &error);
	g_assert(error == NULL);
	fulm_composite_component_add_component(self->root_component, FULM_COMPONENT(constant_component), &error);
	g_assert(error == NULL);
	fulm_storable_store(FULM_STORABLE(self->root_component), self->storage, NULL);
	g_assert(error == NULL);


}


static gboolean
_fulm_application_timeout(gpointer data)
{
	FulmApplication* application = FULM_APPLICATION(data);

	static gint i = 0;
	i++;
	g_print("Callback n°%i\n", i);
	if (i == 3) {
		g_print("It's time to quit!\n");
		GAction* quit_action = g_action_map_lookup_action(G_ACTION_MAP(application), "quit");
		g_assert(quit_action != NULL);
		g_action_activate(quit_action, NULL);
		
	}

	return TRUE;
}
