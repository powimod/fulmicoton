/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */



#include "storage.h"
#include "gdbm-storage.h"

G_DEFINE_TYPE(FulmStorage, fulm_storage, G_TYPE_OBJECT)

GQuark
fulm_storage_error_quark(void)
{
	return g_quark_from_static_string("fulm-type-storage-error-quark");
}


static void
fulm_storage_init(FulmStorage* self)
{
}

static void
fulm_storage_dispose(GObject* object)
{
	G_OBJECT_CLASS(fulm_storage_parent_class)->dispose(object);
}

static void
fulm_storage_finalize(GObject* object)
{
	G_OBJECT_CLASS(fulm_storage_parent_class)->finalize(object);
}

static void
fulm_storage_class_init(FulmStorageClass* storage_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(storage_class);
	object_class->dispose = fulm_storage_dispose;
	object_class->finalize = fulm_storage_finalize;
	storage_class->open = NULL; /* virtual function */
	storage_class->initialize = NULL; /* virtual function */
	storage_class->close = NULL; /* virtual function */
	storage_class->is_opened = NULL; /* virtual function */

	storage_class->delete_by_key = NULL; /* virtual function */

	storage_class->store_string = NULL; /* virtual function */
	storage_class->fetch_string = NULL; /* virtual function */

	storage_class->store_json = NULL; /* virtual function */
	storage_class->fetch_json = NULL; /* virtual function */

/* FIXME remove this functions
	storage_class->store_component = NULL; // virtual function 
	storage_class->fetch_component = NULL; /. virtual function
*/

	storage_class->generate_unique_key = NULL; /* virtual function */
	storage_class->generate_unique_key_with_prefix = NULL; /* virtual function */
}

gboolean
fulm_storage_initialize(FulmStorage* self, GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert(storage_class->initialize != NULL);
	storage_class->initialize(self, error);
	return TRUE;
}

gboolean
fulm_storage_open(FulmStorage* self, GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert(storage_class->open != NULL);
	storage_class->open(self, error);
	return TRUE;
}

gboolean
fulm_storage_close(FulmStorage* self, GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert(storage_class->close != NULL);
	storage_class->close(self, error);
	return TRUE;
}

gboolean
fulm_storage_is_opened(FulmStorage* self)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert(storage_class->is_opened != NULL);
	storage_class->is_opened(self);
	return TRUE;
}


gboolean
fulm_storage_delete_by_key(FulmStorage* self, const gchar* key, GError** perror)
{
	g_assert_true(FULM_IS_STORAGE(self));
	g_assert_nonnull(key);
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert_nonnull(storage_class->delete_by_key);
	return storage_class->delete_by_key(self, key, perror);
}



gboolean
fulm_storage_store_string(FulmStorage* self, const gchar* key, const gchar* value, GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert(storage_class->store_string != NULL);
	return storage_class->store_string(self, key, value, error);
}


gboolean
fulm_storage_fetch_string(FulmStorage* self, const gchar* key, gchar** pvalue, GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert(storage_class->store_string != NULL);
	return storage_class->fetch_string(self, key, pvalue, error);
}


gboolean
fulm_storage_store_json (FulmStorage* self, const gchar* key, JsonNode* json_node, GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert(storage_class->store_json != NULL);
	return storage_class->store_json(self, key, json_node, error);
}


gboolean
fulm_storage_fetch_json (FulmStorage* self, const gchar* key, JsonNode** p_json_node, GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert(storage_class->fetch_json != NULL);
	return storage_class->fetch_json(self, key, p_json_node, error);
}

/* FIXME remove this functions ?
gboolean
fulm_storage_store_component(FulmStorage* self, FulmComponent* component , GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert(storage_class->store_component != NULL);
	return storage_class->store_component(self, component, error);
}


gboolean
fulm_storage_fetch_component(FulmStorage* self, FulmComponent** pComponent, GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	g_assert(storage_class->store_component != NULL);
	return storage_class->fetch_component(self, pComponent, error);
}
*/


FulmStorage*
fulm_storage_new(FulmStorageType storage_type, GFile* storage_file, GError** error)
{
	FulmStorage* storage = NULL;
	switch (storage_type) {
		case FULM_STORAGE_TYPE_DEFAULT:
		case FULM_STORAGE_TYPE_GDBM:
			storage = FULM_STORAGE(fulm_gdbm_storage_new(storage_file));
			break;
		default:
			if (error != NULL)
				g_set_error(error,  FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_INVALID_TYPE,
					"Invalid storage type");
			return NULL;
	}
	g_assert(storage != NULL);
	return storage;
}

gchar*
fulm_storage_generate_unique_key(FulmStorage* self, GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	return storage_class->generate_unique_key(self, error);
}


gchar*
fulm_storage_generate_unique_key_with_prefix(FulmStorage* self, const gchar* prefix, GError** error)
{
	FulmStorageClass* storage_class = FULM_STORAGE_GET_CLASS(self);
	return storage_class->generate_unique_key_with_prefix(self, prefix, error);
}
