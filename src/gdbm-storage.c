/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */



#include <dbm.h>
#include "gdbm-storage.h"

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "Fulmicoton-GdbmStorage"

#define FULM_GDBM_STORAGE_KEY_COUNTER "fulm_storage_key_counter"

#ifdef G_OS_WIN32
#include <fileapi.h> // Windows file attribute SetFileAttributes
#endif


struct _FulmGdbmStorage {
	FulmStorage parent;
	GFile* db_file;	
	GDBM_FILE gdbf;
};

G_DEFINE_TYPE(FulmGdbmStorage, fulm_gdbm_storage, FULM_TYPE_STORAGE)


gboolean fulm_gdbm_storage_close(FulmStorage* gdbm_storage, GError** perror);

static gboolean fulm_gdbm_storage_delete_by_key(FulmStorage* storage, const gchar* key, GError** error);
static gboolean fulm_gdbm_storage_store_string(FulmStorage* storage, const gchar* key, const gchar *value, GError** perror);


static void
fulm_gdbm_storage_init(FulmGdbmStorage* self)
{
	self->db_file = NULL;
	self->gdbf = NULL;
}

static void
fulm_gdbm_storage_dispose(GObject* object)
{
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(object);
	if (self->gdbf != NULL)
		fulm_gdbm_storage_close(FULM_STORAGE(self), NULL);
	g_object_unref(self->db_file);
	self->db_file = NULL;
	G_OBJECT_CLASS(fulm_gdbm_storage_parent_class)->dispose(object);
}

static void
fulm_gdbm_storage_finalize(GObject* object)
{
	G_OBJECT_CLASS(fulm_gdbm_storage_parent_class)->finalize(object);
}

gboolean
fulm_gdbm_storage_initialize(FulmStorage* storage, GError** perror)
{
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(storage);
	g_assert(self->db_file != NULL);
	const gchar* db_path = g_file_peek_path(self->db_file);
	if (self->gdbf != NULL){
		if (perror != NULL) 
			g_set_error(perror,  FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_ALREADY_OPENED,
				"Database file \"%s\" is already opened", db_path);
		return FALSE;
	}
	g_info("initializing dbm file [%s]...\n", db_path);
	if (g_file_query_exists(self->db_file, NULL)) {
		if (perror != NULL) 
			g_set_error(perror,  FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_ALREADY_EXISTS,
				"Database file \"%s\" already exists", db_path);
		return FALSE;
	}
	self->gdbf = gdbm_open (db_path, 0, GDBM_NEWDB, 0, NULL);
	if (self->gdbf == NULL) {
		// TODO return GDBM's error message
		if (perror != NULL) 
			g_set_error(perror,  FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_CANNOT_OPEN,
				"Can't open initialize file \"%s\" ", db_path);
		return FALSE;
	}

	gchar* value = g_strdup_printf("%i", 0);
	if (! fulm_gdbm_storage_store_string(FULM_STORAGE(self), FULM_GDBM_STORAGE_KEY_COUNTER, value, perror)){
		return FALSE;
	}
	g_free(value);

	g_print("->DBM file successfully initialized !\n");

	return TRUE;
}

gboolean
fulm_gdbm_storage_open(FulmStorage* storage, GError** perror)
{
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(storage);
	g_assert(self->db_file != NULL);
	const gchar* db_path = g_file_peek_path(self->db_file);
	if (self->gdbf != NULL){
		if (perror != NULL) 
			g_set_error(perror,  FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_ALREADY_OPENED,
				"Database file \"%s\" is already opened", db_path);
		return FALSE;
	}
	g_debug("opening dbm file [%s]...", db_path);
	if (! g_file_query_exists(self->db_file, NULL)) {
		if (perror != NULL) 
			g_set_error(perror,  FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_DOES_NOT_EXIST,
				"Database file \"%s\" does not exist", db_path);
		return FALSE;
	}
	self->gdbf = gdbm_open (db_path, 0, GDBM_WRITER, 0, NULL);
	if (self->gdbf == NULL) {
		if (perror != NULL) {
			g_set_error(perror,  FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_CANNOT_OPEN,
				"Can't open database file \"%s\" ", db_path);
		}
		return FALSE;
	}
	return TRUE;
}


gboolean
fulm_gdbm_storage_close(FulmStorage* storage, GError** perror)
{
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(storage);
	g_assert(self->db_file != NULL);
	const gchar* db_path = g_file_peek_path(self->db_file);
	g_print("closing dbm file [%s]...\n", db_path);
	if (self->gdbf == NULL){
		if (perror != NULL) 
			g_set_error(perror,  FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_NOT_OPENED,
				"Database file \"%s\" is closed", db_path);
		return FALSE;
	}
	gdbm_close (self->gdbf);
	// FIXME why is it necessary to clear FAT-style READONLY attribute ?
	// FIXME remove READONLY file attribute just after file is opened
#ifdef G_OS_WIN32
	// on Windows, read file attribute is set when file is not closed properly
	g_debug("Clear FAT-style READONLY attribute on DB file on Windows");
	SetFileAttributes(db_path, FILE_ATTRIBUTE_NORMAL); 
#endif
	self->gdbf = NULL;
	return TRUE;
}

gboolean
fulm_gdbm_storage_is_opened(FulmStorage* storage)
{
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(storage);
	return (self->gdbf != NULL);
}

static gboolean
fulm_gdbm_storage_delete_by_key(FulmStorage* storage, const gchar* key, GError** perror)
{
	g_assert_true(FULM_IS_STORAGE(storage));
	g_assert_nonnull(key);
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(storage);
	if (self->gdbf == NULL)
	{
		if (perror != NULL) 
			g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_NOT_OPENED, "Storage is closed");
		return FALSE;
	}

	datum datum_key;
	datum_key.dptr = (char*) key;
	datum_key.dsize = strlen(key) + 1; // include \0 at then end


	if (gdbm_delete(self->gdbf, datum_key) != 0){
		if (perror != NULL) 
			g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_CANNOT_STORE,
				"Can't store \"%s\" key in database", key);
		return FALSE;
	}

	return TRUE;
}

static gboolean
fulm_gdbm_storage_store_string(FulmStorage* storage, const gchar* key, const gchar *value, GError** perror)
{
	g_return_val_if_fail(key != NULL, FALSE);
	g_return_val_if_fail(value != NULL, FALSE);
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(storage);
	if (self->gdbf == NULL)
	{
		if (perror != NULL) 
			g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_NOT_OPENED, "Storage is closed");
		return FALSE;
	}

	datum datum_key;
	datum_key.dptr = (char*) key;
	datum_key.dsize = strlen(key) + 1; // include \0 at then end

	datum datum_value;
	datum_value.dptr = (char*) value;
	datum_value.dsize = strlen (value) + 1; // include \0 at then end

	if (gdbm_store(self->gdbf, datum_key, datum_value, GDBM_REPLACE) != 0){
		if (perror != NULL) 
			g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_CANNOT_STORE,
				"Can't store \"%s\" key in database", key);
		return FALSE;
	}
	return TRUE;
}

static gboolean
fulm_gdbm_storage_fetch_string(FulmStorage* storage, const gchar* key, gchar** pvalue, GError** perror)
{
	g_return_val_if_fail(key != NULL, FALSE);
	g_return_val_if_fail(pvalue != NULL, FALSE);
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(storage);
	if (self->gdbf == NULL)
	{
		if (perror != NULL) 
			g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_NOT_OPENED, "Storage is closed");
		return FALSE;
	}

	datum datum_key;
	datum_key.dptr = (gchar*) key;
	datum_key.dsize = strlen(key) + 1; // include \0 at then end

	datum datum_value = gdbm_fetch (self->gdbf, datum_key);

	if (datum_value.dptr == NULL) {
		if (gdbm_errno == GDBM_ITEM_NOT_FOUND) {
			// do not print a warning if key is not found
			if (perror != NULL) {
				g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_KEY_NOT_FOUND,
					"Key \"%s\" not found in database", key);
			}
			return FALSE;
		}
		else {
			gchar* gdbm_error_message = g_strdup(gdbm_db_strerror(self->gdbf));
			g_warning("Can't fetch Key \"%s\" from database : %s", key, gdbm_error_message);
			if (perror != NULL) 
				g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_CANNOT_FETCH,
					"Can't fetch Key \"%s\" from database : %s", key, gdbm_error_message);
			return FALSE;
		}
	}

	*pvalue = datum_value.dptr;
	return TRUE;

}

static gboolean
fulm_gdbm_storage_store_json (FulmStorage* storage, const gchar* key, JsonNode* json_node, GError** perror)
{
	g_return_val_if_fail(key != NULL, FALSE);
	g_return_val_if_fail(json_node != NULL, FALSE);
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(storage);
	if (self->gdbf == NULL)
	{
		if (perror != NULL) 
			g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_NOT_OPENED, "Storage is closed");
		return FALSE;
	}

	JsonGenerator *generator = json_generator_new();
	json_generator_set_root(generator, json_node);
	gchar *str = json_generator_to_data (generator, NULL);
	g_print("fulm_gdbm_storage_store_json [%s] : %s\n", key, str);
	g_object_unref(generator);

	// TODO code factorisation in fulm_gdbm_storage_store_data
	datum datum_key;
	datum_key.dptr = (char*) key;
	datum_key.dsize = strlen(key) + 1; // include \0 at then end

	datum datum_value;
	datum_value.dptr = (char*) str;
	datum_value.dsize = strlen (str) + 1; // include \0 at then end

	if (gdbm_store(self->gdbf, datum_key, datum_value, GDBM_REPLACE) != 0){
		if (perror != NULL) 
			g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_CANNOT_STORE,
				"Can't store \"%s\" key in database", key);
		g_free(str);
		return FALSE;
	}

	g_free(str);

	return TRUE;
}


static gboolean
fulm_gdbm_storage_fetch_json (FulmStorage* storage, const gchar* key, JsonNode** p_json_node, GError** perror)
{
	g_return_val_if_fail(key != NULL, FALSE);
	g_return_val_if_fail(p_json_node != NULL, FALSE);
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(storage);
	if (self->gdbf == NULL)
	{
		if (perror != NULL) 
			g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_DATABASE_NOT_OPENED, "Storage is closed");
		return FALSE;
	}

	gchar* json_buffer = NULL;
	if (!fulm_gdbm_storage_fetch_string (storage, key, &json_buffer, perror))
		return FALSE;

	JsonParser* parser = json_parser_new();
	if (! json_parser_load_from_data(parser, json_buffer, -1, perror)) {
		g_warning("Unable to parse JSON : %s\n", (*perror)->message);
		g_set_error(perror, FULM_TYPE_STORAGE_ERROR, FULM_STORAGE_ERROR_INVALID_JSON,
			"Invalide JSON with key \"%s\"", key);
		g_object_unref(parser);
		g_free(json_buffer);
		return FALSE;
	}

	*p_json_node = json_parser_get_root (parser);
	json_node_ref(*p_json_node); // prevent node to be freed when parser is destroyed
	json_node_seal(*p_json_node);

	g_object_unref(parser);
	g_return_val_if_fail(*p_json_node != NULL, FALSE);

	return TRUE;
}


// TODO use fulm_storage_generate_unique_key_with_prefix
static gchar*
fulm_gdbm_storage_generate_unique_key(FulmStorage* self, GError** perror)
{
	g_assert(FULM_IS_GDBM_STORAGE(self));
	gulong key_count = 0; 
	
	gchar* value = NULL;
	if (! fulm_gdbm_storage_fetch_string(self, FULM_GDBM_STORAGE_KEY_COUNTER, &value, perror)) {
		g_error("Can't fetch [%s] in database !\n", FULM_GDBM_STORAGE_KEY_COUNTER);
		g_free(value);
		return NULL;
	}

	key_count = atol(value);
	key_count++;

	value = g_strdup_printf("%lu", key_count);
	g_assert(value != NULL);
	if (! fulm_gdbm_storage_store_string(self, FULM_GDBM_STORAGE_KEY_COUNTER, value, perror)) {
		g_free(value);
		return NULL;
	}

	return value;
}

static gchar*
fulm_gdbm_storage_generate_unique_key_with_prefix(FulmStorage* self, const gchar* prefix, GError** perror)
{
	g_assert(FULM_IS_GDBM_STORAGE(self));
	g_assert_nonnull(prefix);

	gchar* counter_str = NULL;
	GError* internal_error = NULL;
	gulong counter = 0;

	gchar* counter_key = g_strdup_printf("KEY#%s", prefix);

	fulm_gdbm_storage_fetch_string(self, counter_key, &counter_str, &internal_error); 

	if (internal_error == NULL) {
		// assume this is a key not found error
		counter = atol(counter_str);
		g_free(counter_str);
	}
	else {
		if (internal_error->code !=  FULM_STORAGE_ERROR_KEY_NOT_FOUND){
			g_free(counter_str);
			return NULL;
		}
	}

	counter++;
	counter_str = g_strdup_printf("%lu", counter);
	g_assert(counter_str != NULL);
	g_clear_error(perror);
	if (! fulm_gdbm_storage_store_string(self, counter_key, counter_str, perror)) {
		g_free(counter_str);
		g_free(counter_key);
		return NULL;
	}
	g_free(counter_key);

	gchar* key_str = g_strdup_printf("%s#%lu", prefix, counter);
	return key_str;
}

static void
fulm_gdbm_storage_class_init(FulmGdbmStorageClass* gdbm_storage_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(gdbm_storage_class);
	object_class->dispose = fulm_gdbm_storage_dispose;
	object_class->finalize = fulm_gdbm_storage_finalize;

	FulmStorageClass* storage_class = FULM_STORAGE_CLASS(gdbm_storage_class);
	storage_class->initialize = fulm_gdbm_storage_initialize;
	storage_class->open = fulm_gdbm_storage_open;
	storage_class->close = fulm_gdbm_storage_close;
	storage_class->is_opened = fulm_gdbm_storage_is_opened;

	storage_class->delete_by_key = fulm_gdbm_storage_delete_by_key;

	storage_class->store_string = fulm_gdbm_storage_store_string;
	storage_class->fetch_string = fulm_gdbm_storage_fetch_string;

	storage_class->store_json = fulm_gdbm_storage_store_json;
	storage_class->fetch_json = fulm_gdbm_storage_fetch_json;

	/*FIXME remove this functions ?
	storage_class->store_component = fulm_gdbm_storage_store_component;
	storage_class->fetch_component = fulm_gdbm_storage_fetch_component;
	*/

	storage_class->generate_unique_key = fulm_gdbm_storage_generate_unique_key;
	storage_class->generate_unique_key_with_prefix = fulm_gdbm_storage_generate_unique_key_with_prefix;
}



FulmGdbmStorage*
fulm_gdbm_storage_new(GFile* db_file)
{
	g_return_val_if_fail(db_file != NULL && G_IS_FILE(db_file), NULL);
	
	FulmGdbmStorage* self = FULM_GDBM_STORAGE(g_object_new(FULM_TYPE_GDBM_STORAGE, NULL));
	self->db_file = db_file;
	g_assert(self->db_file != NULL);
	g_object_ref(G_OBJECT(db_file));
	return self;
}



