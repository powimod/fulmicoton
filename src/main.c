/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */



#include <glib.h>
#include "application.h"
#include "storage.h"
#include "constant-component.h"
#include "composite-component.h"

int main(int argc, char** argv)
{
	g_print("Creating application...\n");
	FulmApplication* application = fulm_application_new();

	g_print("Starting application...\n");
	g_application_run(G_APPLICATION(application), argc, argv);
	
	g_object_unref(application);
	g_print("End\n");
	return 0;
}
