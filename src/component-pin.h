/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FULM_COMPONENT_PIN__
#define __FULM_COMPONENT_PIN__

#include <glib-object.h>
#include "global.h"

G_BEGIN_DECLS


typedef struct _FulmComponent FulmComponent;

typedef enum _FulmComponentPinType FulmComponentPinType;
enum _FulmComponentPinType
{
	FULM_COMPONENT_PIN_TYPE_INPUT = 11, // TODO use 1, 2, 3
	FULM_COMPONENT_PIN_TYPE_OUTPUT = 12,
	FULM_COMPONENT_PIN_TYPE_HIDDEN = 13
};

#define FULM_TYPE_COMPONENT_PIN fulm_component_pin_get_type()

G_DECLARE_FINAL_TYPE(FulmComponentPin, fulm_component_pin, FULM, COMPONENT_PIN, GObject)

FulmComponentPin* fulm_component_pin_new(FulmComponentPinType type, const gchar* name, FulmComponent* component);

FulmComponentPinType fulm_component_pin_get_pin_type(FulmComponentPin* component_pin);
const gchar* fulm_component_pin_get_name(FulmComponentPin* component_pin);

const gchar* fulm_component_pin_get_value(FulmComponentPin* component_pin);
void fulm_component_pin_set_value(FulmComponentPin* component_pin, const gchar* value);


const gchar* fulm_component_pin_get_connected_component_key(FulmComponentPin* self);
const gchar* fulm_component_pin_get_connected_pin_name(FulmComponentPin* self);
void fulm_component_pin_set_connection(FulmComponentPin* component_pin, const gchar* component_key, const gchar* pin_name);

FulmComponent* fulm_component_pin_get_component(FulmComponentPin* component_pin);

G_END_DECLS

#endif /*__FULM_COMPONENT_PIN__*/

