/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */



#ifndef __FULM_STORAGE_H__
#define __FULM_STORAGE_H__

#include <glib-object.h>
#include <gio/gio.h>
#include <json-glib/json-glib.h>
#include "global.h"

#define FULM_TYPE_STORAGE fulm_storage_get_type()

G_DECLARE_DERIVABLE_TYPE(FulmStorage, fulm_storage, FULM, STORAGE, GObject)

typedef enum {
	FULM_STORAGE_ERROR_INVALID_TYPE,
	FULM_STORAGE_ERROR_DATABASE_ALREADY_OPENED,
	FULM_STORAGE_ERROR_DATABASE_ALREADY_EXISTS,
	FULM_STORAGE_ERROR_DATABASE_DOES_NOT_EXIST,
	FULM_STORAGE_ERROR_DATABASE_NOT_OPENED,
	FULM_STORAGE_ERROR_DATABASE_CANNOT_OPEN,
	FULM_STORAGE_ERROR_CANNOT_STORE,
	FULM_STORAGE_ERROR_CANNOT_FETCH,
	FULM_STORAGE_ERROR_KEY_NOT_FOUND,
	FULM_STORAGE_ERROR_INVALID_JSON
} FulmStorageError;

#define FULM_TYPE_STORAGE_ERROR fulm_storage_error_quark()
GQuark fulm_storage_error_quark(void);

typedef enum {
	FULM_STORAGE_TYPE_DEFAULT = 0,
	FULM_STORAGE_TYPE_GDBM
} FulmStorageType;



struct _FulmStorageClass{
	GObjectClass parent_class;
	gboolean (*initialize) (FulmStorage* self, GError** err);
	gboolean (*open) (FulmStorage* self, GError** err);
	gboolean (*close) (FulmStorage* self, GError** err);
	gboolean (*is_opened) (FulmStorage* self);
	gchar* (*generate_unique_key)(FulmStorage* storage, GError** error);
	gchar* (*generate_unique_key_with_prefix)(FulmStorage* storage, const gchar* prefix, GError** error);

	gboolean (*delete_by_key) (FulmStorage* self, const gchar* key, GError** error);

	gboolean (*store_string) (FulmStorage* self, const gchar* key, const gchar* value, GError** error);
	gboolean (*fetch_string) (FulmStorage* self, const gchar* key, gchar** pvalue, GError** error);

	gboolean (*store_json) (FulmStorage* self, const gchar* key, JsonNode* json_node, GError** error);
	gboolean (*fetch_json) (FulmStorage* self, const gchar* key, JsonNode** p_json_node, GError** error);
	/* FIXME remove this functions ? */
/*
	gboolean (*store_component) (FulmStorage* self, FulmComponent* component , GError** error);
	gboolean (*fetch_component) (FulmStorage* self, FulmComponent** pComponent, GError** error);
*/
};

gboolean fulm_storage_initialize(FulmStorage* self, GError** error);
gboolean fulm_storage_open(FulmStorage* self, GError** error);
gboolean fulm_storage_close(FulmStorage* self, GError** error);
gboolean fulm_storage_is_opened(FulmStorage* self);

gboolean fulm_storage_delete_by_key(FulmStorage* self, const gchar* key, GError** error);

gboolean fulm_storage_store_string(FulmStorage* self, const gchar* key, const gchar* value, GError** error);
gboolean fulm_storage_fetch_string(FulmStorage* self, const gchar* key, gchar** pvalue, GError** error);

gboolean fulm_storage_store_json (FulmStorage* self, const gchar* key, JsonNode* json_node, GError** error);
gboolean fulm_storage_fetch_json (FulmStorage* self, const gchar* key, JsonNode** p_json_node, GError** error);

/* FIXME remove this functions ?
gboolean fulm_storage_store_component(FulmStorage* self, FulmComponent* component , GError** error);
gboolean fulm_storage_fetch_component(FulmStorage* self, FulmComponent** pComponent, GError** error);
*/

FulmStorage* fulm_storage_new(FulmStorageType storage_type, GFile* storage_file, GError** err);
gchar* fulm_storage_generate_unique_key(FulmStorage* self, GError** error);
gchar* fulm_storage_generate_unique_key_with_prefix(FulmStorage* self, const gchar* prefix, GError** error);

#endif /*__FULM_STORAGE_H__*/
