/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:fulm_message_manager
 * @Title: FulmMessageManager
 * @short_description:
 *
 * #FulmMessageManager is an abstract class for all message_managers which can be used in Fulmicoton.
 *
 */

#include "message-manager.h"

#define _ID_FIELD "msg"
#define _MESSAGE_KEY_PREFIX "msg"
#define _MESSAGE_KEYS_FIELD "msg_keys"

typedef struct {
	FulmStorage* message_storage;
	GSList *message_key_list;
	guint message_count;
} FulmMessageManagerPrivate;


G_DEFINE_TYPE_WITH_CODE(FulmMessageManager, fulm_message_manager, G_TYPE_OBJECT,
	G_ADD_PRIVATE(FulmMessageManager)
);


static void
fulm_message_manager_init(FulmMessageManager* message_manager)
{
	FulmMessageManagerPrivate* priv = fulm_message_manager_get_instance_private(message_manager);
	priv->message_storage = NULL;
	priv->message_key_list = NULL;
	priv->message_count = 0;
}

static void
fulm_message_manager_dispose(GObject* object)
{
	g_return_if_fail(FULM_IS_MESSAGE_MANAGER(object));
	FulmMessageManager* self = FULM_MESSAGE_MANAGER(object);
	FulmMessageManagerPrivate *priv = fulm_message_manager_get_instance_private(self);
	if (priv->message_storage != NULL)
		g_object_unref(priv->message_storage);
	G_OBJECT_CLASS(fulm_message_manager_parent_class)->dispose(object);
}


static void
fulm_message_manager_finalize(GObject* object)
{
	FulmMessageManager* message_manager = FULM_MESSAGE_MANAGER(object);
	FulmMessageManagerPrivate *priv = fulm_message_manager_get_instance_private(message_manager);
	g_slist_free_full(priv->message_key_list , g_free);
	G_OBJECT_CLASS(fulm_message_manager_parent_class)->finalize(object);
}


/*
const gchar*
fulm_message_manager_get_key(FulmMessageManager* message_manager)
{
	FulmMessageManagerPrivate *priv = fulm_message_manager_get_instance_private(message_manager);
	return (const gchar*)priv->key;
}
*/



static void
fulm_message_manager_class_init(FulmMessageManagerClass* message_manager_class)
{
	GObjectClass* object_class = G_OBJECT_CLASS(message_manager_class);
	object_class->dispose = fulm_message_manager_dispose;
	object_class->finalize = fulm_message_manager_finalize;
}



void
fulm_message_manager_save_to_json (FulmMessageManager* self, JsonBuilder *json_builder)
{
	g_assert(FULM_IS_MESSAGE_MANAGER(self));
	g_assert(JSON_IS_BUILDER(json_builder));
	FulmMessageManagerPrivate* priv = fulm_message_manager_get_instance_private(self);
	json_builder_begin_object(json_builder);
	json_builder_set_member_name(json_builder, _MESSAGE_KEYS_FIELD);
	json_builder_begin_array(json_builder);
	GSList* msg_pointer = priv->message_key_list;
	while (msg_pointer != NULL){
		gchar* msg_key = msg_pointer->data;
		json_builder_add_string_value(json_builder, msg_key);
		msg_pointer = g_slist_next(msg_pointer);
	}
	json_builder_end_array(json_builder);
	json_builder_end_object(json_builder);
}


void
fulm_message_manager_load_from_json (FulmMessageManager* self, JsonReader *json_reader)
{ 
	g_assert(FULM_IS_MESSAGE_MANAGER(self));
	g_assert(JSON_IS_READER(json_reader));
	FulmMessageManagerPrivate* priv = fulm_message_manager_get_instance_private(self);
	g_assert(priv->message_key_list == NULL);
	json_reader_read_member(json_reader, _MESSAGE_KEYS_FIELD);
	gint n = json_reader_count_elements(json_reader);
	priv->message_count = n;
	for (gint i = 0; i < n; i++) {
		if (! json_reader_read_element(json_reader,i))
			g_error("Can't read key n°%i", i);
		const gchar* message_key = json_reader_get_string_value(json_reader);
		priv->message_key_list = g_slist_append(priv->message_key_list, g_strdup(message_key));
		json_reader_end_element(json_reader);
	}
	json_reader_end_member(json_reader);
}

FulmMessageManager*
fulm_message_manager_new(FulmStorage* message_storage)
{
	g_assert_nonnull(message_storage);
	g_assert(FULM_IS_STORAGE(message_storage));
	FulmMessageManager* self = g_object_new(FULM_TYPE_MESSAGE_MANAGER, NULL);
	FulmMessageManagerPrivate* priv = fulm_message_manager_get_instance_private(self);
	priv->message_storage = message_storage;
	g_object_ref(priv->message_storage);
	return self;
}



// TODO should return a GError
FulmMessage*
fulm_message_manager_post_message(FulmMessageManager* message_manager, const gchar* component_key, const gchar* pin_name, const gchar* value)
{
	g_assert(FULM_IS_MESSAGE_MANAGER(message_manager));
	g_assert(component_key != NULL);
	g_assert(pin_name != NULL);
	g_assert(value != NULL);
	FulmMessageManagerPrivate* priv = fulm_message_manager_get_instance_private(message_manager);
	GError* error = NULL;
	gchar* message_key = fulm_storage_generate_unique_key_with_prefix(priv->message_storage, _MESSAGE_KEY_PREFIX, &error);
	g_assert_no_error(error);
	g_assert(message_key != NULL);
	FulmMessage* message = fulm_message_new_full(message_key, component_key, pin_name, value); 
	priv->message_key_list = g_slist_append(priv->message_key_list, message_key);
	priv->message_count++;
	return message;
}


guint
fulm_message_manager_get_message_count(FulmMessageManager* message_manager)
{
	g_assert(FULM_IS_MESSAGE_MANAGER(message_manager));
	FulmMessageManagerPrivate* priv = fulm_message_manager_get_instance_private(message_manager);
	return priv->message_count;
}

/**
 * fulm_message_manager_get_message_keys:
 * @message_manager : a #FulmMessageManager
 * Returns : (transfer container) : a null terminated array containing message keys
 */
gchar**
fulm_message_manager_get_message_keys(FulmMessageManager* message_manager)
{
	g_assert(FULM_IS_MESSAGE_MANAGER(message_manager));
	FulmMessageManagerPrivate* priv = fulm_message_manager_get_instance_private(message_manager);

	guint length = priv->message_count;
	gchar** keys_array = g_malloc(sizeof(const gchar*) * (length + 1));
	GSList *key_pointer = priv->message_key_list;
	gint i = 0;
	while (key_pointer != NULL){
		keys_array[i++] = key_pointer->data;
		key_pointer = g_slist_next(key_pointer);
	}
	keys_array[i] = NULL; // array terminated with NULL pointer
	return keys_array;
}

/**
 * fulm_message_manager_get_first_message_key:
 * @message_manager : a #FulmMessageManager
 * Returns : (nullable) (transfer none) : returns the key of the first message or *NULL if message list is empty
 */
const gchar*
fulm_message_manager_get_first_message_key(FulmMessageManager* message_manager)
{
	g_assert(FULM_IS_MESSAGE_MANAGER(message_manager));
	FulmMessageManagerPrivate* priv = fulm_message_manager_get_instance_private(message_manager);
	if (priv->message_count == 0)
		return NULL;
	g_assert(priv->message_key_list != NULL);
	return priv->message_key_list->data;
}

/**
 * fulm_message_manager_get_message_key_by_index:
 * @message_manager : a #FulmMessageManager
 * @index : index of the message key to get (first index is zero)
 * Returns : (nullable) (transfer none) : returns the message key or *NULL if out of range
 */
const gchar*
fulm_message_manager_get_message_key_by_index (FulmMessageManager* message_manager, guint searched_key_index)
{
	g_assert(FULM_IS_MESSAGE_MANAGER(message_manager));
	FulmMessageManagerPrivate* priv = fulm_message_manager_get_instance_private(message_manager);
	if (searched_key_index >= priv->message_count)
		return NULL;
	GSList *key_pointer = priv->message_key_list;
	guint index = 0;
	while (index++ < searched_key_index && key_pointer != NULL){
		key_pointer = g_slist_next(key_pointer);
	}
	g_assert_nonnull(key_pointer);
	return key_pointer->data;
}

