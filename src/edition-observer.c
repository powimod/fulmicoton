/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "edition-observer.h"

G_DEFINE_INTERFACE(FulmEditionObserver, fulm_edition_observer, G_TYPE_OBJECT)

void
fulm_edition_observer_component_created(FulmEditionObserver* edition_observer, FulmComponent* component)
{
	FulmEditionObserverInterface* interface = FULM_EDITION_OBSERVER_GET_IFACE(edition_observer);
	g_return_if_fail(interface->component_created != NULL);
	interface->component_created(edition_observer, component);
}


void
fulm_edition_observer_component_deleted(FulmEditionObserver* edition_observer, FulmComponent* component)
{
	FulmEditionObserverInterface* interface = FULM_EDITION_OBSERVER_GET_IFACE(edition_observer);
	g_return_if_fail(interface->component_deleted != NULL);
	interface->component_deleted(edition_observer, component);
}


void
fulm_edition_observer_pins_connected (FulmEditionObserver* edition_observer, FulmComponentPin* source_pin, FulmComponentPin* target_pin)
{
	FulmEditionObserverInterface* interface = FULM_EDITION_OBSERVER_GET_IFACE(edition_observer);
	g_return_if_fail(interface->pins_connected != NULL);
	interface->pins_connected(edition_observer, source_pin, target_pin);
}


void
fulm_edition_observer_pins_disconnected (FulmEditionObserver* edition_observer, FulmComponentPin* source_pin, FulmComponentPin* target_pin)
{
	FulmEditionObserverInterface* interface = FULM_EDITION_OBSERVER_GET_IFACE(edition_observer);
	g_return_if_fail(interface->pins_disconnected != NULL);
	interface->pins_disconnected(edition_observer, source_pin, target_pin);
}


static void
fulm_edition_observer_default_init(FulmEditionObserverInterface *interface)
{
	interface->component_created = fulm_edition_observer_component_created;
	interface->component_deleted = fulm_edition_observer_component_deleted;
	interface->pins_connected = fulm_edition_observer_pins_connected;
	interface->pins_disconnected = fulm_edition_observer_pins_disconnected;
}
