/* 
 * This file is part of the fulmicoton distribution (https://gitlab.gnome.org/powimod/fulmicoton.git)
 * Copyright (c) 2021 Dominique Parisot
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __FULM_APPLICATION__
#define __FULM_APPLICATION__

#include <glib-object.h>
#include "global.h"
#include "gdbm-storage.h"
#include "component-factory.h"

G_BEGIN_DECLS

#define FULM_TYPE_APPLICATION fulm_application_get_type()

G_DECLARE_FINAL_TYPE(FulmApplication, fulm_application, FULM, APPLICATION, GApplication)

FulmApplication* fulm_application_new(void);
const FulmStorage* fulm_application_get_storage(FulmApplication* application);
const FulmComponentFactory* fulm_application_get_component_factory(FulmApplication* application);

G_END_DECLS

#endif /*__FULM_APPLICATION__*/
